"""Invoke tasks."""

from shinvoke import namespace

namespace.configure(
    {
        "docker":{
            "project_name": "smart-panel-app"
        }
    }
)
