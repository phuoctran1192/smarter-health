import React from 'react';
import 'App.css';
import { AppRoutes } from '@routes';
import { AppProvider } from './providers/app';
import { ThemeProvider } from '@material-ui/styles';
import { SPTheme } from './theme';
function App() {
  return (
    <ThemeProvider theme={SPTheme}>
      <AppProvider>
        <AppRoutes />
      </AppProvider>
    </ThemeProvider>
  );
}

export default App;
