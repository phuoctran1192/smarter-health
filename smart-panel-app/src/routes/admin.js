import { AdminLayout } from '@layouts';
import { Suspense } from 'react';
import { Outlet, Navigate } from 'react-router-dom';
import { SmartSpinner } from '@components/Elements';
import { Authorize } from './guards/authorize';
import { Applicant } from '@features/admin/applicant';
import { User } from '../features/admin/user';

const App = () => {
  return (
    <AdminLayout>
      <Authorize redirectTo="/smart-panel/login">
        <Suspense
          fallback={
            <div className="h-full w-full flex items-center justify-center">
              <SmartSpinner />
            </div>
          }
        >
          <Outlet />
        </Suspense>
      </Authorize>
    </AdminLayout>
  );
};
export const adminRoutes = [
  {
    path: '/',
    element: <App />,
    children: [
      { path: '/smart-panel/admin/applicant', element: <Applicant /> },
      { path: '/smart-panel/admin/user', element: <User /> },
      { path: '*', element: <Navigate to="." /> },
    ],
  },
];
