import { useRoutes } from 'react-router-dom';
import { protectedRoutes } from './protected';
import { publicRoutes } from './public';
import { adminRoutes } from './admin';
import { Login } from 'features/auth/Login';
import Landing from 'features/Landing';

export const AppRoutes = () => {

  const commonRoutes = [{ path: '/smart-panel', element: <Login /> }, { path: '/smart-panel/landing', element: <Landing /> }];

  const routes = [...commonRoutes, ...publicRoutes, ...protectedRoutes, ...adminRoutes];

  const element = useRoutes([...routes]);

  return <>{element}</>;
};


