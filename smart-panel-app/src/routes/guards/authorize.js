import { useRequireAuth } from '@hooks'

export const Authorize = ({ children, redirectTo = '/login' }) => {
    const auth = useRequireAuth(redirectTo);
    return <div auth={auth}>{children}</div>;
}

