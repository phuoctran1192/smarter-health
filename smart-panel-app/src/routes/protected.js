import { MasterLayout } from '@layouts';
import { Suspense } from 'react';
import { Outlet, Navigate } from 'react-router-dom';
import { SmartSpinner } from '@components/Elements';
import { Profile } from '@features/profile';
import { Authorize } from './guards/authorize';

const App = () => {
  return (
    <MasterLayout>
      <Authorize redirectTo="/smart-panel/login">
        <Suspense
          fallback={
            <div className="h-full w-full flex items-center justify-center">
              <SmartSpinner />
            </div>
          }
        >
          <Outlet />
        </Suspense>
      </Authorize>
    </MasterLayout>
  );
};

export const protectedRoutes = [
  {
    path: '/',
    element: <App />,
    children: [
      { path: '/smart-panel/profile/:doctorId', element: <Profile /> },
      { path: '*', element: <Navigate to="." /> },
    ],
  },
];
