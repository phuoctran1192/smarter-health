import React from 'react';
import { makeStyles } from '@material-ui/core';
import { useField } from 'formik';
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/material.css'

const useStyles = makeStyles({
  container : {
    '&>.special-label': {
      display:'none !important'
    }
  },
  input: {
    width: '100% !important'
  }
});
const SMPhoneInput = ({ name, ...otherProps }) => {
  const classes = useStyles();
  const [field, meta] = useField(name);

  const configPhoneInput = {
    ...field,
    ...otherProps,
  };

  if (meta && meta.touched && meta.error) {
    configPhoneInput.error = true;
    configPhoneInput.helperText = meta.error;
  }

  return (
      <PhoneInput
        country='sg'
        inputClass={classes.input}
        containerClass={classes.container}
        {...configPhoneInput}
        inputProps={{name: name}}
      />
  );
};

export default SMPhoneInput;



