import React from 'react';
import { Button, makeStyles } from '@material-ui/core';
import { useFormikContext } from 'formik';

const useStyles = makeStyles({
  button: {
    backgroundColor: '#000',
  },
});
const ButtonWrapper = ({ children, disabled, ...otherProps }) => {
  const classes = useStyles();
  const { submitForm } = useFormikContext();
  const handleSubmit = () => {
    submitForm();
  };
  const configButton = {
    variant: 'contained',
    color: 'primary',
    fullWidth: true,
    onClick: handleSubmit,
    ...otherProps,
  };

  return (
    <Button {...configButton} disabled={disabled} className={classes.button}>
      {children}
    </Button>
  );
};

export default ButtonWrapper;
