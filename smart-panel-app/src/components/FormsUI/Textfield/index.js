import React from 'react';
import { TextField } from '@material-ui/core';
import { useField } from 'formik';
import SimpleLabel from '../Label';

const TextfieldWrapper = ({ name, label, ...otherProps }) => {
  const [field, meta] = useField(name);

  const configTextfield = {
    ...field,
    ...otherProps,
    fullWidth: true,
    variant: 'outlined',
  };

  if (meta && meta.touched && meta.error) {
    configTextfield.error = true;
    configTextfield.helperText = meta.error;
  }
  if(!configTextfield.value) {
    configTextfield.value = ""
  }

  return (
    <div>
      <SimpleLabel htmlFor={name}>
        {label}
      </SimpleLabel>
      <TextField {...configTextfield} />
    </div>
  );
};

export default TextfieldWrapper;
