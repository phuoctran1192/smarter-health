import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { useField, useFormikContext } from 'formik';
import { withStyles } from '@material-ui/styles';
import { FormHelperText } from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    padding: 0,
    position: 'unset',
    '&:hover': {
      backgroundColor: 'transparent',
    },
    '&.Mui-checked': {
      '&::before': {
        borderColor: '#4024CD !important',
      },
    },
    '&::before': {
      border: '1px solid #C4C4C4',
      borderRadius: '4px',
      content: '""',
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
    },
  },
  icon: {
    width: 0,
    height: 14,
    marginRight: 3,
    marginLeft: 3,
    position: 'static',
    right: '12px',
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundImage: `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='14' height='10' viewBox='0 0 14 10' fill='none'%3E%3Cpath d='M1 3.58065L3.39968 7.19307C3.89815 7.94345 4.14738 8.31864 4.50787 8.41795C4.60904 8.44582 4.71398 8.45759 4.81881 8.45283C5.19234 8.43587 5.51851 8.12523 6.17085 7.50395L13 1' stroke='%234126CF' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'/%3E%3Cpath d='M1 3.58065L3.39968 7.19307C3.89815 7.94345 4.14738 8.31864 4.50787 8.41795C4.60904 8.44582 4.71398 8.45759 4.81881 8.45283C5.19234 8.43587 5.51851 8.12523 6.17085 7.50395L13 1' stroke='%2371E3C7' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'/%3E%3Cpath d='M1 3.58065L3.39968 7.19307C3.89815 7.94345 4.14738 8.31864 4.50787 8.41795C4.60904 8.44582 4.71398 8.45759 4.81881 8.45283C5.19234 8.43587 5.51851 8.12523 6.17085 7.50395L13 1' stroke='%234024CD' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'/%3E%3C/svg%3E")`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    width: 14,
  },
});

function StyledRadio(props) {
  const classes = useStyles();

  return (
    <>
      <Radio
        className={classes.root}
        disableRipple
        color="default"
        checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
        icon={<span className={classes.icon} />}
        {...props}
      />
    </>
  );
}

const RadioLabel = withStyles({
  root: {
    paddingTop: 13,
    paddingBottom: 13,
    justifyContent: 'center',
    marginLeft: 0,
    flex: '1 auto',
    position: 'relative',
    '&:last-child': {
      marginRight: '0',
    },
    '& >.Mui-checked + .MuiFormControlLabel-label': {
      color: '#4024CD',
    },
    '& >.MuiTypography-body1': {
      fontSize: 16,
      lineHeight: '24px',
      color: '#737373',
    },
  },
})(FormControlLabel);

const useRadioStyles = makeStyles({
  root: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridTemplateColumns: '1fr 1fr',
  },
  helperText: {
    color: '#ED5E58',
  },
});
export default function CustomizedRadios({ name, options, handleOnChange, ...otherProps }) {
  const { setFieldValue } = useFormikContext();
  const [field, meta] = useField(name);
  const classes = useRadioStyles();

  const handleChange = (evt) => {
    const { value } = evt.target;
    setFieldValue(name, value);
    handleOnChange && handleOnChange(value);
  };

  const configRadio = {
    ...field,
    ...otherProps,
    onChange: handleChange,
  };

  if (typeof configRadio.value === 'number' || typeof configRadio.value === 'boolean') {
    configRadio.value = configRadio.value.toString();
  }

  if (!configRadio.value) {
    configRadio.value = '';
  }

  const errorProps = {};
  if (meta && meta.touched && meta.error) {
    errorProps.error = true;
    errorProps.helperText = meta.error;
  }

  return (
    <>
      <RadioGroup
        defaultValue={`${configRadio.value}`}
        {...configRadio}
        aria-label={name}
        name={name}
        className={classes.root}
      >
        {Object.keys(options).map((item, pos) => {
          return (
            <RadioLabel key={pos} value={item} control={<StyledRadio />} label={options[item]} />
          );
        })}
      </RadioGroup>
      {errorProps.error && (
        <FormHelperText className={classes.helperText}>{errorProps.helperText}</FormHelperText>
      )}
    </>
  );
}
