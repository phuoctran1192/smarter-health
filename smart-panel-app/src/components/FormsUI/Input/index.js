import React from 'react';
import { TextField } from '@material-ui/core';

const SMInput = ({ name, field }) => {
  const configTextfield = {
    ...field,
    fullWidth: true,
    variant: 'outlined',
  };

  return (
    <TextField {...configTextfield} name={name}/>
  );
};

export default SMInput;
