import { InputLabel, withStyles } from '@material-ui/core';
import React from 'react';

const SimpleLabel = ({ children, htmlFor }) => {
  const TextLabel = withStyles(() => ({
    root: {
      fontSize: 16,
      lineHeight: '24px',
      color: '#000',
      marginBottom: 10,
    },
  }))(InputLabel);

  return <TextLabel htmlFor={htmlFor}>{children}</TextLabel>;
};

export default SimpleLabel;
