import React, { useEffect, useRef } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { withStyles } from '@material-ui/styles';
import { Checkbox, FormControl } from '@material-ui/core';
import FormGroup from '@material-ui/core/FormGroup';

const useStyles = makeStyles({
  root: {
    padding: 0,
    position: 'relative',
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  icon: {
    width: 0,
    height: 14,
    position: 'absolute',
    right: '12px',
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundImage: `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='14' height='10' viewBox='0 0 14 10' fill='none'%3E%3Cpath d='M1 3.58065L3.39968 7.19307C3.89815 7.94345 4.14738 8.31864 4.50787 8.41795C4.60904 8.44582 4.71398 8.45759 4.81881 8.45283C5.19234 8.43587 5.51851 8.12523 6.17085 7.50395L13 1' stroke='%234126CF' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'/%3E%3Cpath d='M1 3.58065L3.39968 7.19307C3.89815 7.94345 4.14738 8.31864 4.50787 8.41795C4.60904 8.44582 4.71398 8.45759 4.81881 8.45283C5.19234 8.43587 5.51851 8.12523 6.17085 7.50395L13 1' stroke='%2371E3C7' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'/%3E%3Cpath d='M1 3.58065L3.39968 7.19307C3.89815 7.94345 4.14738 8.31864 4.50787 8.41795C4.60904 8.44582 4.71398 8.45759 4.81881 8.45283C5.19234 8.43587 5.51851 8.12523 6.17085 7.50395L13 1' stroke='%234024CD' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'/%3E%3C/svg%3E")`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain',
    width: 14,
  },
});

function SPCheckbox(props) {
  const classes = useStyles();
  return (
    <Checkbox
      className={classes.root}
      disableRipple
      color="default"
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      {...props}
    />
  );
}

const CheckboxLabel = withStyles({
  root: {
    border: '1px solid #4024CD',
    paddingTop: 13,
    paddingBottom: 13,
    borderRadius: '4px',
    justifyContent: 'center',
    marginLeft: 0,
    marginRight: 0,
    '& >.Mui-checked + .MuiFormControlLabel-label': {
      color: '#4024CD',
    },
    '& >.MuiTypography-body1': {
      fontSize: 16,
      lineHeight: '24px',
      color: '#737373',
    },
  },
})(FormControlLabel);

const useCheckboxStyles = makeStyles({
  root: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr 1fr 1fr',
    gridColumnGap: 20,
    gridRowGap: 20,
  },
  formControl: {
    width: '100%',
  },
});

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

export default function SPMultiCheckbox({ options, initValue, onDateChange, ...otherProps }) {
  const classes = useCheckboxStyles();
  const checkedValue = {};
  options.map((cur) => {
    if(initValue.includes(cur.value)) {
      checkedValue[cur.value] = true
    } else {
      checkedValue[cur.value] = false
    }
  });
  const [state, setState] = React.useState(checkedValue);
  const prevState = usePrevious(state)

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  useEffect(() => {
    let valueChange = {}
    if(prevState) {
      Object.keys(prevState).forEach((key) => {
        if(prevState && state && prevState[key] !== state[key]) {
          if((prevState[key]).toString() === "true") {
            valueChange = {item: key, change: 'REMOVE'};
          } else {
            valueChange = {item: key, change: 'ADD'};
          }
        }
      })
    }
    onDateChange(valueChange)
  }, [state]);

  return (
    <FormControl className={classes.formControl}>
      <FormGroup className={classes.root}>
        {options.map((item, pos) => {
          return (
            <CheckboxLabel
              key={pos}
              control={
                <SPCheckbox checked={state[item.value]} onChange={handleChange} name={item.value} />
              }
              label={item.label}
            />
          );
        })}
      </FormGroup>
    </FormControl>
  );
}