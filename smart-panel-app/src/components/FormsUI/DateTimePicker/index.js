import React from 'react';
import { TextField } from '@material-ui/core';
import { useField } from 'formik';
import SimpleLabel from '../Label';

const DateTimePicker = ({ name, label, ...otherProps }) => {
  const [field, meta] = useField(name);

  const configDateTimePicker = {
    ...field,
    ...otherProps,
    type: 'date',
    variant: 'outlined',
    fullWidth: true,
    InputLabelProps: {
      shrink: true,
    },
  };

  if (meta && meta.touched && meta.error) {
    configDateTimePicker.error = true;
    configDateTimePicker.helperText = meta.error;
  }

  return (
    <div>
      <SimpleLabel htmlFor={name}>{label}</SimpleLabel>
      <TextField {...configDateTimePicker} />
    </div>
  );
};

export default DateTimePicker;
