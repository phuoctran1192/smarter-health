import { FormControl, InputLabel, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { ErrorMessage, useField, useFormikContext } from 'formik';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  inputLabel: (props) => ({
    fontSize: 16,
    lineHeight: '24px',
    color: '#000',
    display: 'inline-block',
    alignItems: 'top',
    whiteSpace: 'pre-line',
    marginBottom: props.editable ? theme.spacing(1) : 'none',
    '& .MuiFormLabel-asterisk ': {
      color: 'red',
    },
    position: 'unset',
  }),
  formItem: (props) => ({
    display: 'flex',
    flexDirection: props.editable ? 'column' : 'row',
    width: '100%',
    padding: theme.spacing(1.5, 0),
  }),
  content: (props) => ({
    wordBreak: !props.editable ? 'break-all' : '',
  }),
  error: {
    color: '#ED5E58',
    padding: '5px 0',
    fontSize: '14px',
  },
}));

export const FormItem = ({
  id,
  name,
  value,
  label,
  inputType,
  required,
  editable = true,
  isShowError = true,
  ...props
}) => {
  const classes = useStyles({ editable });
  const { setFieldValue } = useFormikContext();
  const [field] = useField(name);
  const handleOnChange = (value) => {
    setFieldValue(name, value);
  };
  return (
    <Grid container className={classes.formItem} gutter={4}>
      {label && (
        <Grid item xs={editable ? 12 : 6} lg={editable ? 12 : 3}>
          <InputLabel className={classes.inputLabel} required={required}>
            {label}
          </InputLabel>
        </Grid>
      )}
      <Grid item xs={editable ? 12 : 6} lg={editable ? 12 : 9} className={classes.content}>
        {editable ? (
          <>
            <FormControl fullWidth variant="outlined">
              <>
                {React.cloneElement(props.children, {
                  onChange: handleOnChange,
                  name: name,
                  field: field,
                  value: field.value,
                })}
              </>
            </FormControl>
            {isShowError && <ErrorMessage className={classes.error} component="div" name={name} />}
          </>
        ) : (
          <span>{value || 'N/A'}</span>
        )}
      </Grid>
    </Grid>
  );
};
