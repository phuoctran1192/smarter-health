import { makeStyles, TextField } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  textfield: {
    width: '100%',
    fontSize: '16px',
  }
}));

export const SPTextField = ({ value, onChange, name, ...props }) => {
  const classes = useStyles();

  const handleOnChange = (event) => {
    onChange && onChange(event.target.value);
  };

  return (
    <TextField
      fullWidth
      variant = {'outlined'}
      value={value}
      onChange={handleOnChange}
      className={classes.textfield}
      {...props}
    />
  );
};
