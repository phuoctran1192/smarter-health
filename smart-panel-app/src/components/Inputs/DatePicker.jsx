import React from 'react';
import { useField, useFormikContext } from 'formik';
import SimpleLabel from 'components/FormsUI/Label';

import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import moment from 'moment';
import {
  DATE_PICKER_DEFAULT_MIN_DATE_FORMAT,
  DATE_PICKER_DISPLAY_FORMAT,
  DATE_PICKER_VALUE_FORMAT,
} from 'utils/constants';

export const DatePicker = ({ name, label, minDate, maxDate, value, onChange, ...otherProps }) => {
  const { setFieldValue, setFieldTouched } = useFormikContext();
  const [date] = useField(name);

  const inputMinDate = moment(minDate || DATE_PICKER_DEFAULT_MIN_DATE_FORMAT);

  const parseString = (str) => {
    if (!str) return null;
    if (str === '') return null;
    return moment(str, DATE_PICKER_VALUE_FORMAT).toDate();
  };

  const handleChange = (m) => {
    setFieldTouched(field.name, true, true);
    setFieldValue(field.name, m?.format(DATE_PICKER_VALUE_FORMAT) || '');
  };

  const [field, meta] = useField(name);
  const configDateTimePicker = {
    inputVariant: 'outlined',
    fullWidth: true,
    InputLabelProps: {
      shrink: true,
    },
    minDate: inputMinDate.toDate(),
    clearable: true,
    onChange: handleChange,
    value: parseString(date.value),
    format: DATE_PICKER_DISPLAY_FORMAT,
    ...otherProps,
  };

  configDateTimePicker.maxDate = maxDate
    ? moment(maxDate).toDate()
    : moment().add(200, 'years').toDate();

  if (meta && meta.touched && meta.error) {
    configDateTimePicker.error = true;
    configDateTimePicker.helperText = meta.error;
  }

  return (
    <div>
      <SimpleLabel htmlFor={name}>{label}</SimpleLabel>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <KeyboardDatePicker {...configDateTimePicker} />
      </MuiPickersUtilsProvider>
    </div>
  );
};
