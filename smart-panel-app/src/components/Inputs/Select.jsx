import { FormControl, InputLabel, makeStyles, MenuItem, Select } from '@material-ui/core';
import { useFormikContext } from 'formik';

const useStyles = makeStyles((theme) => ({
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  select: {
    width: '100%',
    fontSize: '16px',
  },
  menuItem: {
    fontSize: '16px',
  },
}));
export const SPSelect = ({ value, onChange, dataSource = [], name, ...props }) => {
  const classes = useStyles();
  const handleOnChange = (event) => {
    onChange && onChange(event.target.value);
  };
  return (
    <Select
      value={value}
      onChange={handleOnChange}
      inputProps={{
        name: 'age',
        id: 'filled-age-native-simple',
      }}
      classes={{ root: classes.select }}
      name={name}
      {...props}
    >
      {dataSource.map((item, index) => (
        <MenuItem
          key={`${item.label}-${index}`}
          value={item.value}
          classes={{ root: classes.menuItem }}
        >
          {item.label}
        </MenuItem>
      ))}
    </Select>
  );
};
