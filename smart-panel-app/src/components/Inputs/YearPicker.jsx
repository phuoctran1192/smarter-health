import React from 'react';
import { TextField } from '@material-ui/core';
import { useField } from 'formik';
import SimpleLabel from 'components/FormsUI/Label';
export const YearPicker = ({ name, label, ...props }) => {
    const [field, meta] = useField(name);

    const handleChange = (e) => {
        if (String(e.target.value).length > 4) return;
        field.onChange(e);
    }

    const configYearPicker = {
        ...field,
        ...props,
        type: "number",
        variant: 'outlined',
        fullWidth: true,
        InputLabelProps: {
            shrink: true,
        },
        value: field.value,
        onChange: handleChange,
    };

    if (meta && meta.touched && meta.error) {
        configYearPicker.error = true;
        configYearPicker.helperText = meta.error;
    }
    if (!configYearPicker.value) {
        configYearPicker.value = ""
    }

    return (
        <div>
            <SimpleLabel htmlFor={name}>
                {label}
            </SimpleLabel>
            <TextField {...configYearPicker} InputProps={{ inputProps: { min: 0, max: 9999 } }} />
        </div>
    );
};
