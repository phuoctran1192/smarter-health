import React, { useEffect } from 'react';
import { MenuItem, Select, FormHelperText, Input, Checkbox, ListItemText } from '@material-ui/core';
import { useField, useFormikContext } from 'formik';
import SimpleLabel from '../FormsUI/Label';
import { makeStyles } from '@material-ui/styles';

const useMultipleSelectStyles = makeStyles({
    helperText: {
        color: '#ED5E58',
    },
});

// Options have shape: {id: number, name: string}
// Field value has type array of id number
const FormikMultipleSelect = ({ id, name, label, options }) => {
    const classes = useMultipleSelectStyles();

    const { setFieldValue } = useFormikContext();
    const [field, meta] = useField(name);

    const availableOptions =
        Array.isArray(options) ?
            options.filter(item => item && item.id && item.name) :
            [];
    const selectedOptions = Array.isArray(field.value) ? field.value : [];

    const handleChange = (evt) => {
        setFieldValue(field.name, evt.target.value);
    };

    const errorProps = {};
    if (meta && meta.touched && meta.error) {
        errorProps.error = true;
        errorProps.helperText = meta.error;
    }

    useEffect(() => {
        if (field.value == null || !Array.isArray(field.value)) setFieldValue(field.name, []);
    }, []);

    const optionDict = {};
    availableOptions.forEach(item => optionDict[item.id] = item.name);
    const renderChecked = (selected) => {
        return selected.map(value => optionDict[value]).filter(n => n).join(', ');
    }
    return (
        <div>
            <SimpleLabel id={`multiple-checkbox-label-${id}`}>{label}</SimpleLabel>
            <Select
                labelId={`multiple-checkbox-label-${id}`}
                id={`multiple-checkbox-${id}`}
                multiple
                fullWidth
                variant='outlined'
                value={selectedOptions}
                onChange={handleChange}
                renderValue={(selected) => renderChecked(selected)}
            >
                {availableOptions.map((option) => {
                    return (
                        <MenuItem value={option.id} key={option.id}>
                            <Checkbox checked={selectedOptions.indexOf(option.id) > -1} />
                            <ListItemText primary={option.name} />
                        </MenuItem>
                    );
                })}
            </Select>
            {errorProps.error && <FormHelperText className={classes.helperText}>{errorProps.helperText}</FormHelperText>}
        </div>
    );
};

export default FormikMultipleSelect;
