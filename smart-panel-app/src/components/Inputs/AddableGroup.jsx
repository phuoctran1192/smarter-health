import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Divider, Grid, Typography } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
const useStyles = makeStyles((theme) => ({
  wrapper: {
    padding: '0',
  },
  header: {
    paddingBottom: '20px',
  },
  title: {},
  delete: {
    textAlign: 'right',
    color: '#ED5E58',
    textDecoration: 'underline',
    cursor: 'pointer',
  },
  add: {
    color: theme.palette.primary.main,
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'end',
    '& .text': {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: 'fit-content',
    },
  },
  divider: {
    marginTop: '5px',
    backgroundColor: theme.palette.primary.main,
    height: '2px',
  },
  button: {
    height: '60px',
    width: '100%',
    backgroundColor: theme.palette.primary.black,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
    lineHeight: '24px',
    cursor: 'pointer',
    borderRadius: '5px',
    marginTop: '60px',
    border: 'none',
  },
}));
export const withAddable =
  (Component) =>
  ({
    formModel,
    title = 'Language',
    subTitle = 'Language',
    handleOnSave,
    form,
    field,
    defaultValue,
    setValue,
    ...props
  }) => {
    const classes = useStyles();
    const { name, value } = field;
    const { setFieldValue } = form;

    useEffect(() => {
      if (defaultValue) {
        setFieldValue(name, defaultValue);
        setValue && setValue(defaultValue);
      }
    }, [defaultValue]);

    const addItem = () => {
      const temp = [...value];
      formModel =
        formModel ||
        Object.keys(form.values[field.name][0]).reduce((model, key) => {
          return { ...model, [key]: '' };
        }, {});
      temp.push({
        ...formModel,
      });
      setValue && setValue(temp);
      setFieldValue(name, temp);
    };

    const deleteItem = (index) => {
      const temp = [...value];
      temp.splice(index, 1);
      setValue && setValue(temp);
      setFieldValue(name, temp);
    };

    return (
      <div className={classes.group}>
        <div className={classes.header}>
          <Grid container direction="row" justifyContent="space-between">
            <Grid item xs={6} className={classes.title}>
              <Typography variant="subtitle1">
                <b>{title}</b>
              </Typography>
            </Grid>
            <Grid item xs={6} className={classes.add} onClick={addItem}>
              <b className="text">
                <AddIcon /> {`Add ${subTitle}`}
              </b>
            </Grid>
          </Grid>
          <Divider classes={{ root: classes.divider }} />
        </div>
        <div>
          {value?.length > 0 &&
            value.map((item, index) => (
              <div className={classes.wrapper} key={`${item[field.name]}-${index}`}>
                <Grid
                  container
                  direction="row"
                  justifyContent="space-between"
                  classes={{ root: classes.header }}
                >
                  <Grid item xs={6}>
                    <b>
                      {index + 1}.{subTitle}
                    </b>
                  </Grid>
                  {!!index && (
                    <Grid item xs={6} className={classes.delete} onClick={() => deleteItem(index)}>
                      Delete
                    </Grid>
                  )}
                </Grid>
                <Component value={item} index={index} {...props} />
              </div>
            ))}
        </div>
      </div>
    );
  };
