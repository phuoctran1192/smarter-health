import React from 'react';
import { MenuItem, Select, FormHelperText, Checkbox, ListItemText } from '@material-ui/core';
import SimpleLabel from '../FormsUI/Label';
import { makeStyles } from '@material-ui/styles';

const useMultipleSelectStyles = makeStyles({
  helperText: {
    color: '#ED5E58',
  },
});

const ReadOnlyMultipleSelect = ({ id, label, value, errorProps, options }) => {
  const classes = useMultipleSelectStyles();

  const availableOptions = Array.isArray(options)
    ? options.filter((item) => item && item.id && item.name)
    : [];
  const selectedOptions = Array.isArray(value) ? value : [];

  const optionDict = {};
  availableOptions.forEach((item) => (optionDict[item.id] = item.name));

  return (
    <div>
      {options.length === 0 ? (
        <React.Fragment>
          <SimpleLabel id={`multiple-checkbox-label-${id}`}>{label}</SimpleLabel>
          <Select
            labelId={`multiple-checkbox-label-${id}`}
            id={`multiple-checkbox-${id}`}
            fullWidth
            variant="outlined"
            value={'empty'}
            disabled
          >
            <MenuItem value="empty">N.A</MenuItem>
          </Select>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <SimpleLabel id={`multiple-checkbox-label-${id}`}>{label}</SimpleLabel>
          <Select
            labelId={`multiple-checkbox-label-${id}`}
            id={`multiple-checkbox-${id}`}
            multiple
            fullWidth
            variant="outlined"
            value={value}
            onChange={() => {}}
            renderValue={(selected) => selected.map((s) => optionDict[s]).join(', ')}
            disabled
          >
            {availableOptions.map((option) => {
              return (
                <MenuItem value={option.id} key={option.id}>
                  <Checkbox checked={selectedOptions.indexOf(option.id) > -1} />
                  <ListItemText primary={option.name} />
                </MenuItem>
              );
            })}
          </Select>
          {errorProps?.error && (
            <FormHelperText className={classes.helperText}>{errorProps.helperText}</FormHelperText>
          )}
        </React.Fragment>
      )}
    </div>
  );
};

export default ReadOnlyMultipleSelect;
