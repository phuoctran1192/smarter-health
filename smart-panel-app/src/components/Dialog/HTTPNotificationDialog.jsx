import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { LoadingButton } from '@components/Buttons/LoadingButton';
import SomethingWrong from '@assets/icons/not-access.svg';
const useStyles = makeStyles((theme) => ({
  button: {
    maxWidth: '370px',
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
    },
    '& .MuiButton-label:hover': {
      backgroundColor: theme.palette.primary.main,
    },
  },
  image: {
    width: 'fit-content',
    margin: 'auto',
    marginBottom: '46px',
  },
  paper: {
    padding: '20px',
    maxWidth: '930px',
  },
  title: {
    fontWeight: 700,
    marginBottom: '20px',
  },
  scrollPaper: {
    alignItems: 'start',
    paddingTop: '5%',
  },
}));
export const SPHTTPNotificationDialog = ({ title, message, icon = SomethingWrong, ...props }) => {
  const classes = useStyles();
  return (
    <Dialog
      {...props}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      classes={{ paper: classes.paper, scrollPaper: classes.scrollPaper }}
    >
      <DialogContent>
        <div className={classes.image}>
          <img src={icon} />
        </div>
        <Typography variant="h3" align="center" className={classes.title}>
          {title}
        </Typography>
        <DialogContentText id="alert-dialog-description">
          <Typography style={{ fontSize: '22px' }}>{message}</Typography>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <LoadingButton text="Back to login" onClick={props.onRedirect} classes={classes.button} />
      </DialogActions>
    </Dialog>
  );
};
