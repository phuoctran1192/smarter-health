import React, { useEffect, useState } from 'react';
import {
  Box,
  Paper,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Table,
  Grid,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { SmartSpinner } from '../../Elements';
import { SPTablePagination } from '../TablePagination';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  table: {
    position: 'relative',
    fontSize: '16px',
  },
  tableHead: {
    backgroundColor: '#d1eefd',
    fontWeight: 800,
    color: '#03384f',
    fontSize: '15px',
  },
  loading: {
    position: 'absolute',
    height: '200px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    top: '30%',
    left: '50%',
  },
  tableLoading: {
    opacity: 0.5,
  },
}));

const DEFAUT_PAGESIZE = 10;

export const SPTable = ({
  isReloadTable,
  setIsReloadTable,
  params,
  columns,
  apiGetList = null,
  field,
}) => {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageIndex, setPageIndex] = useState(0);
  const [pageSize, setPageSize] = useState(DEFAUT_PAGESIZE);
  const [loading, setLoading] = useState(true);
  const [totalPage, setTotalPage] = useState(0);

  const fetchList = () => {
    if (isReloadTable && apiGetList) {
      setLoading(true);
      const payload = {
        pageSize: pageSize,
        pageIndex: pageIndex,
        ...params,
      };
      apiGetList(payload)
        .then((res) => {
          if (res?.data?.content) {
            setData(res.data.content[field]);
            setTotal(res.data.content.totalItems);
            setTotalPage(
              res.data.content.totalItems % pageSize > 0
                ? Math.floor(res.data.content.totalItems / pageSize) + 1
                : Math.floor(res.data.content.totalItems / pageSize),
            );
          }
        })
        .catch((err) => console.log(err))
        .finally(() => {
          setIsReloadTable(false);
          setLoading(false);
        });
    }
  };

  useEffect(() => {
    fetchList();
  }, [isReloadTable]);

  const handleChangePage = (event, newPage) => {
    setPageIndex(newPage - 1);
    setIsReloadTable(true);
  };
  const handleChangePageSize = (value) => {
    setPageSize(value);
    setPageIndex(0);
    setIsReloadTable(true);
  };

  return (
    <Box className={classes.table} boxShadow={2}>
      <TableContainer
        component={Paper}
        elevation={0}
        id="tableScroll"
        style={{ marginTop: 30, borderRadius: '0.3cm' }}
      >
        <Table style={{ backgroundColor: 'white' }}>
          <TableHead>
            <TableRow>
              {columns.map((column, id) => (
                <TableCell classes={{ root: classes.tableHead }} key={id}>
                  {column.title}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          {loading && (
            <div className={classes.loading}>
              <SmartSpinner />
            </div>
          )}
          <TableBody className={clsx(loading && classes.tableLoading)}>
            {data.length === 0 ? (
              <TableRow>
                <TableCell style={{ textAlign: 'center' }} colSpan={columns.length}>
                  No results found
                </TableCell>
              </TableRow>
            ) : (
              data.map((item, index) => (
                <TableRow key={item.id}>
                  {columns.map((cell, cellIndex) =>
                    cell.render ? (
                      <TableCell
                        key={`${item.name}_${cellIndex}`}
                        component="th"
                        scope="row"
                        style={{ textAlign: item.textAlign }}
                      >
                        {cell.render(item, index)}
                      </TableCell>
                    ) : (
                      <TableCell component="th" scope="row">
                        {item[cell.dataIndex]}
                      </TableCell>
                    ),
                  )}
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <SPTablePagination
        totalPage={totalPage}
        handleChangePage={handleChangePage}
        handleChangePageSize={handleChangePageSize}
        pageSize={pageSize}
      ></SPTablePagination>
    </Box>
  );
};
