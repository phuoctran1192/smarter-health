import { Box, Grid } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import { makeStyles } from '@material-ui/core/styles';
import { SPSelect } from '@components/Inputs';

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: 'white',
  },
  pageSzie: {
    paddingLeft: 40,
    display: 'flex',
    justifyContent: 'left',
  },
  pagination: {
    paddingTop: 12,
    paddingBottom: 12,
    display: 'flex',
    justifyContent: 'right',
  },
  select: {
    '&&&:before': {
      borderBottom: 'none',
    },
    '&&:after': {
      borderBottom: 'none',
    },
  },
  paginationButton: {
    '& .Mui-selected': {
      backgroundColor: '#d1eefd',
    },
    '& button:hover': {
      backgroundColor: '#f5fbfe',
    },
  },
}));
const PAGE_SIZES = [
  { label: 10, value: 10 },
  { label: 25, value: 25 },
  { label: 50, value: 50 },
  { label: 100, value: 100 },
];
export const SPTablePagination = ({
  totalPage,
  handleChangePage,
  handleChangePageSize,
  pageSize,
  ...props
}) => {
  const classes = useStyles();
  const onChangePageSize = (value) => {
    return handleChangePageSize(value);
  };
  return (
    <Grid container className={classes.container}>
      <Grid item xs={3} md={6} className={classes.pageSzie}>
        <Box component="p" style={{ textAlign: 'center', paddingRight: 40 }}>
          Rows per size
        </Box>
        <SPSelect
          classes={classes.select}
          dataSource={PAGE_SIZES}
          disableUnderline
          onChange={onChangePageSize}
          value={pageSize}
        />
      </Grid>
      <Grid item xs={9} md={6} className={classes.pagination}>
        <Pagination
          classes={{ ul: classes.paginationButton }}
          count={totalPage}
          showFirstButton
          showLastButton
          onChange={handleChangePage}
        />
      </Grid>
    </Grid>
  );
};
