import { Grid, LinearProgress, Button, makeStyles } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import React, { useEffect, useState } from 'react';
import { uploadFile } from 'services/upload-file.service';
import ReplayIcon from '@material-ui/icons/Replay';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';

const StyledLinearProgress = withStyles({
  colorPrimary: {
    backgroundColor: '#E0E0E2',
    borderRadius: '5px',
  },
  barColorPrimary: {
    backgroundColor: '#4024CD',
    borderRadius: '5px',
  },
})(LinearProgress);
const useStyles = makeStyles({
  uploadWrapper: {
    display: 'flex',
    alignItems: 'center',
    marginTop: '30px',
    marginBottom: '30px',
  },
  titleWrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 15,
  },
  uploadInfor: {
    flex: 1,
  },
  actionButton: {
    padding: 0,
    minWidth: 'initial',
    marginRight: 24,
  },
  statusText: {
    fontSize: 14,
    lineHeight: '150%',
    textAlign: 'right',
  },
  successText: {
    color: '#C9D67B',
  },
  failText: {
    color: '#ED5E58',
  },
});

export function SingleFileUploadWithProgress({ file, onDelete, onUpload }) {
  const classes = useStyles();
  const [progress, setProgress] = useState(0);
  const [uploadStatus, setUploadStatus] = useState('pending');

  async function upload() {
    setUploadStatus('pending');
    await uploadFile(file, setProgress, setUploadStatus).then((uploadFile) => {
      if (uploadFile) {
        uploadFile.file = file;
        setUploadStatus('success');
        onUpload({ uploadFile });
      } else {
        setUploadStatus('error');
      }
    });
  }
  useEffect(() => {
    upload();
  }, []);

  const fileDelete = () => {
    onDelete(file);
  };

  return (
    <Grid item>
      <div className={classes.uploadWrapper}>
        <Button size="small" onClick={fileDelete} className={classes.actionButton}>
          <CancelOutlinedIcon color="secondary" />
        </Button>
        {uploadStatus === 'error' && (
          <Button size="small" onClick={() => upload()} className={classes.actionButton}>
            <ReplayIcon color="secondary" />
          </Button>
        )}
        <div className={classes.uploadInfor}>
          <div className={classes.titleWrapper}>
            <span>{file.name}</span>
            <span>
              {
                {
                  pending: `${(file.size / (1024 * 1024)).toFixed(2)} Mb`,
                  success: (<span className={`${classes.statusText} ${classes.successText}`}>Completed</span>),
                  error: (<span className={`${classes.statusText} ${classes.failText}`}>Upload Failed</span>),
                  errorMaxSizeExceed: (<span className={`${classes.statusText} ${classes.failText}`}>Max file size is 5MB</span>),
                }[uploadStatus]
              }
            </span>
          </div>
          <div style={{ width: '100%' }}>
            <StyledLinearProgress variant="determinate" value={progress} />
          </div>
        </div>
      </div>
    </Grid>
  );
}
