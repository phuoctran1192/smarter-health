import { Grid, makeStyles } from '@material-ui/core';
import { useFormikContext } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import { useDropzone } from 'react-dropzone';
import { SingleFileUploadWithProgress } from './SingleFileUploadWithProgress';
import { UploadError } from './UploadError';
import { v4 as uuidv4 } from 'uuid';
import { updateDocument } from 'store/documentSlice';
import { SingleFileUploaded } from './SingleFileUploaded';

function getNewId() {
  return uuidv4();
}
const useStyles = makeStyles({
  dropZone: {
    backgroundColor: '#F7F7F7',
    textAlign: 'center',
    backgroundImage: `url("data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' rx='4' ry='4' stroke='%23C4C4C4FF' stroke-width='4' stroke-dasharray='6%2c 14' stroke-dashoffset='0' stroke-linecap='square'/%3e%3c/svg%3e")`,
    borderRadius: '10px',
    paddingTop: '40px',
    paddingBottom: '40px',
  },
  dropZoneText: {
    fontSize: 16,
    lineHeight: '24px',
    color: '#737373',
    marginTop: 0,
    marginBottom: 0,
  },
  textBlue: {
    color: '#4126CF',
    fontWeight: 'bold',
  },
});
export function MultipleFileUploadField({ name, uploaded }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { setFieldValue } = useFormikContext();
  const [files, setFiles] = useState([]);
  const [uploadedFiles, setUploadedFiles] = useState([]);
  const [uploadSuccessFiles, setUploadSuccessFiles] = useState([]);

  const onDrop = (accFiles, rejFiles) => {
    const mappedAcc = accFiles.map((file) => ({
      file,
      errors: [],
      id: getNewId(),
    }));
    const mappedRej = rejFiles.map((r) => ({ ...r, id: getNewId() }));
    setFiles((curr) => [...curr, ...mappedAcc, ...mappedRej]);
  };
  const updateData = () => {
    const fileSuccessed = [];
    uploadSuccessFiles.forEach((s) => {
      const { file, ...rest } = s;
      return fileSuccessed.push(rest);
    });
    const finalFile = [...uploadedFiles, ...fileSuccessed];
    dispatch(updateDocument({ finalFile, name }));
    const fileUploadId = [];
    finalFile.forEach((ff) => {
      const { uploadId, ...rest } = ff;
      return fileUploadId.push({ uploadId: uploadId });
    });
    setFieldValue(name, fileUploadId);
  }
  useEffect(() => {
    updateData();
  }, [uploadedFiles, uploadSuccessFiles]);

  useEffect(() => {
    setUploadedFiles(uploaded || []);
  }, []);

  function onUpload({ uploadFile }) {
    setUploadSuccessFiles((curSuccess) => [...curSuccess, uploadFile]);
  }

  function onDelete(file) {
    setFiles((curr) => curr.filter((fw) => fw.file !== file));
    setUploadSuccessFiles((curSuccess) => curSuccess.filter((f) => f.file !== file));
  }

  function onUploadedDelete(file) {
    setUploadedFiles((cur) => cur.filter((f) => f !== file));
  }

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: ['.pdf', '.jpg','.jpeg', '.png', '.doc', '.docx'],
    maxSize: 5 * 1024 * 1024, // 5MB
  });

  return (
    <React.Fragment>
      <Grid item>
        <div {...getRootProps({})} className={classes.dropZone}>
          <input {...getInputProps()} />
          <p className={classes.dropZoneText}>
            Drag file here or <span className={classes.textBlue}>Browse</span> file
          </p>
        </div>
      </Grid>
      {uploadedFiles.length > 0 &&
        uploadedFiles.map((f, idx) => (
          <SingleFileUploaded onDelete={onUploadedDelete} file={f} key={idx} />
        ))}
      {files.map((fileWrapper) => (
        <Grid item key={fileWrapper.id}>
          {fileWrapper.errors.length ? (
            <UploadError file={fileWrapper.file} errors={fileWrapper.errors} onDelete={onDelete} />
          ) : (
            <SingleFileUploadWithProgress
              onDelete={onDelete}
              onUpload={onUpload}
              file={fileWrapper.file}
              fileID={fileWrapper.id}
            />
          )}
        </Grid>
      ))}
    </React.Fragment>
  );
}
