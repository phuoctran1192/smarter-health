import React from 'react';
import { Grid, Button, makeStyles } from '@material-ui/core';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';

const useStyles = makeStyles({
  uploadWrapper: {
    display: 'flex',
    alignItems: 'center',
    marginTop: '30px',
    marginBottom: '30px',
  },
  titleWrapper: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  uploadInfor: {
    flex: 1,
  },
  actionButton: {
    padding: 0,
    minWidth: 'initial',
    marginRight: 24,
  },
  statusText: {
    fontSize: 14,
    lineHeight: '150%',
    textAlign: 'right',
  },
  failText: {
    color: '#ED5E58',
  },
});

export function SingleFileUploaded({ file, onDelete }) {
  const classes = useStyles();
  return (
    <Grid item>
      <div className={classes.uploadWrapper}>
        <Button size="small" onClick={() => onDelete(file)} className={classes.actionButton}>
          <CancelOutlinedIcon color="secondary" />
        </Button>
        <div className={classes.uploadInfor}>
          <div className={classes.titleWrapper}>
            <span>{file.fileName}.{file.fileExtension}</span>
            <span className={`${classes.statusText}`}>Uploaded</span>
          </div>
        </div>
      </div>
    </Grid>
  );
}
