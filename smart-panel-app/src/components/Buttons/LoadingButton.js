import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, CircularProgress } from '@material-ui/core';
import clsx from 'clsx';

export const LoadingButton = ({
  classes,
  loading,
  text,
  loadingColor = 'white',
  loadingSize = 24,
  ...props
}) => {
  const buttonStyles = useStyles({ loadingSize });
  return (
    <Button
      className={clsx(classes, buttonStyles.root, buttonStyles.primaryButton)}
      type="submit"
      {...props}
    >
      {loading ? <CircularProgress color={loadingColor} size={loadingSize} /> : text}
    </Button>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: (props) => `${props.loadingSize * 3.75}px`,
    minHeight: (props) => `${props.loadingSize * 1.4}px`,
    '&:hover': {
      backgroundColor: theme.palette.primary.black,
    },
  },
  primaryButton: {
    height: '60px',
    width: '100%',
    backgroundColor: theme.palette.primary.black,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
    lineHeight: '24px',
    cursor: 'pointer',
    borderRadius: '5px',
    border: 'none',
    margin: 'auto',
    '& :hover': {
      backgroundColor: theme.palette.primary.black,
    },
    '&.Mui-disabled': {
      backgroundColor: 'rgba(0, 0, 0, 0.26)',
    },
  },
}));
