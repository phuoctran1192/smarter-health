import React from 'react';

const { withStyles, Tooltip, makeStyles } = require('@material-ui/core');
const useStyles = makeStyles({
  toolTip: {
    fontFamily: 'Lato',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '14px',
    lineHeight: '17px',
    color: '#4024CD',
    position: 'relative',
    '&::before': {
      content: '""',
      display: 'block',
      width: 16,
      height: 16,
      backgroundImage: `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 16 16' fill='none'%3E%3Cg clip-path='url(%23clip0_41_3)'%3E%3Cpath d='M6.684 3.48C6.684 3.28 6.752 3.12 6.888 3C7.024 2.872 7.204 2.808 7.428 2.808C7.652 2.808 7.832 2.872 7.968 3C8.104 3.12 8.172 3.28 8.172 3.48C8.172 3.68 8.104 3.844 7.968 3.972C7.832 4.092 7.652 4.152 7.428 4.152C7.204 4.152 7.024 4.092 6.888 3.972C6.752 3.844 6.684 3.68 6.684 3.48ZM7.968 5.412V12H6.876V5.412H7.968Z' fill='%234126CF'/%3E%3Ccircle cx='8' cy='8' r='7.5' stroke='%234024CD'/%3E%3C/g%3E%3Cdefs%3E%3CclipPath id='clip0_41_3'%3E%3Crect width='16' height='16' fill='white'/%3E%3C/clipPath%3E%3C/defs%3E%3C/svg%3E")`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'contain',
      position: 'absolute',
      right: 'calc(100% + 10px)'
    }
  },
});
export const CustomTooltip = withStyles({
  tooltip: {
    margin: 0,
    backgroundColor: '#F5F3FF',
    color: '#4024CD',
    maxWidth: 300,
    fontFamily: "Lato",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "13px",
    lineHeight: "150%",
    borderRadius: '4px',
    padding: '15px',
  }
})(Tooltip);

export const TooltipWrapper = ({ children, title }) => {
  const classes = useStyles();
  return (
    <CustomTooltip title={title}>
      <span className={classes.toolTip}>{children}</span>
    </CustomTooltip>
  );
};
