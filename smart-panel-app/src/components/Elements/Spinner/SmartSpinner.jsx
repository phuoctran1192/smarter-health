import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.admin.main,
  },
}));
export const SmartSpinner = ({ color = 'primary' }) => {
  const classes = useStyles();
  return <CircularProgress className={classes.root} />;
};
