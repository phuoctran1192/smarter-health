import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  info: {
    borderRadius: '12px',
    padding: '20px 20px',
    border: '1px solid #29b6f6',
    position: 'relative',
    color: '#29b6f6',
  },
  success: {
    borderRadius: '12px',
    padding: '20px 20px',
    border: '1px solid #66bb6a',
    position: 'relative',
    color: '#66bb6a',
  },
  warning: {
    borderRadius: '12px',
    padding: '20px 20px',
    border: '1px solid #ffa726',
    position: 'relative',
    color: '#ffa726',
  },
  error: {
    borderRadius: '12px',
    padding: '20px 20px',
    border: '1px solid #f44336',
    position: 'relative',
    color: '#f44336',
  },
}));
export const SPMessageBox = ({ info, success, warning, error, title, message, ...otherProps }) => {
  const classes = useStyles();

  const renderMessageBox = () => {
    switch (true) {
      case info:
        return (
          <div className="container-930" {...otherProps}>
            <p className={classes.info}>
              <strong>{title}</strong> {message}
            </p>
          </div>
        );
      case success:
        return (
          <div className="container-930" {...otherProps}>
            <p className={classes.success}>
              <strong>{title}</strong> {message}
            </p>
          </div>
        );
      case warning:
        return (
          <div className="container-930" {...otherProps}>
            <p className={classes.warning}>
              <strong>{title}</strong> {message}
            </p>
          </div>
        );
      case error:
        return (
          <div className="container-930" {...otherProps}>
            <p className={classes.error}>
              <strong>{title}</strong> {message}
            </p>
          </div>
        );
      default:
        return <></>;
    }
  };

  return message && message !== '' ? renderMessageBox() : <></>;
};
