import * as React from 'react';
import { store } from '@store'

export const StoreProvider = ({ children }) => {
    return (
        <>
            <>{children}</>
            <div store={store}></div>
        </>
    )
}