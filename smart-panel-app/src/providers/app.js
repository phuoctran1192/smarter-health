import * as React from 'react';
import { ErrorBoundary } from 'react-error-boundary';
import { HelmetProvider } from 'react-helmet-async';
import { BrowserRouter as Router } from 'react-router-dom';
import { SmartSpinner } from '@components/Elements';
import { ProvideAuth } from '@hooks';
import { StoreProvider } from '@providers/StoreProvider';

const ErrorFallback = () => {
  return (
    <div
      className="text-red-500 w-screen h-screen flex flex-col justify-center items-center"
      role="alert"
    >
      <h2 className="text-lg font-semibold">Oops, something went wrong :( </h2>
    </div>
  );
};
export const AppProvider = ({ ...props }) => {
  return (
    <React.Suspense
      fallback={
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <SmartSpinner />
        </div>
      }
    >
      <ErrorBoundary FallbackComponent={ErrorFallback}>
        <HelmetProvider>
          <StoreProvider>
            <ProvideAuth>
              <Router>{props.children}</Router>
            </ProvideAuth>
          </StoreProvider>
        </HelmetProvider>
      </ErrorBoundary>
    </React.Suspense>
  );
};
