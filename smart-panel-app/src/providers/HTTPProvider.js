import * as React from 'react';
import { SPHTTPNotificationDialog } from '@components/Dialog';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import Actions from '@store/notification/Actions';

export const HTTPProvider = ({ children }) => {
  const httpMessage = useSelector((state) => state.notification.httpMessage);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onRedirect = () => {
    navigate(httpMessage.redirectTo);
    dispatch(
      Actions.updateMessageHttp({
        hasMessage: false,
        title: '',
        message: '',
        httpCode: '',
        redirectTo: '',
        icon: '',
      }),
    );
  };

  return (
    <>
      <>{children}</>
      <SPHTTPNotificationDialog
        open={httpMessage.hasMessage}
        title={httpMessage.title}
        message={httpMessage.message}
        icon={httpMessage.icon}
        onRedirect={onRedirect}
      />
    </>
  );
};
