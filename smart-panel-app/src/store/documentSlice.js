import { createSlice } from "@reduxjs/toolkit";

const initialState = {};

export const documentSlice = createSlice({
  name: "document",
  initialState,
  reducers: {
    updateDocument: (state, action) => {
      const name = action.payload.name;
      const finalFile = action.payload.finalFile;
      state[name] = finalFile || initialState[name];
    }
  },
});

// Export actions
export const { updateDocument } = documentSlice.actions;

// Export reducer
export default documentSlice.reducer;
