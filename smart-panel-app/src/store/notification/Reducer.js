import { createReducer } from '@reduxjs/toolkit';
import Actions from './Actions';
import { initialState } from './InitialState';

export const reducer = createReducer(initialState, (builder) => {
  builder.addCase(Actions.updateMessageHttp, (state, action) => {
    console.log(action.payload);
    state.httpMessage = action.payload;
  });
});
