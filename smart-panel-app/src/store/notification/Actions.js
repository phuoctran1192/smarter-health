import { createAction } from '@reduxjs/toolkit';

const updateMessageHttp = createAction(`ui/updateMessageHttp`);

const Actions = {
  updateMessageHttp,
};

export default Actions;
