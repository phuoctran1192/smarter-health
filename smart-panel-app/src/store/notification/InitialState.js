import SomethingWrong from '@assets/icons/not-access.svg';
export const initialState = {
  httpMessage: {
    hasMessage: false,
    httpCode: 0,
    title: 'Something went wrong!',
    message: '',
    redirectTo: '',
    icon: SomethingWrong,
  },
};
