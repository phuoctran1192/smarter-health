import { configureStore } from "@reduxjs/toolkit";
import userReducer from "store/userSlice";
import documentReducer from "store/documentSlice";
import { reducer as notificationReducer } from "./notification"
export const store = configureStore({
  reducer: {
    user: userReducer,
    document: documentReducer,
    notification: notificationReducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false, }),
});