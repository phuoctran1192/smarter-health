import { useMemo } from "react";
import {
    useParams,
    useLocation,
    useMatch,
    useNavigate,
} from "react-router-dom";
import queryString from "query-string";

export const useRouter = () => {
    const params = useParams();
    const location = useLocation();
    const navigate = useNavigate();
    return useMemo(() => {
        return {
            query: {
                ...queryString.parse(location.search),
                ...params,
            },
            location,
            navigate,
        };
    }, [params, location, navigate]);
}