import { useEffect } from "react";
import { useAuth } from "./useAuth";
import { useRouter } from "./useRouter";

export const useRequireAuth = (redirectUrl = "/login") => {
    const auth = useAuth();
    const router = useRouter();

    useEffect(() => {
        if (!auth.user) {
            router.navigate(redirectUrl);
        }
    }, [auth, router]);

    return auth;
}