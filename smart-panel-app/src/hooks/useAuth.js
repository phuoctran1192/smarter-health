import React, { useState, useEffect, useContext, createContext } from 'react';
import jwt_decode from 'jwt-decode';
import AuthService from '@services/auth.service';
import { useNavigate } from 'react-router-dom';
const authContext = createContext();

export const ProvideAuth = ({ children }) => {
  const auth = useProvideAuth();
  return <authContext.Provider value={auth}>{children}</authContext.Provider>;
};

export const useProvideAuth = () => {
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState('');

  let userLocal = JSON.parse(localStorage.getItem('user'));
  const token = userLocal?.access_token;
  if (token) {
    const { exp } = jwt_decode(token);
    if (Date.now() > exp * 1000) {
      userLocal = null;
    }
  }

  const [user, setUser] = useState(userLocal);

  const login = (email, password) =>
    new Promise((resolve, reject) => {
      setLoading(true);
      AuthService.login(email, password)
        .then((data) => {
          setUser(data);
          resolve(data);
        })
        .catch((error) => {
          if (error.response) {
            reject(error.response);
            setErrors(error.response);
          }
        })
        .finally(() => setLoading(false));
    });

  const verifyToken = (payload) =>
    new Promise((resolve, reject) => {
      setLoading(true);
      AuthService.verifyToken(payload)
        .then((data) => {
          resolve(data);
        })
        .catch((error) => {
          if (error.response) {
            reject(error.response);
            setErrors(error.response);
          }
        })
        .finally(() => setLoading(false));
    });

  const activate = (payload) =>
    new Promise((resolve, reject) => {
      setLoading(true);
      AuthService.activate(payload)
        .then((data) => {
          resolve(data);
        })
        .catch((error) => {
          if (error.response) {
            setErrors(error.response);
          }
        })
        .finally(() => setLoading(false));
    });

  const register = (user) => {
    // AuthService.register(user)
  };

  const logout = () => {
    AuthService.logout();
    setUser(null);
  };

  useEffect(() => {
    const user = localStorage.getItem('user');
    const token = JSON.parse(localStorage.getItem('user'))?.content?.accessToken;

    if (token) {
      const { exp } = jwt_decode(token);
      if (Date.now() < exp * 1000) {
        setUser(user);
      } else {
        setUser(null);
      }
    }
  }, []);

  return {
    user,
    login,
    register,
    logout,
    activate,
    verifyToken,
    loading,
    errors,
  };
};

export const useAuth = () => {
  return useContext(authContext);
};
