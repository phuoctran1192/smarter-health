import App from './App';
import renderer from 'react-test-renderer';

describe('should render correctly', () => {
  it('should match snapshot', () => {
    const tree = renderer.create(<App />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
