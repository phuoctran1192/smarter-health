import * as Yup from 'yup';
import moment from 'moment';
import { DATE_PICKER_DISPLAY_FORMAT, DATE_PICKER_VALUE_FORMAT } from 'utils/constants';

const MESSAGE_MAX_DATE = (value) => "Date should not be before maximal date " + moment(value).format(DATE_PICKER_DISPLAY_FORMAT);
const MESSAGE_MIN_DATE = (value) => "Date should not be before minimal date " + moment(value).format(DATE_PICKER_DISPLAY_FORMAT);
const MESSAGE_REQUIRED = "This field is required";
const MESSAGE_INVALID_DATE = "Invalid date format, must be " + DATE_PICKER_DISPLAY_FORMAT;

export default class DateSchemaBuilder {
    schema = Yup.string().test({
        message: MESSAGE_INVALID_DATE,
        // This validation shouldn't do all the works of .required()
        test: (value) => (!value || value === "" || moment(value, DATE_PICKER_VALUE_FORMAT).isValid())
    }); // default date

    withRequired(customMessage) {
        let message = customMessage || MESSAGE_REQUIRED;
        this.schema = this.schema.required(message);
        return this;
    }

    withRequiredWhen(field, test, customMessage) {
        let message = customMessage || MESSAGE_REQUIRED;
        let currentSchema = this.schema.clone();
        this.schema = currentSchema.when(field, {
            is: (val) => test(val),
            then: currentSchema.required(message)
        })
        return this;
    }

    withMinDate(min, customMessage) {
        let minMoment = moment(min);
        if (minMoment.isValid()) {
            let message = customMessage || MESSAGE_MIN_DATE(min);
            this.schema = this.schema.test({
                message,
                test: (value) => {
                    let valueMoment = moment(value, DATE_PICKER_VALUE_FORMAT);
                    return !valueMoment.isValid() || valueMoment.isSameOrAfter(minMoment);
                }
            })
        }
        return this;
    }

    withMaxDate(max, customMessage) {
        let maxMoment = moment(max);
        if (maxMoment.isValid()) {
            let message = customMessage || MESSAGE_MAX_DATE(max);
            this.schema = this.schema.test({
                message,
                test: (value) => {
                    let valueMoment = moment(value, DATE_PICKER_VALUE_FORMAT);
                    return !valueMoment.isValid() || valueMoment.isSameOrBefore(maxMoment);
                }
            })
        }
        return this;
    }

    withAfter(field, customMessage, ignoreInvalid) {
        let message = customMessage || "Must be after " + field;
        let currentSchema = this.schema.clone();
        this.schema = currentSchema.when(field, (target) => {
            return currentSchema.test({
                message,
                test: (value) => {
                    if (ignoreInvalid && (target == null || target === "" || !moment(target, DATE_PICKER_VALUE_FORMAT).isValid())) return true;
                    if (value == null || value === "") return true;
                    let targetMoment = moment(target, DATE_PICKER_VALUE_FORMAT);
                    let valueMoment = moment(value, DATE_PICKER_VALUE_FORMAT);
                    return valueMoment.isValid() && targetMoment.isValid() && valueMoment.isAfter(targetMoment);
                }
            });
        })
        return this;
    }

    build() {
        return this.schema;
    }
}