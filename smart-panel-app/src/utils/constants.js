import moment from 'moment';

export const ROLE = {
  ADMIN: 'ROLE_ADMIN',
  DOCTOR: 'ROLE_DOCTOR',
  SUPER_ADMIN: 'ROLE_SUPER_ADMIN',
};
export const APPLICANT_STATUS = {
  CREATED: 'CREATED',
  REJECTED: 'REJECTED',
  APPROVED: 'APPROVED',
};

export const DATE_PICKER_VALUE_FORMAT = 'DD/MM/yyyy';
export const DATE_PICKER_DISPLAY_FORMAT = 'DD/MM/yyyy';
export const DATE_PICKER_DEFAULT_MIN_DATE_FORMAT = moment().year(1900).startOf('year');
export const WEEKDAY = [
  { value: 'MONDAY', label: 'Monday' },
  { value: 'TUESDAY', label: 'Tueday' },
  { value: 'WEDNESDAY', label: 'Wednesday' },
  { value: 'THURSDAY', label: 'Thursday' },
  { value: 'FRIDAY', label: 'Friday' },
  { value: 'SATURDAY', label: 'Saturday' },
  { value: 'SUNDAY', label: 'Sunday' },
];
