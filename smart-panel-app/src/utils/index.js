export default class Utils {
  static changeBool(obj) {
    var map = Object.create(null);
    map["true"] = true;
    map["false"] = false;
    // the recursive iterator
    function walker(obj) {
      var k,
        has = Object.prototype.hasOwnProperty.bind(obj);
      for (k in obj)
        if (has(k)) {
          switch (typeof obj[k]) {
            case "object":
              walker(obj[k]);
              break;
            case "string":
              if (obj[k].toLowerCase() in map)
                obj[k] = map[obj[k].toLowerCase()];
              break;
            default:
              walker(obj[k]);
          }
        }
    }
    return walker(obj);
  }
  static stringifyValueObject(obj) {
    var keys = Object.keys(obj);
    keys.forEach(function (key) {
      var value = obj[key];
      if (typeof value === "boolean") {
        obj[key] = value.toString();
      } else if (typeof value === "object") {
        Utils.stringifyValueObject(obj[key]);
      }
    });
  }
  static parseAllDateToString(obj) {
    var keys = Object.keys(obj);
    keys.forEach(function (key) {
      var value = obj[key];
      if (Object.prototype.toString.call(value) === "[object Date]") {
        obj[key] = value.toISOString().split("T")[0];
      } else if (Object.prototype.toString.call(value) === "[object Object]") {
        Utils.parseAllDateToString(obj[key]);
      } else if (Object.prototype.toString.call(value) === "[object Array]") {
        value.map((item) => Utils.parseAllDateToString(item));
      }
    });
  }
  static parseAllDateToTimeStamp(obj) {
    var keys = Object.keys(obj);
    keys.forEach(function (key) {
      var value = obj[key];
      if (Object.prototype.toString.call(value) === "[object Date]") {
        obj[key] = value.getTime();
      } else if (Object.prototype.toString.call(value) === "[object Object]") {
        Utils.parseAllDateToTimeStamp(obj[key]);
      } else if (Object.prototype.toString.call(value) === "[object Array]") {
        value.map((item) => Utils.parseAllDateToTimeStamp(item));
      }
    });
  }
  static parseAllNumberToString(obj) {
    var keys = Object.keys(obj);
    keys.forEach(function (key) {
      var value = obj[key];
      if (typeof value === "number") {
        obj[key] = value.toString();
      } else if (typeof value === "object") {
        Utils.parseAllNumberToString(obj[key]);
      }
    });
  }
  static getPartObject = (o, ...fields) => {
    return fields.reduce((a, x) => {
      if (o.hasOwnProperty(x)) a[x] = o[x];
      return a;
    }, {});
  }
}
