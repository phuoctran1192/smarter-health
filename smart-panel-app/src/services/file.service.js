import HttpService from './httpServices';
import { SMART_PANEL_PREFIX } from '@services/utils/constants';
const version = 'v1';
const getFileService = (uploadId) =>
  HttpService.get(`${SMART_PANEL_PREFIX}/${version}/document?uploadID=${uploadId}`);

const FileService = {
  getFileService,
};
export default FileService;
