export const PUBLIC_URL = process.env.PUBLIC_URL;
export const SMART_PANEL_PREFIX = `${PUBLIC_URL}/api`;
export const SMART_PANEL_ADMIN_PREFIX = `${PUBLIC_URL}/admin/api`;
export const ORIGIN_LOCATION = window && window.location && window.location.origin;
export const BASE_URL =
  process.env.REACT_APP_ARROW_URL || `${ORIGIN_LOCATION}/` || 'http://localhost:3000/';
