import HttpService from './httpServices';
import { SMART_PANEL_ADMIN_PREFIX } from './utils/constants';
const version = 'v1';

const getRoleUser = () => HttpService.get(`${SMART_PANEL_ADMIN_PREFIX}/${version}/users/userInfo`);

const RoleService = {
  getRoleUser,
};
export default RoleService;
