import HttpService from './httpServices';
import { BASE_URL, SMART_PANEL_ADMIN_PREFIX, SMART_PANEL_PREFIX } from './utils/constants';
const version = 'v1';
const getApplicants = (params) => {
  const url = `${SMART_PANEL_ADMIN_PREFIX}/${version}/users/applications?size=${params.pageSize}&page=${params.pageIndex}`;
  return HttpService.get(url);
};

const rejectApplicants = (payload) => {
  return HttpService.post(
    `${SMART_PANEL_PREFIX}/${version}/doctor/${payload.doctorId}/reject`,
    payload.data,
  );
};

const approveApplicants = (payload) => {
  const data = {
    url: `${BASE_URL}smart-panel/confirm-registration`,
  };
  return HttpService.post(
    `${SMART_PANEL_PREFIX}/${version}/doctor/${payload.doctorId}/approve`,
    data,
  );
};

const getApplicantDetail = (payload) => {
  return HttpService.get(`${SMART_PANEL_PREFIX}/${version}/doctor/${payload.doctorId}`);
};

const ApplicantService = {
  getApplicants,
  rejectApplicants,
  approveApplicants,
  getApplicantDetail,
};
export default ApplicantService;
