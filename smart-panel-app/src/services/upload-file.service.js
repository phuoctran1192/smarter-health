import { BASE_URL, SMART_PANEL_PREFIX } from './utils/constants';

export const uploadFile = (file, onProgress, onError) =>
  new Promise((res, rej) => {
    const url = `${BASE_URL}${SMART_PANEL_PREFIX}/v1/document`;
    const xhr = new XMLHttpRequest();
    xhr.open('POST', url);

    xhr.onload = (evt) => {
      const resp = JSON.parse(xhr.responseText);
      if (resp[0]) {
        res(resp[0]);
      } else if (resp && resp.status === 413) {
        onError('errorMaxSizeExceed');
      } else {
        res(null);
      }
    };
    xhr.onerror = (evt) => {
      rej(evt);
      onError(evt.type);
    };
    xhr.upload.onprogress = (event) => {
      if (event.lengthComputable) {
        const percentage = (event.loaded / event.total) * 100;
        onProgress(Math.round(percentage));
      }
    };

    const formData = new FormData();
    formData.append('files', file);

    xhr.send(formData);
  });
