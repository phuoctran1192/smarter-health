import HttpService from './httpServices';
import { SMART_PANEL_PREFIX } from '@services/utils/constants';
const version = 'v1';
const getMeta = (endpoint) => HttpService.get(`${SMART_PANEL_PREFIX}/${version}/metadata/${endpoint}`);

const MetaService = {
  getMeta,
};
export default MetaService;
