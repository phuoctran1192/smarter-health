import HttpService from './httpServices';
import { SMART_PANEL_PREFIX } from '@services/utils/constants';
import { SMART_PANEL_ADMIN_PREFIX } from './utils/constants';
const version = 'v1';

const register = (formData) => {
  return HttpService.publicPost(`${SMART_PANEL_PREFIX}/v1/doctor`, formData);
};
const login = (email, password) => {
  const params = new URLSearchParams();
  params.append('username', email);
  params.append('password', password);
  return HttpService.publicPost(`/auth/${version}/smart-panel/token`, params, {
    'Content-Type': 'application/x-www-form-urlencoded',
  }).then((response) => {
    if (response.data.access_token) {
      localStorage.setItem('user', JSON.stringify(response.data));
    }
    return response.data;
  });
};

const activate = (data) => {
  return HttpService.publicPost(`${SMART_PANEL_PREFIX}/${version}/auth/submitPassword`, data);
};

const verifyToken = (params) => {
  return HttpService.publicGet(
    `${SMART_PANEL_PREFIX}/${version}/auth/checkRegistrationToken/${params.token}`,
  );
};

const logout = () => {
  localStorage.removeItem('user');
};

const AuthService = {
  register,
  login,
  logout,
  activate,
  verifyToken,
};
export default AuthService;
