import HttpService from './httpServices';
import { BASE_URL, SMART_PANEL_ADMIN_PREFIX, SMART_PANEL_PREFIX } from './utils/constants';
const version = 'v1';
const getUsers = (params) => {
  const url = `${SMART_PANEL_ADMIN_PREFIX}/${version}/users/admins?size=${params.pageSize}&page=${params.pageIndex}`;
  return HttpService.get(url);
};

const UserService = {
  getUsers,
};
export default UserService;
