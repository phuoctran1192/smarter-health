import HttpService from './httpServices';
import { SMART_PANEL_PREFIX } from '@services/utils/constants';

const version = 'v1';
const getDoctorProfile = () => HttpService.get(`${SMART_PANEL_PREFIX}/${version}/doctor/`);
const getDoctorProfileById = (id) => HttpService.get(`${SMART_PANEL_PREFIX}/${version}/doctor/${id}`);
const getDoctorWorkBackground = () => HttpService.get(`${SMART_PANEL_PREFIX}/${version}/doctor/workBackground`);
const getDoctorAvatar = () => HttpService.get(`${SMART_PANEL_PREFIX}/${version}/doctor/profilePhoto`);
const getDoctorProficiency = () => HttpService.get(`${SMART_PANEL_PREFIX}/${version}/doctor/languages`);

const updateDoctorProfile = (data) => HttpService.patch(`${SMART_PANEL_PREFIX}/${version}/doctor/personalInformation`, data);
const updateDoctorProfessional = (data) => HttpService.patch(`${SMART_PANEL_PREFIX}/${version}/doctor/professionalInformation`, data);
const updateDoctorWorkBackground = (workBackgroundData) => HttpService.patch(`${SMART_PANEL_PREFIX}/${version}/doctor/workBackground`, workBackgroundData);
const updateDoctorAvatar = (uploadID) => HttpService.patch(`${SMART_PANEL_PREFIX}/${version}/doctor/profilePhoto`, uploadID);
const updateDoctorProfessionalDocument = (data) => HttpService.patch(`${SMART_PANEL_PREFIX}/${version}/doctor/professionalInformation/documents`, data);
const updateDoctorProficiency = (data) => HttpService.patch(`${SMART_PANEL_PREFIX}/${version}/doctor/languages`, data);

const DoctorService = {
  getDoctorProfile,
  getDoctorProfileById,
  getDoctorWorkBackground,
  getDoctorAvatar,
  updateDoctorProfile,
  updateDoctorProfessional,
  updateDoctorWorkBackground,
  updateDoctorAvatar,
  updateDoctorProfessionalDocument,
  getDoctorProficiency,
  updateDoctorProficiency
};
export default DoctorService;
