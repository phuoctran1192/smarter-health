import Container from '@material-ui/core/Container';
import { HTTPProvider } from '@providers/HTTPProvider';

export const MasterLayout = ({ ...props }) => {
  return (
    <HTTPProvider>
      <Container>{props.children}</Container>
    </HTTPProvider>
  );
};
