import { makeStyles } from '@material-ui/core';
import { MiniDrawer, SPAppBar } from '../components/Elements';
import clsx from 'clsx';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { HTTPProvider } from '@providers/HTTPProvider'

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    backgroundColor: 'aliceblue',
  },
  content: {
    marginTop: '53px',
    minHeight: 'calc(100vh - 53px)',
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));
export const AdminLayout = ({ ...props }) => {
  const classes = useStyles();
  const navigate = useNavigate();
  const [open, setOpen] = useState(false);
  const menus = [
    {
      text: 'Applicant',
      icon: <AssignmentIndIcon />,
      onClick: () => navigate('/smart-panel/admin/applicant'),
    },
    {
      text: 'Admin Users',
      icon: <AssignmentIndIcon />,
      onClick: () => navigate('/smart-panel/admin/user'),
    },
  ];

  const handleOnClose = () => {
    setOpen(false);
  };

  return (
    <HTTPProvider>
      <div className={classes.container}>
        <SPAppBar drawerOpen={open} onClickMenu={() => setOpen(true)} />
        <MiniDrawer items={menus} open={open} onClose={handleOnClose} />
        <main
          className={clsx(classes.content, {
            [classes.contentShift]: open,
          })}
        >
          <div>{props.children}</div>
        </main>
      </div>
    </HTTPProvider>
  );
};
