import { createTheme } from '@material-ui/core';
export const SPTheme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1140,
      xl: 1536,
    },
  },
  palette: {
    primary: {
      main: '#4024CD',
      lightGrey: '#C4C4C4',
      black: '#010101',
    },
    error: {
      main: '#ED5E58', // change the error color
    },
    admin: {
      main: '#03ACEF',
    },
  },
  typography: {
    fontFamily: ['Poppins', 'sans-serif'].join(','),
    fontSize: 16,
    h3: {
      fontFamily: ['Playfair Display', 'serif'].join(','),
      fontSize: 42,
      lineHeight: 'initial',
    },
    h4: {
      fontFamily: ['Playfair Display', 'serif'].join(','),
      fontSize: 30,
      lineHeight: '40px',
      fontWeight: 700,
    },
    subtitle1: {
      fontFamily: ['Poppins', 'serif'].join(','),
      fontSize: 22,
      fontWeight: 600,
      lineHeight: '125%',
    },
    subtitle2: {
      fontFamily: ['Poppins', 'serif'].join(','),
      fontSize: 16,
      fontWeight: 600,
      lineHeight: '24px',
    },
    body1: {
      fontSize: 13,
      lineHeight: '20px',
    },
  },
  overrides: {
    MuiInputLabel: {
      root: {
        fontSize: '16px',
      },
    },
    MuiTableCell: {
      root: {
        fontSize: '16px',
      },
    },
  },
});
