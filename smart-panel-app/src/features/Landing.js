import React from 'react';
import { Link } from 'react-router-dom';
import landing from '@assets/images/landing.jpg';

export default function Landing() {
  return (
    <div className="landing-page">
      <img src={landing} alt="smart panel" className="landing-page-ill" />
      <div className="landing-page-content">
        <div className="heading-2 landing-page-heading">Important Note</div>
        <div className="body-2 mt-20 mb-40 landing-page-text">
          Health Connective is open only to doctors who have been granted{' '}
          <span className="text-primary">Full Registration</span> by the
          <span className="text-primary"> Singapore Medical Council.</span>
          <br />
          Doctors in both public and private sectors are welcome to participate.
        </div>
        <div className="landing-page-button">
          <Link to="/smart-panel/login" className="button text-decoration-none landing-page-button-home">
            Back to Homepage
          </Link>
          <Link
            to="/smart-panel/signup"
            className="button text-decoration-none landing-page-button-signup"
          >
            Signup
          </Link>
        </div>
      </div>
    </div>
  );
}
