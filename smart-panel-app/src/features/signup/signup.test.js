import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { act } from "react-dom/test-utils";
import { MemoryRouter } from "react-router";
import { Signup, Wizard } from "./index";
import { Field } from "formik";

test("wizard should navigate, submit on last step", async () => {
  const handleSubmit = jest.fn();
  const MockWizard = () => {
    return (
      <Wizard
        editStep={{ value: 0 }}
        isSubmitted={false}
        initialValues={{
          firstName: "",
          lastName: "",
        }}
        onSubmit={handleSubmit}
      >
        <div>
          <h2>Step 1</h2>
          <label htmlFor="firstName">First Name</label>
          <Field
            data-testid="mockFirstName"
            id="firstName"
            name="firstName"
            placeholder="first name"
          />
        </div>

        <div>
          <h2>Step 2</h2>
          <label htmlFor="lastName">Last Name</label>
          <Field
            data-testid="mockLastName"
            id="lastName"
            name="lastName"
            placeholder="last name"
          />
        </div>
      </Wizard>
    );
  };
  const container = (val) => {
    return (
      <MemoryRouter>
        <MockWizard editStep={{ value: val }} />
      </MemoryRouter>
    );
  };
  render(container());

  // should move to next step
  await act(async () => {
    const fN = await screen.findByTestId("mockFirstName");
    userEvent.type(fN, "first name");
    const next = await screen.getByRole("button", { name: /Next/i });
    userEvent.click(next);
  });
  await waitFor(() => expect(getField("mockLastName")).toBeTruthy());

  // should move to previous step
  await act(async () => {
    const back = await screen.getByRole('button', {  name: /back/i});
    userEvent.click(back);
  });
  await waitFor(() => expect(getField("mockFirstName")).toBeTruthy());

  // should submit on last step with typed value
  await act(async () => {
    const next = await screen.getByRole("button", { name: /Next/i });
    userEvent.click(next);
    const lN = await screen.findByTestId("mockLastName");
    userEvent.type(lN, "last name");
    const submit = await screen.getByRole("button", { name: /Submit/i });
    userEvent.click(submit);
  });
  await waitFor(() => expect(handleSubmit).toHaveBeenCalledTimes(1));
  await waitFor(() =>
    expect(handleSubmit).toHaveBeenCalledWith(
      {
        firstName: "first name",
        lastName: "last name",
      },
      expect.anything()
    )
  );

  // should move to assign step
  const { rerender } = render(container(1));
  rerender(container(1));
  expect(screen.getByTestId("mockLastName")).toBeTruthy();
});

function getField(id) {
  return screen.getByTestId(id);
}

function selectGender() {
  return screen.getByRole("radio", { name: /female/i });
}

function selectDob() {
  return () => {
    screen.getByPlaceholderText("DD/MM/YYYY");
    screen.getByRole("button", {
      name: /choose thursday, november 4th, 2021/i,
    });
  };
}

function selectIdType() {
  const dropdown = screen.getByTestId("smp-identityType");
  userEvent.click(dropdown);
  userEvent.selectOptions(dropdown, ["Option 1"], { bubbles: true });
}
