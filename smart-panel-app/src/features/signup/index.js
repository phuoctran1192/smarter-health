import React, { useState, useEffect } from 'react';
import { Form, Formik } from 'formik';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import AuthService from 'services/auth.service';
import { SideBar } from '@features/signup/sidebar';
import Utils from 'utils';
import {
  Step1Personal,
  personalInforInitialValues,
  personalInforValidationSchema,
} from '@features/signup/steps/step1-personal';
import {
  Step2Professional,
  professionalInforInitialValues,
  professionalInforValidationSchema,
} from '@features/signup/steps/step2-professional';
import { Step3Document } from '@features/signup/steps/step3-document';
import { Step4Preview } from '@features/signup/steps/step4-preview';
import { Step5ThankYou } from '@features/signup/steps/step5-thankyou';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  submitErrBox: {
    borderRadius: '12px',
    padding: '20px 20px',
    border: '1px solid red',
    position: 'relative',
    color: 'red',
  },
});
export const Wizard = ({ children, initialValues, onSubmit, onBack, editStep, submitErrMsg }) => {
  const classes = useStyles();
  const [stepNumber, setStepNumber] = useState(0);
  const steps = React.Children.toArray(children);
  const [snapshot, setSnapshot] = useState(initialValues);

  const step = steps[stepNumber];
  const totalSteps = steps.length;
  const isLastStep = stepNumber === totalSteps - 1;

  const next = (values) => {
    setSnapshot(values);
    setStepNumber(Math.min(stepNumber + 1, totalSteps - 1));
  };

  const previous = (values) => {
    onBack && onBack();
    setSnapshot(values);
    setStepNumber(Math.max(stepNumber - 1, 0));
  };

  const handleSubmit = async (values, bag) => {
    if (step.props.onSubmit) {
      await step.props.onSubmit(values, bag);
    }
    if (isLastStep) {
      return onSubmit(values, bag);
    } else {
      bag.setTouched({});
      next(values);
    }
  };

  useEffect(() => {
    setStepNumber(editStep.value);
  }, [editStep]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [stepNumber, submitErrMsg])

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <div className="container-1140">
          <h3
            className="smp-signup-breadcrumb"
            data-testid="smp-signup-breadcrumb"
            style={{ marginBottom: '50px' }}
          >
            {`Homepage > Sign Up > 
            ${{
                0: 'Personal Information and Contact Information',
                1: 'Professional Information, Qualification(s) and Practicing Certificate',
                2: 'Upload Supporting Document(s)',
                3: 'Preview',
              }[stepNumber]
              }`}
          </h3>
        </div>
      </Grid>
      {submitErrMsg && submitErrMsg !== "" && <Grid item xs={12}>
        <div className="container-930">
          <p className={classes.submitErrBox}>{submitErrMsg}</p>
        </div>
      </Grid>}
      <Grid item xs={12}>
        <SideBar data={{ stepNumber, totalSteps }} />
      </Grid>
      <Grid item xs={12}>
        <div className="container-930">
          <Formik
            initialValues={snapshot}
            onSubmit={handleSubmit}
            validationSchema={step.props.validationSchema}
          >
            {(formik) => (
              <Form>
                {step}
                <div className="signup-buttons">
                  {stepNumber === 0 && (
                    <Link
                      className="signup-button backButton text-decoration-none"
                      to="/smart-panel"
                    >
                      Back
                    </Link>
                  )}
                  {stepNumber > 0 && (
                    <button
                      onClick={() => previous(formik.values)}
                      type="button"
                      className="signup-button"
                    >
                      {'Back'}
                    </button>
                  )}
                  <button
                    data-testid="smp-submit"
                    className="signup-button nextButton"
                    disabled={formik.isSubmitting}
                    type="submit"
                  >
                    {isLastStep ? 'Submit' : 'Next'}
                  </button>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </Grid>
    </Grid>
  );
};

export const Signup = () => {
  const [isSubmitted, setSubmitted] = useState(false);
  const [editStep, setEditStep] = useState({ value: 0 });
  const [submitErrMsg, setSubmitErrMsg] = useState("");

  const setStep = (s) => {
    setEditStep({ value: s });
  };
  const formSubmit = async (values) => {
    setSubmitErrMsg("");
    Utils.changeBool(values);
    Utils.parseAllDateToString(values);
    const formData = { ...values };
    AuthService.register(formData).then((res) => {
      setSubmitted(true);
    }).catch((err) => {
      var msg = "Failed to submit form. ";
      if (err.response && err.response.data) {
        msg += err.response.data.message;
      }
      setSubmitErrMsg(msg);
    });
  };

  // clear error submit message when back to previous steps
  useEffect(() => {
    setSubmitErrMsg("");
  }, [editStep]);

  return (
    <>
      {!isSubmitted ? (
        <div className="signup ">
          <Wizard
            editStep={editStep}
            isSubmitted={isSubmitted}
            initialValues={{
              ...personalInforInitialValues,
              ...professionalInforInitialValues,
            }}
            onSubmit={formSubmit}
            onBack={() => setSubmitErrMsg("")}
            submitErrMsg={submitErrMsg}
          >
            <Step1Personal validationSchema={personalInforValidationSchema} />
            <Step2Professional validationSchema={professionalInforValidationSchema} />
            <Step3Document />
            <Step4Preview setStep={setStep} />
          </Wizard>
        </div>
      ) : (
        <Step5ThankYou />
      )}
    </>
  );
};
