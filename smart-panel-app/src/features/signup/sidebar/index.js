import { Typography } from '@material-ui/core';
import clsx from 'clsx';

export const SideBar = ({ data }) => {
  const currentStep = data.stepNumber + 1;
  const stepClass = (s) => {
    if(currentStep === s) {
      return "active"
    } else if (currentStep > s) {
      return "finished"
    } else {
      return ""
    }
  }

  return (
    <div className="smp-signup-sidebar text-center container-930">
      <Typography component="h4" variant="h4">
        Complete your Application
      </Typography>
      <p>Fill up the required fields and you're all set!</p>
      <div className="step-wrapper">
        <div className="step-progress" data-process={currentStep/data.totalSteps*100}></div>
        <div className={clsx("step-title", `${stepClass(1)}`)}>
          <span className="step-number">1</span>
          <div className="step-text">Personal Information</div>
        </div>
        <div className={clsx("step-title", `${stepClass(2)}`)}>
          <span className="step-number">2</span>
          <div className="step-text">Professional Qualification(s)</div>
        </div>
        <div className={clsx("step-title", `${stepClass(3)}`)}>
          <span className="step-number">3</span>
          <div className="step-text">Upload Document(s)</div>
        </div>
        <div className={clsx("step-title", `${stepClass(4)}`)}>
          <span className="step-number">4</span>
          <div className="step-text">Preview Application</div>
        </div>
      </div>
    </div>
  );
};
