import React from 'react';
import { FieldArray } from 'formik';
import Grid from '@material-ui/core/Grid';
import Textfield from 'components/FormsUI/Textfield';
import { YearPicker } from 'components/Inputs/YearPicker';

export const Qualification = ({ addQualification }) => {
  return (
    <FieldArray name="qualifications">
      {(props) => {
        const { push, remove, form } = props;
        const { values } = form;
        const { qualifications } = values;
        const qualification = { name: '', year: '', institution: '' };
        addQualification = () => {
          push(qualification);
        };
        return (
          <>
            <Grid item xs={12}>
              <div className="flex-center-between addable-section-title">
                <div className="formTitle">Qualification(s)</div>
                <button type="button" className="add-group" onClick={addQualification}>
                  <span className="plus-icon">plus</span>
                  Add Qualification
                </button>
              </div>
            </Grid>
            {qualifications.length > 0 &&
              qualifications.map((item, index) => (
                <Grid container key={index} spacing={2}>
                  <Grid item xs={6}>
                    <p className="subtitle3">{index + 1}. Qualification</p>
                  </Grid>
                  <Grid item xs={6}>
                    {qualifications.length > 1 && (
                      <button
                        type="button"
                        className="addable-section-remove"
                        onClick={() => remove(index)}
                      >
                        Delete
                      </button>
                    )}
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <Textfield name={`qualifications.[${index}].name`} label="Academic Title*" />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <YearPicker name={`qualifications.[${index}].year`} label="Year*" />
                  </Grid>
                  <Grid item xs={12}>
                    <Textfield
                      name={`qualifications.[${index}].institution`}
                      label="Institution Name*"
                    />
                  </Grid>
                  <div style={{ display: 'block', width: '100%', marginBottom: '40px' }}></div>
                </Grid>
              ))}
          </>
        );
      }}
    </FieldArray>
  );
};
