import React, { useEffect } from 'react';
import { useFormikContext } from 'formik';
import * as Yup from 'yup';
import Grid from '@material-ui/core/Grid';
import { Qualification } from '@features/signup/steps/step2-professional/qualification/Qualification';
import { Specialist } from '@features/signup/steps/step2-professional/specialist/Specialist';
import { makeStyles, Typography } from '@material-ui/core';
import Textfield from 'components/FormsUI/Textfield';
import CustomizedRadios from 'components/FormsUI/RadioButton';
import { DatePicker } from 'components/Inputs/DatePicker';
import DateSchemaBuilder from 'utils/formik/DateSchemaBuilder';

const useStyles = makeStyles({
  formTitle: {
    fontSize: 22,
    lineHeight: '33px',
    borderBottom: '1px solid #4126CF',
    marginBottom: 20,
  },
  topSpace: {
    marginTop: 44,
  },
  botSpace: {
    marginBottom: 18,
  },
});

export const professionalInforValidationSchema = Yup.object().shape({
  mcrNumber: Yup.string().required('This field is required'),
  dateOfFullRegistration: new DateSchemaBuilder().withRequired().build(),
  qualifications: Yup.array()
    .of(
      Yup.object().shape({
        name: Yup.string().required('This field is required'),
        year: Yup.string()
          .required('This field is required')
          .matches(/^[0-9]+$/, 'Must be only digits')
          .min(4, 'Must be 4 digits')
          .test({
            message: "Min value is 1900",
            test: (value) => {
              let yearValue = parseInt(value, 10);
              return !isNaN(yearValue) && yearValue >= 1900;
            },
          }),
        institution: Yup.string().required('This field is required'),
      }),
    )
    .required('Must have asleast one')
    .min(1, 'Minimum of 1 qualification'),
  practicingCertificate: Yup.object().shape({
    startDate: new DateSchemaBuilder().withRequired().build(),
    endDate: new DateSchemaBuilder().withRequired()
      .withAfter('startDate', 'End date must be after start date', true)
      .build(),
    isSpecialist: Yup.string(),
    specialities: Yup.array().when('isSpecialist', {
      is: 'true',
      then: Yup.array().of(
        Yup.object().shape({
          entryDate: new DateSchemaBuilder().withRequired().build(),
          speciality: Yup.string().required('This field is required'),
          documents: Yup.array(),
        }),
      ),
    }),
    isFamilyPhysicianRegister: Yup.string().required('This field is required'),
    familyPhysicianRegisterEntryDate: new DateSchemaBuilder()
      .withRequiredWhen(
        'isFamilyPhysicianRegister',
        (val) => val === true || val === 'true',
      )
      .build(),
    isDesignatedWorkplaceDoctor: Yup.string(),
    practicingCertificateDocuments: Yup.array(),
    physicianPracticingCertificateDocuments: Yup.array(),
    workplaceDoctorPracticingCertificateDocuments: Yup.array(),
  }),
});

export const professionalInforInitialValues = {
  mcrNumber: '',
  dateOfFullRegistration: '',
  qualifications: [
    {
      name: '',
      year: '',
      institution: '',
    },
  ],
  practicingCertificate: {
    startDate: '',
    endDate: '',
    isSpecialist: 'false',
    isFamilyPhysicianRegister: 'false',
    familyPhysicianRegisterEntryDate: '',
    isDesignatedWorkplaceDoctor: 'false',
    specialities: [],
  },
};

export const Step2Professional = () => {
  const classes = useStyles();
  const { values, setFieldValue } = useFormikContext();
  useEffect(() => {
    if ((values.practicingCertificate.isFamilyPhysicianRegister).toString() === "false") {
      setFieldValue('practicingCertificate.familyPhysicianRegisterEntryDate', "")
    }
    if ((values.practicingCertificate.isSpecialist).toString() === "false") {
      setFieldValue('practicingCertificate.specialities', [])
    }
  }, [values.practicingCertificate.isFamilyPhysicianRegister, values.practicingCertificate.isSpecialist]);
  return (
    <>
      <div className="box mb-40">
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography component="p" variant="subtitle1" className={classes.formTitle}>
              Professional Information
            </Typography>
          </Grid>
          <Grid item xs={12} className={classes.botSpace}>
            <Textfield name="mcrNumber" label="Medical Council Registration (MCR) No.*" />
          </Grid>
          <Grid item xs={12} className={classes.botSpace}>
            <DatePicker name="dateOfFullRegistration" label="Date of Full Registration*" />
          </Grid>
        </Grid>
      </div>
      <div className="box mb-40">
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Qualification />
          </Grid>
        </Grid>
      </div>
      <div className="box mb-40">
        <Grid container spacing={2}>
          <Grid item xs={12} className={classes.botSpace}>
            <label className="form-label">Are you on the family physician register?*</label>
            <CustomizedRadios
              name="practicingCertificate.isFamilyPhysicianRegister"
              options={{
                true: 'Yes',
                false: 'No',
              }}
            />
          </Grid>
          <Grid item xs={12} className={classes.botSpace}>
            {values.practicingCertificate.isFamilyPhysicianRegister === "true" &&
              <DatePicker
                name="practicingCertificate.familyPhysicianRegisterEntryDate"
                label="Entry Date*"
              />
            }
          </Grid>
          <Grid item xs={12} className={classes.botSpace}>
            <label className="form-label">Are you a designated workplace doctor?*</label>
            <CustomizedRadios
              name="practicingCertificate.isDesignatedWorkplaceDoctor"
              options={{
                true: 'Yes',
                false: 'No',
              }}
            />
          </Grid>
        </Grid>
      </div>
      <div className="box mb-40">
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography component="p" variant="subtitle1" className={classes.formTitle}>
              Practicing Certificate
            </Typography>
          </Grid>
          <Grid item xs={12} className={classes.botSpace}>
            <DatePicker name="practicingCertificate.startDate" label="Start Date*" />
          </Grid>
          <Grid item xs={12} className={classes.botSpace}>
            <DatePicker name="practicingCertificate.endDate" label="End Date*" />
          </Grid>
          <Grid item xs={12} className={classes.botSpace}>
            <label className="form-label">Currently practicing as a*</label>
            <CustomizedRadios
              name="practicingCertificate.isSpecialist"
              options={{
                true: 'Specialist',
                false: 'General Practitioner',
              }}
            />
          </Grid>
        </Grid>
      </div>
      {(values.practicingCertificate.isSpecialist).toString() === 'true' && (
        <div className="box mb-40">
          <Grid item xs={12} className={classes.botSpace}>
            <Specialist />
          </Grid>
        </div>
      )}
    </>
  );
};
