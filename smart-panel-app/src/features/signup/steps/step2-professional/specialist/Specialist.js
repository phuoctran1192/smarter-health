import React, { useEffect, useState } from 'react';
import { FieldArray } from 'formik';
import Grid from '@material-ui/core/Grid';
import MetaService from '@services/meta.service';
import CustomSelect from 'components/FormsUI/Select';
import { DatePicker } from 'components/Inputs/DatePicker';

export const Specialist = () => {
  const [specialityList, setSpecialityList] = useState([]);
  useEffect(() => {
    MetaService.getMeta('specialities')
      .then((res) => {
        if (res?.code && res?.data) {
          const listWithLabel = res.data.reduce((acc, cur) => ({ ...acc, [cur.id]: cur.name }), {});
          setSpecialityList(listWithLabel);
        }
      })
      .catch((err) => console.log(err));
  }, []);
  return (
    <FieldArray name="practicingCertificate.specialities">
      {(props) => {
        const { push, remove, form } = props;
        const { values } = form;
        const { practicingCertificate } = values;
        return (
          <>
            <Grid item xs={12}>
              <div className="flex-center-between addable-section-title">
                <div className="formTitle">Specialty(s)</div>
                <button
                  type="button"
                  className="add-group"
                  onClick={() => {
                    push({ speciality: '', entryDate: '', documents: [] });
                  }}
                >
                  <span className="plus-icon">plus</span>
                  Add Specialty
                </button>
              </div>
            </Grid>
            {practicingCertificate.specialities.length > 0 &&
              practicingCertificate.specialities.map((item, index) => (
                <Grid container spacing={2} key={index}>
                  <Grid item xs={6}>
                    <p className="subtitle3">{index + 1}. Specialty</p>
                  </Grid>
                  <Grid item xs={6}>
                    {practicingCertificate.specialities.length > 1 && (
                      <button
                        type="button"
                        className="addable-section-remove"
                        onClick={() => remove(index)}
                      >
                        Delete
                      </button>
                    )}
                  </Grid>
                  <Grid item xs={12}>
                    <CustomSelect
                      name={`practicingCertificate.specialities.[${index}].speciality`}
                      options={specialityList}
                      label="Specialty/Sub-Specialty*"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <DatePicker
                      name={`practicingCertificate.specialities.[${index}].entryDate`}
                      label="Entry Date*"
                    />
                  </Grid>
                  <div style={{ display: 'block', width: '100%', marginBottom: '40px' }}></div>
                </Grid>
              ))}
          </>
        );
      }}
    </FieldArray>
  );
};
