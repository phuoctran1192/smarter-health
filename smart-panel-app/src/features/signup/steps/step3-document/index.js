import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { MultipleFileUploadField } from 'components/upload-file/MultipleFileUploadField';
import { useFormikContext } from 'formik';
import { makeStyles, Typography } from '@material-ui/core';
import MetaService from '@services/meta.service';
import { useSelector } from 'react-redux';

const useStyles = makeStyles({
  headingText: {
    marginTop: 6,
    marginBottom: 33,
  },
  sectionTitle: {
    marginTop: 40,
    marginBottom: 20,
  },
});

export const Step3Document = () => {
  const classes = useStyles();
  const { values } = useFormikContext();
  const [specialityList, setSpecialityList] = useState([]);
  const document = useSelector((state) => state.document);
  useEffect(() => {
    MetaService.getMeta('specialities')
      .then((res) => {
        if (res?.code && res?.data) {
          const listWithLabel = res.data.reduce((acc, cur) => ({ ...acc, [cur.id]: cur.name }), {});
          setSpecialityList(listWithLabel);
        }
      })
      .catch((err) => console.log(err));
  }, []);
  return (
    <div className="box">
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography component="p" variant="subtitle1">
            Upload Supporting Document(s)
          </Typography>
          <Typography component="p" variant="body1" className={classes.headingText}>
            We will need to validate your information. Please upload a valid practicing certificate
            and other supporting document(s). Files should be less than 5MB (JPG, JPEG, PNG, DOC, DOCX or PDF).
          </Typography>
          <div className="break"></div>
        </Grid>
        <Grid item xs={12}>
          <Typography component="p" variant="subtitle2" className={classes.sectionTitle}>
            Practicing Certificate
          </Typography>
          <MultipleFileUploadField
            name="practicingCertificate.practicingCertificateDocuments"
            uploaded={document['practicingCertificate.practicingCertificateDocuments']}
          />
          <div className="break" style={{ marginTop: '50px' }}></div>
        </Grid>
        {/* specialities */}
        {values.practicingCertificate.specialities.length > 0 &&
          (values.practicingCertificate.isSpecialist).toString() === 'true' &&
          values.practicingCertificate.specialities.map((spec, idx) => {
            return (
              <Grid key={idx} item xs={12}>
                <Typography component="p" variant="subtitle2" className={classes.sectionTitle}>
                  Specialty:{' '}
                  {(specialityList && specialityList[spec.speciality]) || spec.speciality}
                </Typography>
                <MultipleFileUploadField
                  name={`practicingCertificate.specialities[${idx}].documents`}
                  uploaded={document[`practicingCertificate.specialities[${idx}].documents`]}
                />
                <div className="break" style={{ marginTop: '50px' }}></div>
              </Grid>
            );
          })}
        {/* specialities */}
        {(values.practicingCertificate.isFamilyPhysicianRegister).toString() === 'true' && (
          <Grid item xs={12}>
            <Typography component="p" variant="subtitle2" className={classes.sectionTitle}>
              Family Physician Register
            </Typography>
            <MultipleFileUploadField
              name="practicingCertificate.physicianPracticingCertificateDocuments"
              uploaded={document['practicingCertificate.physicianPracticingCertificateDocuments']}
            />
            <div className="break" style={{ marginTop: '50px' }}></div>
          </Grid>
        )}
        {(values.practicingCertificate.isDesignatedWorkplaceDoctor).toString() === 'true' && (
          <Grid item xs={12}>
            <Typography component="p" variant="subtitle2" className={classes.sectionTitle}>
              Designated Workplace Doctor
            </Typography>
            <MultipleFileUploadField
              name="practicingCertificate.workplaceDoctorPracticingCertificateDocuments"
              uploaded={
                document['practicingCertificate.workplaceDoctorPracticingCertificateDocuments']
              }
            />
            <div style={{ marginBottom: '30px' }}></div>
          </Grid>
        )}
      </Grid>
    </div>
  );
};
