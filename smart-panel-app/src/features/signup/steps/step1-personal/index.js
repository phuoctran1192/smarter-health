import React, { useEffect, useState } from 'react';
import { useField } from 'formik';
import * as Yup from 'yup';
import { makeStyles, Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import MetaService from '@services/meta.service';
import CustomSelect from 'components/FormsUI/Select';
import Textfield from 'components/FormsUI/Textfield';
import CustomizedRadios from 'components/FormsUI/RadioButton';
import { FormItem } from 'components/Forms/FormItem';
import PhoneInput from 'components/FormsUI/PhoneInput';
import { DatePicker } from 'components/Inputs/DatePicker';
import moment from 'moment';
import DateSchemaBuilder from 'utils/formik/DateSchemaBuilder';

const CustomTooltip = withStyles({
  tooltip: {
    backgroundColor: '#F5F3FF',
    color: '#4024CD',
    maxWidth: 300,
    fontSize: '13px',
    lineHeight: '150%',
    borderRadius: '4px',
    padding: '15px',
  },
})(Tooltip);
const useStyles = makeStyles({
  formTitle: {
    borderBottom: '1px solid #4126CF',
    marginBottom: 20,
  },
  topSpace: {
    marginTop: 44,
  },
  botSpace: {
    marginBottom: 18,
  },
});
export const personalInforValidationSchema = Yup.object().shape({
  firstName: Yup.string().required('This field is required'),
  lastName: Yup.string().required('This field is required'),
  gender: Yup.string().required('This field is required'),
  dob: new DateSchemaBuilder()
    .withRequired()
    .withMaxDate(moment().subtract(24, 'years').endOf('year'), 'The minimum age to be a doctor is 24 years old')
    .build(),
  identityType: Yup.string().required('This field is required'),
  otherIdentity: Yup.string()
    .nullable()
    .when('identityType', {
      is: '4',
      then: Yup.string().nullable().required('This field is required'),
    }),
  identityNumber: Yup.string().required('This field is required'),
  phoneNumber: Yup.string().required('This field is required'),
  email: Yup.string().email('Please choose a valid email').required('This field is required'),
  address: Yup.object().shape({
    block: Yup.string().required('This field is required'),
    street: Yup.string().required('This field is required'),
    building: Yup.string(),
    level: Yup.string(),
    number: Yup.string(),
    postalCode: Yup.string().required('This field is required'),
  }),
});

export const personalInforInitialValues = {
  email: '',
  firstName: '',
  lastName: '',
  gender: '1',
  dob: '',
  identityType: '',
  otherIdentity: '',
  identityNumber: '',
  phoneNumber: '',
  address: {
    block: '',
    street: '',
    building: '',
    level: '',
    number: '',
    postalCode: '',
    city: '',
    state: '',
    country: '',
  },
  practicingPlaces: [],
  accreditedHospitals: [],
};

export const Step1Personal = () => {
  const classes = useStyles();
  const [idType] = useField('identityType');
  const genderOptions = {
    1: 'Male',
    2: 'Female',
  };
  const [identityList, setIdentityList] = useState([]);
  useEffect(() => {
    MetaService.getMeta('identity-types')
      .then((res) => {
        if (res?.code && res?.data) {
          const identityListWithLabel = res.data.reduce(
            (acc, cur) => ({ ...acc, [cur.id]: cur.name }),
            {},
          );
          setIdentityList(identityListWithLabel);
        }
      })
      .catch((err) => console.log(err));
  }, []);
  return (
    <div>
      <div className="box mb-40">
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography component="p" variant="subtitle1" className={classes.formTitle}>
              Personal Information
            </Typography>
          </Grid>
          <Grid item xs={12} md={6} className={classes.botSpace}>
            <Textfield name="firstName" label="First Name*" />
          </Grid>
          <Grid item xs={12} md={6} className={classes.botSpace}>
            <Textfield name="lastName" label="Last Name*" />
          </Grid>
          <Grid item xs={12} className={classes.botSpace}>
            <div className="flex-center-between">
              <label className="form-label">Gender</label>
              <CustomTooltip title="Medical conditions are very personal in nature and highly influenced by sociodemographic factors e.g. religion. Some patients may have specific preferences.">
                <span className="tooltip-archor">Why are we asking?</span>
              </CustomTooltip>
            </div>
            <CustomizedRadios name="gender" options={genderOptions} />
          </Grid>
          <Grid item xs={12} className={classes.botSpace}>
            <DatePicker name="dob" label="Date of Birth*" maxDate={moment().subtract(24, 'years').endOf('year')} />
          </Grid>
          <Grid item xs={12} className={classes.botSpace}>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6}>
                <CustomSelect
                  data-testid="smp-identityType"
                  id="identityType"
                  name="identityType"
                  options={identityList}
                  label="Identification Document (Type)*"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                {idType.value.toString() === '4' && (
                  <Textfield name="otherIdentity" label="Please specify*" />
                )}
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} md={6} className={classes.botSpace}>
            <Textfield name="identityNumber" label="Identification Document No.*" />
          </Grid>
        </Grid>
      </div>
      <div className="box">
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography component="p" variant="subtitle1" className={classes.formTitle}>
              Contact Information
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <FormItem name="phoneNumber" label="Mobile No.*">
              <PhoneInput />
            </FormItem>
          </Grid>
          <Grid item xs={12} className={classes.botSpace}>
            <Textfield name="email" label="Email*" />
          </Grid>
          <Grid item xs={6} md={6} className={classes.botSpace}>
            <Textfield name="address.block" label="House  / Block No.*" />
          </Grid>
          <Grid item xs={12} md={6} className={classes.botSpace}>
            <Textfield name="address.postalCode" label="Postal Code*" />
          </Grid>
          <Grid item xs={6} md={6} className={classes.botSpace}>
            <Textfield name="address.level" label="Level" />
          </Grid>
          <Grid item xs={6} md={6} className={classes.botSpace}>
            <Textfield name="address.number" label="Number" />
          </Grid>
          <Grid item xs={12} className={classes.botSpace}>
            <Textfield name="address.street" label="Address 1 (Street Name)*" />
          </Grid>
          <Grid item xs={12} className={classes.botSpace}>
            <Textfield name="address.building" label="Address 2 (Building Name)" />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};
