import React, { useEffect, useState } from 'react';
import { Grid, Button, makeStyles, Typography } from '@material-ui/core';
import { useFormikContext } from 'formik';
import { useSelector } from 'react-redux';
import MetaService from '@services/meta.service';
import { DocumentLink } from 'features/profile/components/professional/document/link';

const useStyles = makeStyles({
  previewWrapper: {
    borderRadius: '10px',
    padding: 50,
    backgroundColor: 'white',
    '@media (max-width:780px)': {
      padding: 6,
    },
  },
  boxBlue: {
    borderRadius: '12px',
    padding: '50px 30px',
    border: '1px solid #4126CF',
    position: 'relative',
    '@media (max-width:780px)': {
      paddingLeft: 6,
      paddingRight: 6,
    },
  },
  formTitle: {
    borderBottom: '1px solid #4126CF',
    marginBottom: 0,
    paddingBottom: 15,
  },
  editBtn: {
    color: 'white',
    fontSize: 14,
    lineHeight: '21px',
    padding: '10px 26px !important',
    position: 'absolute',
    top: 0,
    right: 0,
    backgroundColor: '#4126CF',
    borderRadius: '0 10px 0 10px',
    '&:hover': {
      backgroundColor: 'black',
    },
  },
  topSpace: {
    marginTop: 50,
  },
  botSpace: {
    marginBottom: 50,
  },
});

export const Step4Preview = ({ setStep }) => {
  const classes = useStyles();
  const { values } = useFormikContext();

  const [identityList, setIdentityList] = useState([]);
  const [specialityList, setSpecialityList] = useState([]);
  useEffect(() => {
    MetaService.getMeta('specialities')
      .then((res) => {
        if (res?.code && res?.data) {
          const listWithLabel = res.data.reduce((acc, cur) => ({ ...acc, [cur.id]: cur.name }), {});
          setSpecialityList(listWithLabel);
        }
      })
      .catch((err) => console.log(err));
    MetaService.getMeta('identity-types')
      .then((res) => {
        if (res?.code && res?.data) {
          const identityListWithLabel = res.data.reduce(
            (acc, cur) => ({ ...acc, [cur.id]: cur.name }),
            {},
          );
          setIdentityList(identityListWithLabel);
        }
      })
      .catch((err) => console.log(err));
  }, []);

  const useDoc = (name) => {
    return useSelector((state) => state.document[name] || []);
  };
  const practicingCertificateDoc = useDoc('practicingCertificate.practicingCertificateDocuments');
  const familyPhysicianRegisterDoc = useDoc(
    'practicingCertificate.physicianPracticingCertificateDocuments',
  );
  const designatedWorkplaceDoctorDoc = useDoc(
    'practicingCertificate.workplaceDoctorPracticingCertificateDocuments',
  );

  const practicingCertificateDocName = [];
  const familyPhysicianRegisterDocName = [];
  const designatedWorkplaceDoctorDocName = [];
  practicingCertificateDoc.map((doc) => {
    return practicingCertificateDocName.push(`${doc.fileName}.${doc.fileExtension}`);
  });
  familyPhysicianRegisterDoc.map((doc) => {
    return familyPhysicianRegisterDocName.push(`${doc.fileName}.${doc.fileExtension}`);
  });
  designatedWorkplaceDoctorDoc.map((doc) => {
    return designatedWorkplaceDoctorDocName.push(`${doc.fileName}.${doc.fileExtension}`);
  });

  const specialityArray = values.practicingCertificate.specialities;
  const document = useSelector((state) => state.document);
  let documentNameList = {};
  specialityArray.map((sa, idx) => {
    const docInfor = document[`practicingCertificate.specialities[${idx}].documents`];
    const dd = docInfor.reduce(
      (acc, cur) => ({ ...acc, [cur.uploadId]: { name: cur.fileName, type: cur.fileExtension } }),
      {},
    );
    documentNameList = { ...documentNameList, ...dd };
  });
  return (
    <div className={classes.previewWrapper}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography component="p" variant="subtitle1" className={classes.formTitle}>
            Preview
          </Typography>
          <p className={classes.botSpace}>
            Please review the information before submitting. You can still make changes by clicking
            on the 'Edit' icon
          </p>
          <div className={classes.boxBlue}>
            <Typography component="p" variant="subtitle2" className={classes.formTitle}>
              Personal Information
            </Typography>
            <Button
              className={classes.editBtn}
              size="small"
              onClick={() => setStep(0)}
              style={{ padding: '0' }}
            >
              Edit
            </Button>
            <table className="preview-table">
              <tbody>
                <tr>
                  <td>First Name</td>
                  <td>{values.firstName}</td>
                </tr>
                <tr>
                  <td>Last Name</td>
                  <td>{values.lastName}</td>
                </tr>
                <tr>
                  <td>Gender</td>
                  <td>
                    {
                      {
                        1: 'Male',
                        2: 'Female',
                        3: 'Others',
                      }[values.gender]
                    }
                  </td>
                </tr>
                <tr>
                  <td>Date of Birth</td>
                  <td>{values.dob ? values.dob : ''}</td>
                </tr>
                <tr>
                  <td>Identification Document (Type)</td>
                  <td>
                    {(identityList && identityList[values.identityType]) || values.identityType}
                  </td>
                </tr>
                <tr>
                  <td>Identification Document No.</td>
                  <td>{values.identityNumber}</td>
                </tr>
              </tbody>
            </table>
            <Typography
              component="p"
              variant="subtitle2"
              className={`${classes.formTitle} ${classes.topSpace}`}
            >
              Contact Information
            </Typography>
            <table className="preview-table">
              <tbody>
                <tr>
                  <td>Mobile No.</td>
                  <td>{values.phoneNumber}</td>
                </tr>
                <tr>
                  <td>E-mail</td>
                  <td>{values.email}</td>
                </tr>
                <tr>
                  <td>House/Block No.</td>
                  <td>{values.address.block}</td>
                </tr>
                <tr>
                  <td>Address 1 (Street Name)</td>
                  <td>{values.address.street}</td>
                </tr>
                <tr>
                  <td>Address 2 (Building Name)</td>
                  <td>{values.address.building}</td>
                </tr>
                <tr>
                  <td>Level</td>
                  <td>{values.address.level}</td>
                </tr>
                <tr>
                  <td>Number</td>
                  <td>{values.address.number}</td>
                </tr>
                <tr>
                  <td>Postal Code</td>
                  <td>{values.address.postalCode}</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div style={{ paddingTop: '20px', paddingBottom: '20px' }}></div>

          <div className={classes.boxBlue}>
            <Typography component="p" variant="subtitle2" className={classes.formTitle}>
              Professional Information
            </Typography>
            <Button
              className={classes.editBtn}
              size="small"
              onClick={() => setStep(1)}
              style={{ padding: '0' }}
            >
              Edit
            </Button>
            <table className="preview-table">
              <tbody>
                <tr>
                  <td>Medical Council Registration (MCR) No.</td>
                  <td>{values.mcrNumber}</td>
                </tr>
                <tr>
                  <td>Date of Full Registration</td>
                  <td>{values.dateOfFullRegistration ? values.dateOfFullRegistration : ''}</td>
                </tr>
              </tbody>
            </table>
            <Typography
              component="p"
              variant="subtitle2"
              className={`${classes.formTitle} ${classes.topSpace}`}
            >
              Qualification(s)
            </Typography>
            <table style={{ width: '100%' }}>
              <tbody>
                {values.qualifications.map((q, i) => (
                  <tr key={i}>
                    <td colSpan="2">
                      <table className="preview-table">
                        <tbody>
                          <>
                            <tr>
                              <td>Academic Title</td>
                              <td>{q.name}</td>
                            </tr>
                            <tr>
                              <td>Year</td>
                              <td>{q.year}</td>
                            </tr>
                            <tr>
                              <td>Institution Name</td>
                              <td>{q.institution}</td>
                            </tr>
                          </>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <Typography
              component="p"
              variant="subtitle2"
              className={`${classes.formTitle} ${classes.topSpace}`}
            >
              Practicing Certificate
            </Typography>
            <table className="preview-table">
              <tbody>
                <tr>
                  <td>Start Date</td>
                  <td>
                    {values.practicingCertificate.startDate
                      ? values.practicingCertificate.startDate
                      : ''}
                  </td>
                </tr>
                <tr>
                  <td>End Date</td>
                  <td>
                    {values.practicingCertificate.endDate
                      ? values.practicingCertificate.endDate
                      : ''}
                  </td>
                </tr>
                <tr>
                  <td>Currently practicing as a</td>
                  <td>
                    {(values.practicingCertificate.isSpecialist).toString() === 'true'
                      ? 'Specialist'
                      : 'General Practitioner'}
                  </td>
                </tr>
                {values.practicingCertificate.specialities.length > 0 &&
                  values.practicingCertificate.specialities.map((spec, idx) => {
                    return (
                      <React.Fragment key={idx}>
                        <tr>
                          <td>Specialty/Sub-Specialty</td>
                          <td>
                            {(specialityList && specialityList[spec.speciality]) || spec.speciality}
                          </td>
                        </tr>
                        <tr>
                          <td>Entry Date</td>
                          <td>{spec.entryDate ? spec.entryDate : ''}</td>
                        </tr>
                      </React.Fragment>
                    );
                  })}

                <tr>
                  <td>Are you on the family physician register?</td>
                  <td>
                    {(values.practicingCertificate.isFamilyPhysicianRegister).toString() === 'true'
                      ? 'Yes'
                      : 'No'}
                  </td>
                </tr>
                {(values.practicingCertificate.isFamilyPhysicianRegister).toString() === 'true' && (
                  <tr>
                    <td>Entry Date</td>
                    <td>
                      {values.practicingCertificate.familyPhysicianRegisterEntryDate
                        ? values.practicingCertificate.familyPhysicianRegisterEntryDate
                        : ''}
                    </td>
                  </tr>
                )}
                <tr>
                  <td>Are you a designated workplace doctor?</td>
                  <td>
                    {(values.practicingCertificate.isDesignatedWorkplaceDoctor).toString() === 'true'
                      ? 'Yes'
                      : 'No'}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div style={{ paddingTop: '20px', paddingBottom: '20px' }}></div>

          <div className={classes.boxBlue}>
            <Typography component="p" variant="subtitle2" className={classes.formTitle}>
              Upload Supporting Document(s)
            </Typography>
            <Button
              className={classes.editBtn}
              size="small"
              onClick={() => setStep(2)}
              style={{ padding: '0' }}
            >
              Edit
            </Button>
            <table className="preview-table">
              <tbody>
                <tr>
                  <td>Practicing Certification(s)</td>
                  <td>
                    {practicingCertificateDoc.map((d, i) => {
                      return (
                        <div key={i}>
                          <DocumentLink file={d} />
                        </div>
                      );
                    })}
                  </td>
                </tr>
                {(values.practicingCertificate.isFamilyPhysicianRegister).toString() === 'true' && (
                  <tr>
                    <td>Family Physician Register</td>
                    <td>
                      {familyPhysicianRegisterDoc.map((d, i) => {
                        return (
                          <div key={i}>
                            <DocumentLink file={d} />
                          </div>
                        );
                      })}
                    </td>
                  </tr>
                )}
                {(values.practicingCertificate.isDesignatedWorkplaceDoctor).toString() === 'true' && (
                  <tr>
                    <td>Designated Workplace Doctor</td>
                    <td>
                      {designatedWorkplaceDoctorDoc.map((d, i) => {
                        return (
                          <div key={i}>
                            <DocumentLink file={d} />
                          </div>
                        );
                      })}
                    </td>
                  </tr>
                )}
                {(values.practicingCertificate.isSpecialist).toString() === 'true'
                  ? values.practicingCertificate.specialities.length > 0 &&
                  values.practicingCertificate.specialities.map((spec, idx) => {
                    return (
                      <React.Fragment key={idx}>
                        <tr>
                          <td>
                            {(specialityList && specialityList[spec.speciality]) ||
                              spec.speciality}
                          </td>
                          <td>
                            {spec.documents.map((specDoc, idx) => {
                              return (
                                <div key={idx}>
                                  <DocumentLink
                                    file={{
                                      uploadId: specDoc.uploadId,
                                      fileName: documentNameList[specDoc.uploadId].name,
                                      fileExtension: documentNameList[specDoc.uploadId].type,
                                    }}
                                  />
                                </div>
                              );
                            })}
                          </td>
                        </tr>
                      </React.Fragment>
                    );
                  })
                  : null}
              </tbody>
            </table>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};
