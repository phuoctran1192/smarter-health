import React from 'react';
import { Link } from 'react-router-dom';
import thankyou from '@assets/images/thankyou.jpg';

export const Step5ThankYou = () => {
  return (
    <div className="landing-page">
      <img src={thankyou} alt="smart panel" className='landing-page-ill'/>
      <div className="landing-page-content">
        <div className="heading-2 landing-page-heading">Congratulations!</div>
        <div className="body-2 mt-20 mb-40 landing-page-text">
        Your application has been successfully submitted. It will be reviewed and we will keep you updated on the progress via email.
        </div>
        <div className="landing-page-button">
          <Link
            to="/smart-panel/"
            className="button text-decoration-none landing-page-button-signup"
          >
            Back to Homepage
          </Link>
        </div>
      </div>
    </div>
  );
};
