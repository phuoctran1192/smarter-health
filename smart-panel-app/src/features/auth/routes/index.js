import { Route, Routes } from 'react-router-dom';

import { Login } from '@features/auth/Login';
import { Signup } from '@features/signup';
import { Activation } from '@features/activation/Activation';
import { TermsAndConditions } from 'features/activation/TermsAndConditions';

export const AuthRoutes = () => {
  return (
    <Routes>
      <Route path="smart-panel/login" element={<Login />} />
      <Route path="smart-panel/signup" element={<Signup />} />
      <Route path="smart-panel/confirm-registration/:token" element={<Activation />} />
      <Route path="smart-panel/terms-and-conditions" element={<TermsAndConditions />} />
    </Routes>
  );
};
