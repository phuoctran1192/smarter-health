import { LoginForm } from '@features/auth/components/LoginForm';
import { Grid, makeStyles, Typography } from '@material-ui/core';
import LoginImage from '@assets/images/login.svg';
const useStyles = makeStyles((theme) => ({
  login: {
    margin: 'auto',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
  },
  content: {
    width: '80%',
    maxWidth: '900px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  image: {
    width: '400px',
    '& img': {
      width: '100%',
    },
  },
  loginForm: {
    width: '400px',
  },
}));

export const Login = () => {
  const classes = useStyles();
  return (
    <div className={classes.login}>
      <Grid container className={classes.content} spacing={6}>
        <div className={classes.image}>
          <img src={LoginImage} />
        </div>
        <div className={classes.loginForm}>
          <LoginForm />
        </div>
      </Grid>
    </div>
  );
};
