import { IconButton, Grid, makeStyles, Button } from '@material-ui/core';
import RoleService from '@services/role.service';
import { ROLE } from '@utils/constants';
import Visibility from '@material-ui/icons/Visibility';
import LockIcon from '@material-ui/icons/Lock';
import React, { useEffect, useRef, useState } from 'react';
import { Form, Formik } from 'formik';
import { FormItem } from '@components/Forms';
import { SPTextField } from '@components/Inputs';
import * as Yup from 'yup';
import { useAuth } from '@hooks';
import { useRouter } from '@hooks/useRouter';
import { LoadingButton } from '../../../components/Buttons/LoadingButton';
import { VisibilityOff } from '@material-ui/icons';
import { useNavigate } from 'react-router-dom';
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: `${theme.spacing(1)}px 0`,
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: '100%',
  },
  btn: {
    minWidth: '150px',
  },
  title: {
    textAlign: 'center',
  },
  showPassword: {
    padding: '5px',
    marginRight: '-5px',
  },
  extension: {
    display: 'flex',
    fontSize: '12px',
    justifyContent: 'end',
    '& .forgot-pwd': {
      color: theme.palette.primary.main,
    },
  },
  signup: {
    marginTop: '20px',
    textAlign: 'center',
    '& .highlight': {
      color: theme.palette.primary.main,
      textDecoration: 'underline',
      fontWeight: 600,
      cursor: 'pointer',
    },
  },
}));

export const LoginForm = () => {
  const classes = useStyles();
  const router = useRouter();
  const { login, loading, errors } = useAuth();
  const formikRef = useRef();
  const [userProfile, setUserProfile] = useState(null);
  const navigate = useNavigate();
  const validationSchema = Yup.object().shape({
    email: Yup.string().required('This field is required').email('Email is invalid'),
    password: Yup.string()
      .required('This field is required')
      .min(8, 'Password is too short - should be 8 chars minimum.')
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
        'Password must contains Uppercase, Lowercase, Number and Special Case Character',
      ),
  });
  const [showPassword, setShowPassword] = useState(false);
  const onLogin = (account) =>
    login(account.email, account.password).then((auth) => {
      RoleService.getRoleUser()
        .then((response) => {
          const role = response?.data?.content?.roles || [];
          switch (role && true) {
            case role.findIndex((role) => role === ROLE.ADMIN || role === ROLE.SUPER_ADMIN) !== -1:
              router.navigate('/smart-panel/admin/applicant');
              break;
            case role.findIndex((role) => role === ROLE.DOCTOR) !== -1:
              const doctorId = response?.data?.content?.doctorId;
              router.navigate(`/smart-panel/profile/${doctorId}`);
              break;
            default:
              break;
          }
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {});
    });

  useEffect(() => {
    if (errors.status === 400) {
      formikRef.current.setErrors({
        email: 'Email or Password is not correct',
        password: 'Email or Password is not correct',
      });
    }
  }, [errors]);

  const handleToggleShowPass = () => {
    setShowPassword(!showPassword);
  };

  return (
    <div className="login">
      <div className={classes.title}>
        <h1>Welcome Back!</h1>
        <p>Please login to your account</p>
      </div>
      <Formik
        innerRef={formikRef}
        initialValues={{
          email: '',
          password: '',
        }}
        validationSchema={validationSchema}
        onSubmit={onLogin}
      >
        <Form>
          <Grid container spacing={4} style={{ paddingBottom: 0, paddingTop: 0 }}>
            <Grid item xs={12} md={12} style={{ paddingBottom: 0 }}>
              <FormItem label="Email" name="email">
                <SPTextField placeholder="Enter Email" />
              </FormItem>
            </Grid>
            <Grid item xs={12} md={12} style={{ paddingBottom: 0, paddingTop: 0 }}>
              <FormItem label="Password" name="password">
                <SPTextField
                  type={showPassword ? 'text' : 'password'}
                  placeholder="Enter Password"
                  InputProps={{
                    endAdornment: (
                      <IconButton
                        aria-label="toggle password visibility"
                        edge="end"
                        className={classes.showPassword}
                      >
                        <IconButton
                          aria-label="toggle password visibility"
                          edge="end"
                          onClick={handleToggleShowPass}
                        >
                          {showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </IconButton>
                    ),
                    startAdornment: <LockIcon style={{ marginRight: '5px', opacity: '0.7' }} />,
                  }}
                />
              </FormItem>
              {/*<div className={classes.extension}>*/}
              {/*  <div className="forgot-pwd">Forgot Password?</div>*/}
              {/*</div>*/}
            </Grid>
          </Grid>
          <Grid item xs={12} md={12} style={{ marginTop: '40px', width: '100%' }}>
            <LoadingButton type="submit" loading={loading} text="Login" />
          </Grid>
          <div>
            <div className={classes.signup}>
              <span>Don’t have an account? </span>
              <span className="highlight" onClick={() => navigate('/smart-panel/landing')}>
                Sign up{' '}
              </span>
              <span>instead</span>
            </div>
          </div>
        </Form>
      </Formik>
    </div>
  );
};
