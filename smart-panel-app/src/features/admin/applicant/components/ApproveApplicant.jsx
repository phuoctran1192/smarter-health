import { Button, Typography } from '@material-ui/core';
import { LoadingButton } from '../../../../components/Buttons/LoadingButton';
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ApplicantService from '@services/applicant.service';
import { APPLICANT_STATUS } from '@utils/constants';
const useStyles = makeStyles((theme) => ({
  actionModal: {
    display: 'flex',
    justifyContent: 'end',
    padding: '20px 0 0 0',
    marginTop: '50px',
  },
  approve: {
    backgroundColor: theme.palette.admin.main,
    color: 'white',
    padding: '8px 20px',
    marginRight: '20px',
    minWidth: '100px',
    '&:hover': {
      backgroundColor: '#46C3F4E0',
    },
  },
  cancel: {
    backgroundColor: '#3C2D2D29',
    color: 'black',
    padding: '8px 20px',
    marginRight: '20px',
  },
}));
export const ApproveApplicant = ({ doctorId, callbackApprove = null, handleClose }) => {
  const [loading, setLoading] = useState(false);
  const classes = useStyles();

  const approve = (values) => {
    setLoading(true);
    const payload = {
      doctorId,
    };
    ApplicantService.approveApplicants(payload)
      .then((res) => console.log(res))
      .catch((e) => console.log(e))
      .finally(() => {
        setLoading(false);
        callbackApprove && callbackApprove(APPLICANT_STATUS.APPROVED);
      });
  };

  return (
    <div>
      <Typography variant="subtitle1">Are you sure to approve this applicant ?</Typography>
      <div className={classes.actionModal}>
        <Button className={classes.cancel} onClick={handleClose}>
          Cancel
        </Button>
        <LoadingButton
          className={classes.approve}
          type="submit"
          text="Approve"
          loading={loading}
          onClick={approve}
        />
      </div>
    </div>
  );
};
