import { Button, Grid, Typography } from '@material-ui/core';
import { Form, Formik } from 'formik';
import { FormItem } from '../../../../components/Forms';
import { SPTextField } from '../../../../components/Inputs';
import { LoadingButton } from '../../../../components/Buttons/LoadingButton';
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import * as Yup from 'yup';
import ApplicantService from '@services/applicant.service';
import { APPLICANT_STATUS } from '@utils/constants';
const useStyles = makeStyles((theme) => ({
  actionModal: {
    display: 'flex',
    justifyContent: 'end',
    padding: '20px 0 0 0',
  },
  reject: {
    backgroundColor: theme.palette.error.main,
    color: 'white',
    padding: '8px 20px',
    minWidth: '100px',
    '&:hover': {
      backgroundColor: 'rgba(238, 76, 76, 0.59)',
    },
  },
  cancel: {
    backgroundColor: '#3C2D2D29',
    color: 'black',
    padding: '8px 20px',
    marginRight: '20px',
  },
}));
export const RejectApplicant = ({
  doctorId,
  callbackReject = null,
  handleClose,
  applicantStatus,
}) => {
  const [loadingReject, setLoadingReject] = useState(false);
  const classes = useStyles();
  const validationSchema = Yup.object().shape({
    reason: Yup.string().required('This field is required'),
  });
  const reject = (values) => {
    setLoadingReject(true);
    const payload = {
      doctorId,
      data: {
        text: values.reason,
      },
    };
    ApplicantService.rejectApplicants(payload)
      .then((res) => console.log(res))
      .catch((e) => console.log(e))
      .finally(() => {
        setLoadingReject(false);
        callbackReject && callbackReject(APPLICANT_STATUS.REJECTED);
      });
  };

  return (
    <div className="reason">
      <Typography variant="subtitle1">Are you sure to reject this applicant ?</Typography>
      <Formik initialValues={{ reason: '' }} onSubmit={reject} validationSchema={validationSchema}>
        <Form>
          <Grid container>
            <Grid item xs={12}>
              <FormItem required label="Reason" name="reason">
                <SPTextField placeholder="Enter reason" />
              </FormItem>
            </Grid>
            <Grid item xs={12}>
              <div className={classes.actionModal}>
                <Button className={classes.cancel} onClick={handleClose}>
                  Cancel
                </Button>
                <LoadingButton
                  className={classes.reject}
                  type="submit"
                  text="Reject"
                  loading={loadingReject}
                />
              </div>
            </Grid>
          </Grid>
        </Form>
      </Formik>
    </div>
  );
};
