import { makeStyles } from '@material-ui/core/styles';
import { useState, useEffect } from 'react';
import { ListApplicant } from './ListApplicant';
import { ApplicantDetail } from './ApplicantDetail';
import { Breadcrumbs, Button, Link, Typography } from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
  container: {
    maxWidth: 1200,
    margin: 'auto',
  },
  link: {
    textDecoration: 'underline',
    color: theme.palette.admin.main,
    cursor: 'pointer',
  },
  root: {
    backgroundColor: theme.palette.admin.main,
    color: 'white',
  },
}));

export const Applicant = () => {
  const classes = useStyles();
  const [showApplicantDetail, setShowApplicantDetail] = useState(false);
  const [doctorId, setDoctorId] = useState(null);
  const [applicantStatus, setApplicantStatus] = useState(null);
  const viewApplicantDetail = (record) => {
    setShowApplicantDetail(true);
    setDoctorId(record.id);
    setApplicantStatus(record.applicationStatus);
  };

  const handleOnBack = () => {
    setShowApplicantDetail(false);
    setDoctorId(null);
  };

  return (
    <div className={classes.container}>
      {showApplicantDetail ? (
        <ApplicantDetail
          doctorId={doctorId}
          onBack={handleOnBack}
          applicantStatus={applicantStatus}
          setApplicantStatus={setApplicantStatus}
        />
      ) : (
        <ListApplicant onClickRow={viewApplicantDetail} />
      )}
    </div>
  );
};
