import { SPTable } from '@components/Data';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Box, Chip, Typography } from '@material-ui/core';
import { useState } from 'react';
import ApplicantService from '@services/applicant.service';
import moment from 'moment';
import { APPLICANT_STATUS } from '@utils/constants';
const useStyles = makeStyles((theme) => ({
  chipCreated: {
    width: 100,
    fontWeight: '600',
    fontSize: 14,
    color: '#11325a',
    backgroundColor: '#f5f5fa',
  },
  chipOpen: {
    width: 100,
    fontWeight: '600',
    fontSize: 14,
    color: '#42ba96',
    backgroundColor: '#eaf9f4',
  },
  chipRejected: {
    width: 100,
    fontWeight: '600',
    fontSize: 14,
    color: '#df4759',
    backgroundColor: '#fff3f5',
  },
  chipApproved: {
    width: 100,
    fontWeight: '600',
    fontSize: 14,
    color: '#03ACEF',
    backgroundColor: '#E5F6FD',
  },
  container: {
    maxWidth: 1200,
    margin: 'auto',
  },
  link: {
    textDecoration: 'underline',
    color: theme.palette.admin.main,
    cursor: 'pointer',
  },
}));
export const ListApplicant = ({ onClickRow }) => {
  const classes = useStyles();
  const [isReloadTable, setIsReloadTable] = useState(true);
  const columns = [
    {
      title: 'Name',
      dataIndex: 'firstName',
      render: (record) => {
        return (
          <Grid container>
            <Grid item xs={12}>
              <Box component="span" m={1} style={{ margin: 0, fontWeight: '600' }}>
                {record.firstName} {record.lastName}
              </Box>
            </Grid>
          </Grid>
        );
      },
    },
    {
      title: 'Email',
      dataIndex: 'firstName',
      render: (record) => {
        return (
          <Grid container>
            <Grid item xs={12}>
              <Box component="span" m={1} style={{ margin: 0, color: '#6c757d' }}>
                {record.email}
              </Box>
            </Grid>
          </Grid>
        );
      },
    },
    {
      title: 'Status',
      dataIndex: 'applicantStatus',
      textAlign: 'center',
      render: (record) => {
        const status = record.applicationStatus
          ? record.applicationStatus
          : APPLICANT_STATUS.CREATED;
        switch (status) {
          case APPLICANT_STATUS.CREATED:
            return <Chip classes={{ root: classes.chipCreated }} label={status} size="small" />;
          case APPLICANT_STATUS.OPEN:
            return <Chip classes={{ root: classes.chipOpen }} label={status} size="small" />;
          case APPLICANT_STATUS.REJECTED:
            return <Chip classes={{ root: classes.chipRejected }} label={status} size="small" />;
          case APPLICANT_STATUS.APPROVED:
            return <Chip classes={{ root: classes.chipApproved }} label={status} size="small" />;
          default:
            return <Chip label={status} size="small" />;
        }
      },
    },
    {
      title: 'Submitted Date',
      render: (record) =>
        record.submittedDate ? moment(record.submittedDate).format('DD/MM/YYYY [at] HH:mm:ss') : '',
    },
    {
      title: 'Actions',
      render: (record, index) => (
        <a className={classes.link} onClick={() => redirectToDetail(record, index)}>
          View Detail
        </a>
      ),
    },
  ];

  const redirectToDetail = (record) => {
    onClickRow(record);
  };

  return (
    <Grid container className={classes.container}>
      <Grid item xs={12}>
        <Typography component="p" variant="subtitle1">
          Applicants
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <SPTable
          field="applications"
          columns={columns}
          isReloadTable={isReloadTable}
          setIsReloadTable={setIsReloadTable}
          apiGetList={ApplicantService.getApplicants}
        />
      </Grid>
    </Grid>
  );
};
