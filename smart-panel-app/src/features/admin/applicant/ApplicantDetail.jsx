import { makeStyles } from '@material-ui/core/styles';
import React, { useEffect, useState } from 'react';
import MetaService from '@services/meta.service';
import Container from '@material-ui/core/Container';
import ProfileAvatar from '@features/profile/components/avatar';
import ProfileTab from '@features/profile/components/tab';
import { Button, Modal, Paper, Typography } from '@material-ui/core';
import { SmartSpinner } from '../../../components/Elements';
import { RejectApplicant } from './components/RejectApplicant';
import { ApproveApplicant } from './components/ApproveApplicant';
import { APPLICANT_STATUS } from '@utils/constants';
import ApplicantService from '@services/applicant.service';
const useStyles = makeStyles((theme) => ({
  container: {
    maxWidth: 1200,
    margin: 'auto',
  },
  applicantDetail: {
    backgroundColor: 'white',
    minHeight: '70vh',
    display: 'flex',
    justifyContent: (props) => props.loading && 'center',
    alignItems: (props) => props.loading && 'center',
  },
  wrapper: {
    marginTop: '20px',
  },
  title: {
    margin: '30px 0',
    display: 'flex',
    justifyContent: 'left',
    alignItems: 'center',
    borderRadius: '5px',
  },
  back: {
    backgroundColor: theme.palette.admin.main,
    color: 'white',
    padding: '8px 20px',
    marginRight: '20px',
    '&:hover': {
      backgroundColor: '#46C3F4E0',
    },
  },
  action: {
    display: 'flex',
    justifyContent: 'end',
    padding: '20px 20px 0 0',
  },
  actionModal: {
    display: 'flex',
    justifyContent: 'end',
    padding: '20px 0 0 0',
  },
  approve: {
    backgroundColor: theme.palette.admin.main,
    color: 'white',
    padding: '8px 20px',
    marginRight: '20px',
    '&.Mui-disabled': {
      backgroundColor: '#cacab34a',
    },
    '&:hover': {
      backgroundColor: '#46C3F4E0',
    },
  },
  reject: {
    backgroundColor: theme.palette.error.main,
    color: 'white',
    padding: '8px 20px',
    minWidth: '100px',
    '&.Mui-disabled': {
      backgroundColor: '#cacab34a',
    },
    '&:hover': {
      backgroundColor: 'rgba(238, 76, 76, 0.59)',
    },
  },
  modal: {
    width: '500px',
    padding: '20px',
  },
  cancel: {
    backgroundColor: '#3C2D2D29',
    color: 'black',
    padding: '8px 20px',
    marginRight: '20px',
  },
}));
export const ApplicantDetail = ({ doctorId, onBack, applicantStatus, setApplicantStatus }) => {
  const [doctor, setDoctor] = useState({});
  const [identityList, setIdentityList] = useState([]);
  const [specialityList, setSpecialityList] = useState([]);
  const [showPopup, setShowPopup] = useState(false);
  const [loading, setLoading] = useState(true);
  const [action, setAction] = useState('');
  const classes = useStyles({ loading });
  useEffect(() => {
    if (doctorId) {
      setLoading(true);
      const payload = {
        doctorId,
      };
      ApplicantService.getApplicantDetail(payload)
        .then((res) => {
          setDoctor(res.data);
          MetaService.getMeta('identity-types')
            .then((res) => {
              if (res?.code && res?.data) {
                setIdentityList(res.data);
              }
            })
            .catch((err) => console.log(err));
          MetaService.getMeta('specialities')
            .then((res) => {
              if (res?.code && res?.data) {
                setSpecialityList(res.data);
              }
            })
            .catch((err) => console.log(err));
        })
        .catch((err) => console.log(err))
        .finally(() => setLoading(false));
    }
  }, [doctorId]);

  const handleReject = () => {
    setShowPopup(true);
    setAction('reject');
  };

  const handleApprove = () => {
    setShowPopup(true);
    setAction('approve');
  };

  const handleClose = () => {
    setShowPopup(false);
  };

  const callbackAfterConfirm = (value) => {
    handleClose();
    setApplicantStatus(value);
  };

  return (
    <>
      <div className={classes.title}>
        <Button className={classes.back} onClick={onBack}>
          Back
        </Button>
        <Typography variant="h6">APPLICANT DETAIL</Typography>
      </div>
      <div className={classes.applicantDetail}>
        {loading ? (
          <SmartSpinner />
        ) : (
          doctor.id && (
            <div style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
              <div className={classes.action}>
                <Button
                  className={classes.approve}
                  onClick={handleApprove}
                  disabled={applicantStatus && applicantStatus !== APPLICANT_STATUS.CREATED}
                >
                  Approve
                </Button>
                <Button
                  className={classes.reject}
                  onClick={handleReject}
                  disabled={applicantStatus && applicantStatus !== APPLICANT_STATUS.CREATED}
                >
                  Reject
                </Button>
              </div>
              <div className={classes.wrapper}>
                <Container>
                  <ProfileAvatar doctor={doctor} editable={false} />
                </Container>
                <ProfileTab
                  doctor={doctor}
                  identityList={identityList}
                  specialityList={specialityList}
                  editable={false}
                />
              </div>
            </div>
          )
        )}
      </div>
      {showPopup && (
        <Modal
          open={showPopup}
          onClose={handleClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
        >
          <Paper classes={{ root: classes.modal }}>
            {action === 'reject' && (
              <RejectApplicant
                doctorId={doctorId}
                handleClose={handleClose}
                callbackReject={callbackAfterConfirm}
                applicantStatus={applicantStatus}
              />
            )}
            {action === 'approve' && (
              <ApproveApplicant
                doctorId={doctorId}
                handleClose={handleClose}
                callbackApprove={callbackAfterConfirm}
                applicantStatus={applicantStatus}
              />
            )}
          </Paper>
        </Modal>
      )}
    </>
  );
};
