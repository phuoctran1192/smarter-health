import { makeStyles } from '@material-ui/core/styles';
import { ListUser } from './components/ListUser';
const useStyles = makeStyles((theme) => ({
  container: {
    maxWidth: 1200,
    margin: 'auto',
  },
  link: {
    textDecoration: 'underline',
    color: theme.palette.admin.main,
    cursor: 'pointer',
  },
  root: {
    backgroundColor: theme.palette.admin.main,
    color: 'white',
  },
}));

export const User = () => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <ListUser />
    </div>
  );
};
