import { SPTable } from '@components/Data';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Box, Typography } from '@material-ui/core';
import { useState } from 'react';
import UserService from '@services/user.service';
import moment from 'moment';
const useStyles = makeStyles((theme) => ({
  container: {
    maxWidth: 1200,
    margin: 'auto',
  },
  link: {
    textDecoration: 'underline',
    color: theme.palette.admin.main,
    cursor: 'pointer',
  },
}));
export const ListUser = ({ onClickRow }) => {
  const classes = useStyles();
  const [isReloadTable, setIsReloadTable] = useState(true);
  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      textAlign: 'center',
    },
    {
      title: 'Full Name',
      dataIndex: 'fullName',
      render: (record) => {
        return (
          <Grid container>
            <Grid item xs={12}>
              <Box component="span" m={1} style={{ margin: 0, fontWeight: '600' }}>
                {record.fullName}
              </Box>
            </Grid>
          </Grid>
        );
      },
    },
    {
      title: 'Created Date',
      render: (record) => {
        return record.createdDate
          ? moment(record.createdDate).format('DD/MM/YYYY [at] HH:mm:ss')
          : '';
      },
    },
  ];

  const redirectToDetail = (record) => {
    onClickRow(record);
  };

  return (
    <Grid container className={classes.container}>
      <Grid item xs={12}>
        <Typography component="p" variant="subtitle1">
          Users
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <SPTable
          field="users"
          columns={columns}
          isReloadTable={isReloadTable}
          setIsReloadTable={setIsReloadTable}
          apiGetList={UserService.getUsers}
        />
      </Grid>
    </Grid>
  );
};
