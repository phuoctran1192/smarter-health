export const SPECIFIC_FIELD = {
  GENERAL_PRACTITIONER: 'General Practitioner',
  SPECIALIST: 'Specialist',
};
export const Y_N_ANSWER = {
  YES: 'Yes',
  NO: 'No',
};
