import { ApplicantContext } from './ApplicantContext';
export const ApplicantProvider = ({ editable = false, ...props }) => (
  <ApplicantContext.Provider
    value={{
      editable,
    }}
  >
    {props.children}
  </ApplicantContext.Provider>
);
