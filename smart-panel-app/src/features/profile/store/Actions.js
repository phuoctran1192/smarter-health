import { createAction } from '@reduxjs/toolkit';

const updateDoctor = createAction(`doctor/update`);
const updateDoctorWorkBackground = createAction(`doctor/updateWorkBackground`);
const updateMetadata = createAction(`metadata/updateMetadata`);
const editWorkBackground = createAction(`ui/workBackground`);
const editWorkBackgroundFailed = createAction(`error/workBackground`);

const Actions = {
  updateDoctor,
  updateDoctorWorkBackground,
  editWorkBackground,
  updateMetadata,
  editWorkBackgroundFailed
};

export default Actions;
