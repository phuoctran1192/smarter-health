export const initialState = {
    doctor: {},
    editing: {
        workBackground: true,
    },
    meta: {
        hospitals: [],
        ambulatorySurgicalCentres: [],
        insuranceCompanies: [],
        areasOfInterest: [],
        specialities: [],
    },
    errorMessage: {
        workBackgroundSubmitErrorMessage: "",
    },
}