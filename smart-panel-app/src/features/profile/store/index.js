import { configureStore, createSlice } from '@reduxjs/toolkit';
import reducer from './reducer';

const store = configureStore({
  reducer: {
    doctorProfile: reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
export default store;
