import Actions from './Actions';
import DoctorService from 'services/doctor.service';
import MetaService from '@services/meta.service';

export const fetchDoctor = () => async (dispatch) => {
  DoctorService.getDoctorProfile()
    .then((res) => {
      if (res && res.data) dispatch(Actions.updateDoctor(res.data));
    })
    .catch((err) => console.log(err));
};

export const fetchDoctorWorkBackground = () => async (dispatch) => {
  DoctorService.getDoctorWorkBackground()
    .then((res) => {
      if (res && res.data && res.data.content)
        dispatch(Actions.updateDoctorWorkBackground(res.data.content));
    })
    .catch((err) => console.log(err));
};

export const postDoctorWorkBackground = (workBackgroundData) => async (dispatch) => {
  DoctorService.updateDoctorWorkBackground(workBackgroundData)
    .then((res) => {
      if (res && res.data) dispatch(Actions.updateDoctorWorkBackground(res.data));
      dispatch(Actions.editWorkBackground(false));
    })
    .catch((err) => {
      console.log(err);
      dispatch(Actions.editWorkBackgroundFailed(err));
    });
};

export const fetchMetadata = () => async (dispatch) => {
  const metadataTypes = [
    { endpoint: 'area-of-interests', fieldName: 'areasOfInterest' },
    { endpoint: 'specialities', fieldName: 'specialities' },
    { endpoint: 'companies', fieldName: 'insuranceCompanies' },
    { endpoint: 'surgical-centres', fieldName: 'ambulatorySurgicalCentres' },
    { endpoint: 'hospitals', fieldName: 'hospitals' },
    { endpoint: 'genders', fieldName: 'genders' },
    { endpoint: 'identity-types', fieldName: 'identityTypes' },
  ];
  metadataTypes.forEach((type) => {
    MetaService.getMeta(type.endpoint)
      .then((res) => {
        if (res?.code && res?.data) {
          dispatch(Actions.updateMetadata({ fieldName: type.fieldName, data: res.data }));
        }
      })
      .catch((err) => console.log(err));
  });
};

const APIActions = {
  fetchDoctor,
  postDoctorWorkBackground,
  fetchDoctorWorkBackground,
  fetchMetadata,
};
export default APIActions;
