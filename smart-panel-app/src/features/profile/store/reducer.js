import { createReducer } from '@reduxjs/toolkit';
import Actions from './Actions';
import { initialState } from './initialState';

const reducer = createReducer(initialState, (builder) => {
  builder
    .addCase(Actions.updateDoctor, (state, action) => {
      state.doctor = action.payload;
    })
    .addCase(Actions.updateDoctorWorkBackground, (state, action) => {
      state.doctor = {
        ...state.doctor,
        ...action.payload,
      };
    })
    .addCase(Actions.editWorkBackground, (state, action) => {
      state.editing.workBackground = action.payload;
      state.errorMessage.workBackgroundSubmitErrorMessage = "";
    })
    .addCase(Actions.updateMetadata, (state, action) => {
      const { fieldName, data } = action.payload;
      state.meta[fieldName] = data;
    })
    .addCase(Actions.editWorkBackgroundFailed, (state, action) => {
      state.errorMessage.workBackgroundSubmitErrorMessage = action.payload;
    });
});

export default reducer;
