import React, { useEffect } from 'react';
import ProfileAvatar from './components/avatar';
import Container from '@material-ui/core/Container';
import { useParams } from 'react-router-dom';
import ProfileTab from 'features/profile/components/tab';
import { Provider, useDispatch, useSelector } from 'react-redux';
import store from './store';
import APIActions from './store/APIActions';
import { SmartSpinner } from 'components/Elements/Spinner/SmartSpinner';

export const Profile = () => {
  return (
    <>
      <Provider store={store}>
        <ProfileContainer />
      </Provider>
    </>
  );
};

const ProfileContainer = () => {
  const dispatch = useDispatch();
  const doctor = useSelector((state) => state.doctorProfile.doctor);
  const { doctorId } = useParams();
  useEffect(() => {
    doctorId && dispatch(APIActions.fetchDoctor(doctorId));
    dispatch(APIActions.fetchMetadata());
  }, [doctorId, dispatch]);

  return (
    <>
      {doctor.id ? (
        <div>
          <Container>
            <ProfileAvatar doctor={doctor} />
          </Container>

          <ProfileTab doctor={doctor} />
        </div>
      ) : (
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <SmartSpinner loading={true} />
        </div>
      )}
    </>
  );
};
