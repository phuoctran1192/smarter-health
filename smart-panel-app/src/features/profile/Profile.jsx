import React, { useEffect, useState } from 'react';
import ProfileAvatar from '@features/profile/components/avatar';
import DoctorService from 'services/doctor.service';
import Container from '@material-ui/core/Container';
import ProfileTab from '@features/profile/components/tab';
import MetaService from 'services/meta.service';

export const Profile = () => {
  const [doctor, setDoctor] = useState({});
  const [identityList, setIdentityList] = useState([]);
  useEffect(() => {
    DoctorService.getDoctorProfile()
      .then((res) => {
        setDoctor(res.data);
        MetaService.getMeta('identity-types')
          .then((res) => {
            if (res?.code && res?.data) {
              setIdentityList(res.data);
            }
          })
          .catch((err) => console.log(err));
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <>
      {doctor.id ? (
        <div>
          <Container>
            <ProfileAvatar doctor={doctor} />
          </Container>

          <ProfileTab doctor={doctor} identityList={identityList} />
        </div>
      ) : (
        'There is no data'
      )}
    </>
  );
};
