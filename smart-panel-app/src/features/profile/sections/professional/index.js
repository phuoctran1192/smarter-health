import React, { useContext, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Button, Grid } from '@material-ui/core';
import ProfessionalInformationTable from '../../components/professional/view';
import ProfessionalInformationForm from '../../components/professional/edit';
import WorkBackGroundTable from '../../components/FormView/WorkBackground';
import DocumentTable from 'features/profile/components/professional/document/view';
import DocumentEditForm from 'features/profile/components/professional/document/edit';
import { ApplicantContext } from '../../context';
import WorkBackgroundForm from 'features/profile/components/Form/WorkBackgroundForm';
import { useDispatch, useSelector } from 'react-redux';
import Actions from '../../store/Actions';
import APIActions from '../../store/APIActions';
import DoctorService from 'services/doctor.service';
import { SmartSpinner } from 'components/Elements/Spinner/SmartSpinner';

const useStyles = makeStyles((theme) => ({
  boxContainer: {
    backgroundColor: '#F7F7F7',
  },
  box: {
    backgroundColor: 'white',
    padding: '50px 30px',
    width: '100%',
    maxWidth: 930,
    marginTop: 50,
    borderRadius: 10,
    position: 'relative',
    '@media (max-width: 992px)': {
      padding: '30px 16px',
    },
  },
  editButton: {
    borderRadius: '0 10px',
    position: 'absolute',
    right: 0,
    top: 0,
  },
}));

export default function ProfessionalInformationTab({ doctor, identityList, specialityList }) {
  const dispatch = useDispatch();
  const isEditingWorkBackground = useSelector(
    (state) => state.doctorProfile?.editing.workBackground,
  );
  const [loading, setLoading] = useState(true);
  const [isEditInfo, setIsEditInfo] = useState(false);
  const [isEditDocument, setIsEditDocument] = useState(false);
  const { editable } = useContext(ApplicantContext);
  const classes = useStyles();
  const onEditInfoClick = () => {
    setIsEditInfo(!isEditInfo);
  };

  const onEditDocument = () => {
    setIsEditDocument(true);
  };

  const onCancelEditDocument = () => {
    setIsEditDocument(false);
  };

  const handleChangeWorkBackground = (workBackgroundValues) => {
    dispatch(APIActions.postDoctorWorkBackground(workBackgroundValues));
  };

  const handleEditWorkBackgroundClick = () => {
    dispatch(Actions.editWorkBackground(true));
  };

  const getDoctorWorkBackground = () => {
    setLoading(true);
    DoctorService.getDoctorProfile()
      .then((res) => {
        if(res.data.hasFullProfile) {
          dispatch(Actions.editWorkBackground(false));
        }
      })
      .catch((err) => console.log(err))
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    dispatch(Actions.editWorkBackground(true));
    getDoctorWorkBackground();
  }, []);

  return (
    <div>
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
        className={classes.boxContainer}
      >
        <Box className={classes.box}>
          {loading 
            ? (
              <div style={{ display: 'flex', justifyContent: 'center' }}>
                <SmartSpinner loading={loading} />
              </div>
            ) 
            : (
              <>
                {editable && !isEditingWorkBackground && (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleEditWorkBackgroundClick}
                    className={classes.editButton}
                  >
                    Edit
                  </Button>
                )}
                {!isEditingWorkBackground ? (
                  <WorkBackGroundTable doctorData={doctor} />
                ) : (
                  <WorkBackgroundForm doctor={doctor} onChange={handleChangeWorkBackground} />
                )}
              </>
            )
          }
        </Box>
      </Grid>

      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
        className={classes.boxContainer}
      >
        <Box className={classes.box}>
          {editable && !isEditInfo && (
            <Button
              variant="contained"
              color="primary"
              onClick={onEditInfoClick}
              className={classes.editButton}
            >
              Edit
            </Button>
          )}

          {!isEditInfo ? (
            <ProfessionalInformationTable doctorData={doctor} />
          ) : (
            <ProfessionalInformationForm doctorData={doctor} onEditInfoClick={onEditInfoClick} />
          )}
        </Box>
      </Grid>

      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
        className={classes.boxContainer}
      >
        <Box className={classes.box}>
          {editable && (
            <Button
              variant="contained"
              color="primary"
              onClick={onEditDocument}
              className={classes.editButton}
            >
              Edit
            </Button>
          )}
          {!isEditDocument ? (
            <DocumentTable doctorData={doctor} specialityList={specialityList} />
          ) : (
            <DocumentEditForm doctorData={doctor} onCancelEditDocument={onCancelEditDocument} />
          )}
        </Box>
      </Grid>
    </div>
  );
}
