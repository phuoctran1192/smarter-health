import React, { useContext, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Button, Grid } from '@material-ui/core';
import PersonalInformationTable from '../../components/personal-information/personal-information-table';
import Language, { LanguageEdit } from '../../components/personal-information/language/edit';
import PersonalInformationForm from 'features/profile/components/personal-information/personal-information-form';
import { ApplicantContext } from '../../context';
import LanguageTable from '../../components/personal-information/language/view';
import DoctorService from 'services/doctor.service';
import MetaService from 'services/meta.service';
import { SmartSpinner } from '../../../../components/Elements';
const useStyles = makeStyles((theme) => ({
  boxContainer: {
    backgroundColor: '#F7F7F7',
  },
  box: {
    backgroundColor: 'white',
    padding: '50px 30px',
    width: '100%',
    maxWidth: 930,
    marginTop: 50,
    borderRadius: 10,
    position: 'relative',
    '@media (max-width: 992px)': {
      padding: '30px 8px',
    },
  },
  editButton: {
    borderRadius: '0 10px',
    position: 'absolute',
    right: 0,
    top: 0,
  },
}));

export default function PersonalInformationTab({ doctor, identityList }) {
  const [isEdit, setIsEdit] = useState(false);
  const [doctorData, setDoctorData] = useState(doctor);
  const [isEditLanguage, setIsEditLanguage] = useState(false);
  const { editable } = useContext(ApplicantContext);
  const [loading, setLoading] = useState(true);
  const [proficiencyNames, setProficiencyNames] = useState([]);
  const [proficiencyIDs, setProficiencyIDs] = useState([]);
  const [srcLanguage, setSRCLanguage] = useState([]);
  const [srcFluency, setSRCFluency] = useState([]);

  const classes = useStyles();
  const onEditClick = () => {
    setIsEdit(true);
  };
  const onEditLanguage = () => {
    setIsEditLanguage(!isEditLanguage);
  };
  const onCancelEditPersonal = () => {
    setIsEdit(false);
  };
  const onChangeSuccess = () => {
    DoctorService.getDoctorProfile().then((res) => {
      if (res?.code && res?.data) {
        setDoctorData(res.data);
      }
    });
    setIsEdit(false);
  };

  const getDoctorProficiency = () => {
    // Get languages
    setLoading(true);
    MetaService.getMeta('languages')
      .then((res) => {
        const languages = res.data
          ? res.data.map((language) => ({
              code: language.code,
              label: language.name,
              value: language.id,
            }))
          : [];
        setSRCLanguage(languages);
      })
      .catch((err) => console.log(err))
      .finally(() => {});

    // Get fluencies
    MetaService.getMeta('language-proficiencies')
      .then((res) => {
        const fluencies = res.data
          ? res.data.map((fluency) => ({
              label: fluency.name,
              value: fluency.id,
            }))
          : [];
        setSRCFluency(fluencies);
      })
      .catch((err) => console.log(err))
      .finally(() => {});

    // Get doctor language
    DoctorService.getDoctorProficiency()
      .then((res) => {
        const proficiencyNamesTmp =
          res.data && res.data.data
            ? res.data.data.map((item) => ({
                language: item.language.name,
                fluency: item.proficiency.name,
              }))
            : [];

        let proficiencyIDsTmp =
          res.data && res.data.data
            ? res.data.data.map((item) => ({
                language: item.language.id,
                fluency: item.proficiency.id,
              }))
            : [];

        if (proficiencyIDsTmp.length === 0) {
          proficiencyIDsTmp = [
            {
              language: srcLanguage[0]?.id || '',
              fluency: srcFluency[0]?.id || '',
            },
          ];
          setIsEditLanguage(true);
        }
        setProficiencyNames(proficiencyNamesTmp);
        setProficiencyIDs(proficiencyIDsTmp);
      })
      .catch((err) => console.log(err))
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    getDoctorProficiency();
  }, []);

  return (
    <div>
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
        className={classes.boxContainer}
      >
        <Box className={classes.box}>
          {loading ? (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <SmartSpinner loading={loading} />
            </div>
          ) : (
            <>
              {editable && !isEditLanguage && (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={onEditLanguage}
                  className={classes.editButton}
                >
                  Edit
                </Button>
              )}
              {isEditLanguage ? (
                <LanguageEdit
                  doctorData={doctor}
                  proficiency={proficiencyIDs}
                  onEditLanguage={onEditLanguage}
                  srcLanguage={srcLanguage}
                  srcFluency={srcFluency}
                  reloadDoctorProficiency={getDoctorProficiency}
                />
              ) : (
                <LanguageTable doctorData={doctor} proficiency={proficiencyNames} />
              )}
            </>
          )}
        </Box>
        <Box className={classes.box}>
          {editable && (
            <Button
              variant="contained"
              color="primary"
              onClick={onEditClick}
              className={classes.editButton}
            >
              Edit
            </Button>
          )}
          {!isEdit ? (
            <PersonalInformationTable doctorData={doctorData} identityList={identityList} />
          ) : (
            <PersonalInformationForm
              doctorPersonalData={doctorData}
              identityList={identityList}
              onCancelEditPersonal={onCancelEditPersonal}
              onChangeSuccess={onChangeSuccess}
            />
          )}
        </Box>
      </Grid>
    </div>
  );
}
