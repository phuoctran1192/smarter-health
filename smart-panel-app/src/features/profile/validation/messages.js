export const REQUIRED_FIELD_MESSAGE = 'This field is required';
export const FIELD_NUMBER_MESSAGE = 'This field specify a number';
export const START_DATE_BEFORE_END_DATE_MESSAGE = 'Start Date must be Before End Date';
export const END_DATE_AFTER_START_DATE_MESSAGE = 'End date must be after start date';
export const NUMBER_MUST_4_DIGITS_MESSAGE = 'Must always be 4 digits';
export const YEAR_MINIMUM_MESSAGE = 'Minimum value is 1990';
