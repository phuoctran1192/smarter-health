import * as Yup from 'yup';
import { REQUIRED_FIELD_MESSAGE } from './messages';

export const getDoctorWorkBackgroundSectionSchema = (isSpecialist) => ({
    background: Yup.string().nullable().required(REQUIRED_FIELD_MESSAGE),
    isSMAMember: Yup.string().nullable().required(REQUIRED_FIELD_MESSAGE),
    isAMSMember: Yup.string().nullable()
        .test({
            message: REQUIRED_FIELD_MESSAGE,
            test: (value) => !isSpecialist || (value != null && value !== ""),
        }),
    isCFPSMember: Yup.string().nullable()
        .test({
            message: REQUIRED_FIELD_MESSAGE,
            test: (value) => isSpecialist || (value != null && value !== ""),
        }),
    panelDoctorFor: Yup.array().of(Yup.number()),
    clinicManagementSystem: Yup.string().nullable(),
    accreditedHospitals: Yup.array().of(Yup.number())
        .test({
            message: REQUIRED_FIELD_MESSAGE,
            test: (value) => Array.isArray(value) && value.length > 0,
        }),
    accreditedSurgicalCentres: Yup.array().of(Yup.number())
        .test({
            message: REQUIRED_FIELD_MESSAGE,
            test: (value) => Array.isArray(value) && value.length > 0,
        }),
    areasOfInterest: Yup.array().of(Yup.number()),
})