import * as Yup from 'yup';
import { REQUIRED_FIELD_MESSAGE } from './messages';

export const getEmtpyPlaceOfPractice = () => {
    return {
        name: "",
        licenseNumber: "",
        address: {
            block: "",
            building: "",
            street: "",
            level: "",
            number: "",
            postalCode: "",
        },
        email: "",
        contactNumber: "",
        website: "",
        consultationSchedules: []
    };
}

export const PLACE_OF_PRACTICE_FORM_SECTION_SCHEMA = Yup.array()
    .of(
        Yup.object().shape({
            name: Yup.string().nullable().required(REQUIRED_FIELD_MESSAGE),
            licenseNumber: Yup.string().nullable().required(REQUIRED_FIELD_MESSAGE),
            email: Yup.string().email('Please choose a valid email').required(REQUIRED_FIELD_MESSAGE),
            contactNumber: Yup.string().nullable().required(REQUIRED_FIELD_MESSAGE),
            website: Yup.string().nullable(),
            address: Yup.object().shape({
                block: Yup.string().required(REQUIRED_FIELD_MESSAGE),
                building: Yup.string().required(REQUIRED_FIELD_MESSAGE),
                street: Yup.string().required(REQUIRED_FIELD_MESSAGE),
                level: Yup.string(),
                number: Yup.string(),
                postalCode: Yup.string().required(REQUIRED_FIELD_MESSAGE),
            }),
            consultationSchedules: Yup.array().of(
                Yup.object().shape({
                    weekday: Yup.string(),
                    sessions: Yup.array().of(
                        Yup.object().shape({
                            startTime: Yup.string(),
                            endTime: Yup.string()
                            .test("is-greater", "End time should be greater than start time", function(value) {
                                const { startTime } = this.parent;
                                return (value>startTime);
                            })
                        })
                    )
                })
            )
        }),
    )