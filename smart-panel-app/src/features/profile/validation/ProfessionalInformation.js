import moment from 'moment';
import * as Yup from 'yup';
import {
  REQUIRED_FIELD_MESSAGE,
  FIELD_NUMBER_MESSAGE,
  NUMBER_MUST_4_DIGITS_MESSAGE,
  YEAR_MINIMUM_MESSAGE,
  END_DATE_AFTER_START_DATE_MESSAGE,
} from './messages';
import DateSchemaBuilder from '@utils/formik/DateSchemaBuilder';

export const getProfessionalInformationSchema = () =>
  Yup.object().shape({
    professionalInfo: Yup.object().shape({
      mcrNumber: Yup.string().required(REQUIRED_FIELD_MESSAGE),
      dateOfFullRegistration: new DateSchemaBuilder().withRequired().build(),
    }),
    qualifications: Yup.array().of(
      Yup.object().shape({
        name: Yup.string().required(REQUIRED_FIELD_MESSAGE),
        year: Yup.string()
          .required(REQUIRED_FIELD_MESSAGE)
          .matches(/^[0-9]+$/, FIELD_NUMBER_MESSAGE)
          .test({
            message: NUMBER_MUST_4_DIGITS_MESSAGE,
            test: (value) => {
              return !value || !(value.length < 4);
            },
          })
          .test({
            message: YEAR_MINIMUM_MESSAGE,
            test: (value) => !(value < 1990),
          }),
        institution: Yup.string().required(REQUIRED_FIELD_MESSAGE),
      }),
    ),

    practicingCertificate: Yup.object().shape({
      familyPhysicianRegister: Yup.string().required(REQUIRED_FIELD_MESSAGE),
      familyPhysicianRegisterEntryDate: new DateSchemaBuilder()
        .build()
        .when('familyPhysicianRegister', (familyPhysicianRegister, schema) => {
          if (familyPhysicianRegister === 'YES') {
            return new DateSchemaBuilder().withRequired().build();
          } else {
            return schema;
          }
        }),
      designatedWorkplaceDoctor: Yup.string().required(REQUIRED_FIELD_MESSAGE),
      startDate: new DateSchemaBuilder().withRequired().build(),
      endDate: new DateSchemaBuilder()
        .withRequired()
        .withAfter('startDate', END_DATE_AFTER_START_DATE_MESSAGE, true)
        .build(),
      specialist: Yup.string().required(REQUIRED_FIELD_MESSAGE),
    }),
  });
