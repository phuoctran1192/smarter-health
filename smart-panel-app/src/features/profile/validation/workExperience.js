import * as Yup from 'yup';
import { REQUIRED_FIELD_MESSAGE } from './messages';

export const getEmtpyWorkExperience = () => {
    return {
        organization: "",
        department: "",
        position: "",
        start: "",
        end: "",
    };
}

export const WORK_EXPERIENCE_FORM_SECTION_SCHEMA = Yup.array()
    .of(
        Yup.object().shape({
            organization: Yup.string().nullable().required(REQUIRED_FIELD_MESSAGE),
            department: Yup.string().nullable().required(REQUIRED_FIELD_MESSAGE),
            position: Yup.string().nullable().required(REQUIRED_FIELD_MESSAGE),
            start: Yup.string()
                .required('This field is required')
                .matches(/^[0-9]+$/, 'Must be only digits')
                .min(4, 'Must be 4 digits')
                .test({
                message: "Min value is 1900",
                test: (value) => {
                    let yearValue = parseInt(value, 10);
                    return !isNaN(yearValue) && yearValue >= 1900;
                },
                }),
            end: Yup.string()
                .matches(/^[0-9]+$/, 'Must be only digits')
                .min(4, 'Must be 4 digits')
                .nullable()
                .when('isCurrent', {
                    is: false,
                    then: Yup.string().nullable().test({
                        message: "Min value is 1900",
                        test: (value) => {
                            let yearValue = parseInt(value, 10);
                            return !isNaN(yearValue) && yearValue >= 1900;
                        },
                    }),
                })
                .when('isCurrent', {
                    is: true,
                    then: Yup.string().nullable(),
                }),
            isCurrent: Yup.boolean(),
        }),
    )