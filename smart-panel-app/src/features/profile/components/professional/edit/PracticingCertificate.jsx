import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { FormItem } from '@components/Forms';
import { SPTextField, DatePicker } from '@components/Inputs';
import CustomizedRadios from '@components/FormsUI/RadioButton';
import { SPECIFIC_FIELD } from '@features/profile/utils/constants';

const useStyles = makeStyles({
  formTitle: {
    lineHeight: '33px',
    borderBottom: '1px solid #4126CF',
    marginBottom: 20,
  },
});

const PracticingCertificate = () => {
  const classes = useStyles();
  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography component="p" variant="subtitle1" className={classes.formTitle}>
            <b>Practicing Certificate</b>
          </Typography>
        </Grid>
      </Grid>
      <Grid container spacing={4}>
        <Grid item xs={12} md={12}>
          <FormItem
            label="Start Date"
            required
            name="practicingCertificate.startDate"
            isShowError={false}
          >
            <DatePicker />
          </FormItem>
        </Grid>
        <Grid item xs={12} md={12}>
          <FormItem
            label="End Date"
            required
            name="practicingCertificate.endDate"
            isShowError={false}
          >
            <DatePicker />
          </FormItem>
        </Grid>
        <Grid item xs={12} md={12}>
          <FormItem
            label="Currently practicing as a"
            required
            name="practicingCertificate.specialist"
          >
            <CustomizedRadios options={SPECIFIC_FIELD} />
          </FormItem>
        </Grid>
      </Grid>
    </div>
  );
};

export default PracticingCertificate;
