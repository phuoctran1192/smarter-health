import React, { useState } from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { FormItem } from '@components/Forms';
import { SPTextField, DatePicker } from '@components/Inputs';

const useStyles = makeStyles({
  formTitle: {
    lineHeight: '33px',
    borderBottom: '1px solid #4126CF',
    marginBottom: 20,
  },
});

const ProfessionalInfo = () => {
  const classes = useStyles();
  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography component="p" variant="subtitle1" className={classes.formTitle}>
            <b>Professional Information</b>
          </Typography>
        </Grid>
      </Grid>
      <Grid container spacing={4}>
        <Grid item xs={12} md={12}>
          <FormItem
            label="Medical Council Registration (MCR) No."
            required
            name="professionalInfo.mcrNumber"
          >
            <SPTextField />
          </FormItem>
        </Grid>
        <Grid item xs={12} md={12}>
          <FormItem
            label="Date of Full Registration"
            required
            name={`professionalInfo.dateOfFullRegistration`}
            isShowError={false}
          >
            <DatePicker />
          </FormItem>
        </Grid>
      </Grid>
    </div>
  );
};

export default ProfessionalInfo;
