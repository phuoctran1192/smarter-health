import React, { useEffect, useState } from 'react';
import { Grid, Typography, InputLabel } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { FormItem } from '@components/Forms';
import { DatePicker } from '@components/Inputs';
import CustomizedRadios from 'components/FormsUI/RadioButton';
import { Y_N_ANSWER } from '@features/profile/utils/constants';
import { useField, useFormikContext } from 'formik';
const useStyles = makeStyles({
  inputLabel: (props) => ({
    fontSize: 16,
    lineHeight: '24px',
    color: '#000',
    display: 'inline-block',
    alignItems: 'top',
    whiteSpace: 'pre-line',
    '& .MuiFormLabel-asterisk ': {
      color: 'red',
    },
    position: 'unset',
  }),
});

const OtherInfomation = () => {
  const classes = useStyles();
  const { values } = useFormikContext();
  const [showEntryDate, setShowEntryDate] = useState(
    values?.practicingCertificate?.isFamilyPhysicianRegister,
  );
  const handleOnChange = (value) => {
    setShowEntryDate(value === 'YES');
  };

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} md={12}>
        <FormItem
          label="Are you on the family physician register?"
          required
          name={`practicingCertificate.familyPhysicianRegister`}
        >
          <CustomizedRadios options={Y_N_ANSWER} handleOnChange={handleOnChange} />
        </FormItem>
      </Grid>
      {showEntryDate && (
        <Grid item xs={12} md={12}>
          <FormItem
            label="Entry Date"
            required
            name={`practicingCertificate.familyPhysicianRegisterEntryDate`}
            isShowError={false}
          >
            <DatePicker />
          </FormItem>
        </Grid>
      )}

      <Grid item xs={12} md={12}>
        <FormItem
          label=" Are you a designated workplace doctor?"
          required
          name={`practicingCertificate.designatedWorkplaceDoctor`}
        >
          <CustomizedRadios options={Y_N_ANSWER} />
        </FormItem>
      </Grid>
    </Grid>
  );
};

export default OtherInfomation;
