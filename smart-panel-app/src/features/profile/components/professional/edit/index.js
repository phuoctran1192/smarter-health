import React, { useState, useRef, useEffect } from 'react';
import Utils from 'utils';
import { Formik, Form } from 'formik';
import { Divider, Grid, makeStyles } from '@material-ui/core';
import { LoadingButton } from '@components/Buttons/LoadingButton';
import { SPMessageBox } from '@components/Elements/MessageBox';
import DoctorService from '@services/doctor.service';
import ProfessionalInfo from './ProfessionalInfo';
import Qualification from './qualification';
import OtherInfomation from './OtherInfo';
import PracticingCertificate from './PracticingCertificate';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import APIActions from '../../../store/APIActions';
import { getProfessionalInformationSchema } from '../../../validation/ProfessionalInformation';

const useStyles = makeStyles((theme) => ({
  divider: {
    margin: '30px 0',
    backgroundColor: theme.palette.primary.main,
    height: '2px',
  },
  button: {
    height: '60px',
    width: '100%',
    backgroundColor: theme.palette.primary.black,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
    lineHeight: '24px',
    cursor: 'pointer',
    borderRadius: '5px',
    marginTop: '60px',
    border: 'none',
  },
  cancel: {
    height: '60px',
    width: '100%',
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'black',
    lineHeight: '24px',
    cursor: 'pointer',
    borderRadius: '5px',
    border: '1px solid black',
  },
}));

const ProfessionalInformationForm = ({ doctorData, onEditInfoClick }) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const formikRef = useRef();
  const errorRef = useRef();
  const dispatch = useDispatch();
  const validationSchema = getProfessionalInformationSchema();

  let professionalInfoData = Utils.getPartObject(doctorData, 'mcrNumber', 'dateOfFullRegistration');
  professionalInfoData.dateOfFullRegistration = professionalInfoData.dateOfFullRegistration
    ? professionalInfoData.dateOfFullRegistration
    : '';

  const qualificationData = Utils.getPartObject(doctorData, 'qualifications') || {};

  let certificateData =
    Utils.getPartObject(
      doctorData.practicingCertificate,
      'startDate',
      'endDate',
      'isSpecialist',
      'isFamilyPhysicianRegister',
      'familyPhysicianRegisterEntryDate',
      'isDesignatedWorkplaceDoctor',
    ) || {};

  certificateData.endDate = certificateData.endDate ? certificateData.endDate : '';
  certificateData.startDate = certificateData.startDate ? certificateData.startDate : '';
  certificateData.familyPhysicianRegisterEntryDate =
    certificateData.familyPhysicianRegisterEntryDate
      ? certificateData.familyPhysicianRegisterEntryDate
      : '';

  certificateData.familyPhysicianRegister = certificateData.isFamilyPhysicianRegister
    ? 'YES'
    : 'NO';
  certificateData.designatedWorkplaceDoctor = certificateData.isDesignatedWorkplaceDoctor
    ? 'YES'
    : 'NO';
  certificateData.specialist = certificateData.isSpecialist ? 'GENERAL_PRACTITIONER' : 'SPECIALIST';

  const initialValues = {
    professionalInfo: professionalInfoData,
    ...qualificationData,
    practicingCertificate: certificateData,
  };

  const save = (payload) => {
    setLoading(true);
    setError('');
    DoctorService.updateDoctorProfessional(payload)
      .then((data) => {
        dispatch(APIActions.fetchDoctor(data.id));
        onEditInfoClick();
      })
      .catch((error) => {
        if (error.response) {
          let msg = 'Failed to submit form. ';
          if (error.response && error.response.data) {
            const { errors } = error.response.data;
            if (errors?.length) {
              msg = errors.reduce(
                (prev, curr) => {
                  const temp = prev.concat(curr.message);
                  return temp;
                },
                [msg],
              );
            }
          }
          setError(msg);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const onCancel = () => {
    onEditInfoClick();
  };

  const submit = (data) => {
    data.practicingCertificate.isFamilyPhysicianRegister =
      data.practicingCertificate.familyPhysicianRegister === 'YES';
    data.practicingCertificate.isDesignatedWorkplaceDoctor =
      data.practicingCertificate.designatedWorkplaceDoctor === 'YES';
    data.practicingCertificate.isSpecialist =
      data.practicingCertificate.specialist === 'SPECIALIST';

    const payload = {
      mcrNumber: data.professionalInfo.mcrNumber,
      firstRegistrationDate: data.professionalInfo.dateOfFullRegistration,
      practicingCertificate: {
        startDate: data.practicingCertificate.startDate,
        endDate: data.practicingCertificate.endDate,
        isSpecialist: data.practicingCertificate.isSpecialist,
        isFamilyPhysicianRegister: data.practicingCertificate.isFamilyPhysicianRegister,
        familyPhysicianRegisterEntryDate: data.practicingCertificate.isFamilyPhysicianRegister
          ? data.practicingCertificate.familyPhysicianRegisterEntryDate
          : '',
        isDesignatedWorkplaceDoctor: data.practicingCertificate.isDesignatedWorkplaceDoctor,
        specialities: [],
      },
      qualifications: data.qualifications.map((qualification) => ({
        ...qualification,
        year: parseInt(qualification.year),
      })),
    };
    save(payload);
  };

  useEffect(() => {
    if (error && error !== '') {
      errorRef.current.scrollIntoView({
        behavior: 'smooth',
        block: 'nearest',
        inline: 'start',
      });
    }
  }, [error]);

  return (
    <Formik
      innerRef={formikRef}
      validationSchema={validationSchema}
      initialValues={initialValues}
      onSubmit={submit}
    >
      <Form>
        <Grid item xs={12} ref={errorRef}>
          <SPMessageBox error message={error}></SPMessageBox>
        </Grid>

        <ProfessionalInfo name="professionalInfo" />
        <Qualification name="qualifications" />
        <Divider classes={{ root: classes.divider }} />
        <OtherInfomation name="otherInfo" />
        <div style={{ marginTop: '40px' }}>
          <PracticingCertificate name="practicingCertificate" />
        </div>
        <Grid container style={{ marginTop: '40px' }} spacing={6}>
          <Grid item xs={6} lg={6}>
            <LoadingButton
              type="button"
              text="Cancel"
              onClick={onCancel}
              className={classes.cancel}
            />
          </Grid>
          <Grid item xs={6} lg={6}>
            <LoadingButton type="submit" loading={loading} text="Save" />
          </Grid>
        </Grid>
      </Form>
    </Formik>
  );
};

export default ProfessionalInformationForm;
