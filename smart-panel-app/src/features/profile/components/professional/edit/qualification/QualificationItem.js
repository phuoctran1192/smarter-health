import { Grid } from '@material-ui/core';
import React from 'react';
import { FormItem } from '@components/Forms';
import { SPTextField, withAddable } from '@components/Inputs';

import { makeStyles } from '@material-ui/core/styles';
import { YearPicker } from '../../../../../../components/Inputs/YearPicker';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '0',
  },
}));

const QualificationItem = ({ value, index, ...props }) => {
  const classes = useStyles();
  return (
    <Grid container spacing={4} className={classes.root}>
      <Grid item xs={12} md={6}>
        <FormItem required label="Academic Title" name={`qualifications[${index}].name`}>
          <SPTextField />
        </FormItem>
      </Grid>
      <Grid item xs={12} md={6}>
        <FormItem required label="Year" name={`qualifications[${index}].year`} isShowError={false}>
          <YearPicker />
        </FormItem>
      </Grid>
      <Grid item xs={12} md={12}>
        <FormItem required label="Institution Name" name={`qualifications[${index}].institution`}>
          <SPTextField />
        </FormItem>
      </Grid>
    </Grid>
  );
};

export default withAddable(QualificationItem);
