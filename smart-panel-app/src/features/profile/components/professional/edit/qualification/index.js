import React from 'react';
import { Field } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import QualificationItem from './QualificationItem';

const useStyles = makeStyles({
  root: {
    marginTop: 40,
  },
  groupInput: {
    padding: '0',
  },
  formTitle: {
    lineHeight: '33px',
    borderBottom: '1px solid #4126CF',
    marginBottom: 20,
  },
});

const Qualification = ({ qualificationData }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Field
        name="qualifications"
        title="Qualification(s)"
        subTitle="Qualification"
        component={QualificationItem}
      />
    </div>
  );
};

export default Qualification;
