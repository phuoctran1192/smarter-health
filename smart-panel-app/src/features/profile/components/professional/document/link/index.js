import React from 'react';
import { BASE_URL, SMART_PANEL_PREFIX } from 'services/utils/constants';

export const DocumentLink = ({ file }) => {
  const url = `${BASE_URL}${SMART_PANEL_PREFIX}/v1/document?uploadID=`;
  return (
    <>
      {file && file.fileName && file.uploadId ? (
        <a
          download={`${file.fileName}`}
          target="_blank"
          href={`${url}${file.uploadId}`}
        >
          {file.fileName}.{file.fileExtension}
        </a>
      ) : (
        'There is no file'
      )}
    </>
  );
};
