import React from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { Divider, Grid, makeStyles, Typography } from '@material-ui/core';
import Utils from 'utils';
import { MultipleFileUploadField } from 'components/upload-file/MultipleFileUploadField';
import DoctorService from 'services/doctor.service';
import { useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  divider: {
    margin: '30px 0',
    backgroundColor: theme.palette.primary.main,
    height: '2px',
  },
  button: {
    height: '60px',
    width: '100%',
    backgroundColor: theme.palette.primary.black,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
    lineHeight: '24px',
    cursor: 'pointer',
    borderRadius: '5px',
    marginTop: '60px',
    border: 'none',
  },
  buttonCancel: {
    height: '60px',
    width: '100%',
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'black',
    lineHeight: '24px',
    cursor: 'pointer',
    borderRadius: '5px',
    marginTop: '60px',
    border: '1px solid black',
  },
  headingText: {
    marginTop: 6,
    marginBottom: 33,
  },
  sectionTitle: {
    marginTop: 40,
    marginBottom: 20,
  },
}));

const DocumentEditForm = ({ onCancelEditDocument }) => {
  const doctorData = useSelector((state) => state.doctorProfile.doctor);
  const specialityList = useSelector(state => state.doctorProfile.meta.specialities) || [];
  const classes = useStyles();
  const documentData = Utils.getPartObject(doctorData, 'practicingCertificate') || {};
  const submit = (values) => {
    const specialitiesData = values.practicingCertificate?.specialities.map((sitem) => {
      return Utils.getPartObject(sitem, 'id', 'documents') || {};
    });
    const {
      physicianPracticingCertificateDocuments,
      practicingCertificateDocuments,
      workplaceDoctorPracticingCertificateDocuments,
    } = values?.practicingCertificate || [];
    const data = {
      practicingCertificateDocuments: practicingCertificateDocuments,
      physicianPracticingCertificateDocuments: physicianPracticingCertificateDocuments,
      workplaceDoctorPracticingCertificateDocuments: workplaceDoctorPracticingCertificateDocuments,
      specialities: specialitiesData,
    };
    DoctorService.updateDoctorProfessionalDocument(data)
      .then((res) => {
        if (res?.code && res?.data) {
          onCancelEditDocument();
        }
      })
      .catch((err) => console.log(err));
  };

  const cancelEditDocument = () => {
    onCancelEditDocument();
  };

  const validationSchema = Yup.object();

  return (
    <Formik initialValues={documentData} validationSchema={validationSchema} onSubmit={submit}>
      <Form>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography component="p" variant="subtitle1">
              Upload Supporting Document(s)
            </Typography>
            <Typography component="p" variant="body1" className={classes.headingText}>
              We will need to validate your information. Please upload a valid practicing
              certificate and other supporting document(s). Files should be less than 200MB (JPG,
              PNG or PDF).
            </Typography>
            <div className="break"></div>
          </Grid>
          <Grid item xs={12}>
            <Typography component="p" variant="subtitle2" className={classes.sectionTitle}>
              Practicing Certificate
            </Typography>
            <MultipleFileUploadField
              name="practicingCertificate.practicingCertificateDocuments"
              uploaded={documentData.practicingCertificate.practicingCertificateDocuments}
            />
            <div className="break" style={{ marginTop: '50px' }}></div>
          </Grid>
          {/* specialities */}
          {documentData.practicingCertificate.specialities.length > 0 &&
            documentData.practicingCertificate.isSpecialist === true &&
            documentData.practicingCertificate.specialities.map((spec, idx) => {
              const sp = specialityList.filter(s => (s.id === spec.speciality))
              return (
                <Grid key={idx} item xs={12}>
                  <Typography component="p" variant="subtitle2" className={classes.sectionTitle}>
                    Specialty: {sp[0]?.name}
                  </Typography>
                  <MultipleFileUploadField
                    name={`practicingCertificate.specialities[${idx}].documents`}
                    uploaded={documentData.practicingCertificate.specialities[idx].documents}
                  />
                  <div className="break" style={{ marginTop: '50px' }}></div>
                </Grid>
              );
            })}
          {/* specialities */}
          {documentData.practicingCertificate.isFamilyPhysicianRegister === true && (
            <Grid item xs={12}>
              <Typography component="p" variant="subtitle2" className={classes.sectionTitle}>
                Family Physician Register
              </Typography>
              <MultipleFileUploadField
                name="practicingCertificate.physicianPracticingCertificateDocuments"
                uploaded={
                  documentData.practicingCertificate.physicianPracticingCertificateDocuments
                }
              />
              <div className="break" style={{ marginTop: '50px' }}></div>
            </Grid>
          )}
          {documentData.practicingCertificate.isDesignatedWorkplaceDoctor === true && (
            <Grid item xs={12}>
              <Typography component="p" variant="subtitle2" className={classes.sectionTitle}>
                Designated Workplace Doctor
              </Typography>
              <MultipleFileUploadField
                name="practicingCertificate.workplaceDoctorPracticingCertificateDocuments"
                uploaded={
                  documentData.practicingCertificate.workplaceDoctorPracticingCertificateDocuments
                }
              />
              <div style={{ marginBottom: '30px' }}></div>
            </Grid>
          )}
        </Grid>
        <Divider classes={{ root: classes.divider }} />
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <button className={classes.buttonCancel} onClick={cancelEditDocument} type="button">
              Cancel
            </button>
          </Grid>
          <Grid item xs={12} md={6}>
            <button className={classes.button} type="submit">
              Save
            </button>
          </Grid>
        </Grid>
      </Form>
    </Formik>
  );
};

export default DocumentEditForm;
