import React, { useContext, useEffect, useState } from 'react';
import TableContainer from '@material-ui/core/TableContainer';
import Paper from '@material-ui/core/Paper';
import Utils from 'utils';
import CustomTable from '@features/profile/components/table';
import DoctorService from 'services/doctor.service';
import { ApplicantContext } from 'features/profile/context';
import MetaService from '@services/meta.service';

export default function DocumentTable({ doctorData, specialityList }) {
  const { editable } = useContext(ApplicantContext);
  const [doctorAllData, setDoctorAllData] = useState({});
  const [doctorSpecialityList, setDoctorSpecialityList] = useState(specialityList);
  useEffect(() => {
    if (editable) {
      DoctorService.getDoctorProfile()
        .then((res) => {
          if (res?.code && res?.data) {
            setDoctorAllData(res.data);
          }
        })
        .catch((err) => console.log(err));
    } else {
      setDoctorAllData(doctorData);
    }
    if (!specialityList) {
      MetaService.getMeta('specialities')
        .then((res) => {
          if (res?.code && res?.data) {
            setDoctorSpecialityList(res.data);
          }
        })
        .catch((err) => console.log(err));
    }
  }, []);
  const practicingCertificate =
    Utils.getPartObject(doctorAllData, 'practicingCertificate')?.practicingCertificate || {};
  const {
    specialities,
    practicingCertificateDocuments,
    physicianPracticingCertificateDocuments,
    workplaceDoctorPracticingCertificateDocuments,
  } = practicingCertificate || [];

  const getPaternAndData = (orinal, prefix, title) => {
    const patern = [];
    let data = [{}];
    orinal.map((item, idx) => {
      patern.push({
        name: '',
        key: `${prefix}_${idx}`,
      });
      patern[0].name = title;
      data[0][`${prefix}_${idx}`] = {
        fileName: `${item.fileName}.${item.fileExtension}`,
        uploadId: item.uploadId,
      };
    });
    return { patern, data };
  };
  const practicingCertificateDocumentsPatternAndData = getPaternAndData(
    practicingCertificateDocuments || [],
    'prac',
    'Practicing Certification(s)',
  );
  const physicianPracticingCertificateDocumentsPatternAndData = getPaternAndData(
    physicianPracticingCertificateDocuments || [],
    'phy',
    'Family Physician Register',
  );
  const workplaceDoctorPracticingCertificateDocumentsPatternAndData = getPaternAndData(
    workplaceDoctorPracticingCertificateDocuments || [],
    'wor',
    'Workplace doctor Certification(s)',
  );
  const specialitiesDocumentList = [];
  (specialities || []).map((s) => {
    const name = (doctorSpecialityList || []).filter((spec) => spec.id === s.speciality)[0]?.name;
    const specialityPatternAndData = getPaternAndData(s.documents, 'spe', name);
    specialitiesDocumentList.push(specialityPatternAndData);
  });
  return (
    <TableContainer component={Paper} style={{ boxShadow: 'none' }}>
      <CustomTable
        title="Upload Supporting Document(s)"
        rowPattern={practicingCertificateDocumentsPatternAndData.patern}
        data={practicingCertificateDocumentsPatternAndData.data}
      />
      <CustomTable
        noSpace={true}
        rowPattern={physicianPracticingCertificateDocumentsPatternAndData.patern}
        data={physicianPracticingCertificateDocumentsPatternAndData.data}
      />
      <CustomTable
        noSpace={true}
        rowPattern={workplaceDoctorPracticingCertificateDocumentsPatternAndData.patern}
        data={workplaceDoctorPracticingCertificateDocumentsPatternAndData.data}
      />
      {specialitiesDocumentList.map((s, idx) => {
        return s.data.length ? <CustomTable noSpace={true} rowPattern={s.patern} data={s.data} key={idx} /> : null
      })}
    </TableContainer>
  );
}
