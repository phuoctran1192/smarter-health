import React, { useEffect, useState } from 'react';
import TableContainer from '@material-ui/core/TableContainer';
import Paper from '@material-ui/core/Paper';
import Utils from 'utils';
import CustomTable from '@features/profile/components/table';
import { SPECIFIC_FIELD, Y_N_ANSWER } from '../../../utils/constants';
import MetaService from 'services/meta.service';
import DoctorService from 'services/doctor.service';

const PROFESSIONAL_INFORMATION = [
  {
    name: 'Medical Council Registration (MCR) No.*',
    key: 'mcrNumber',
  },
  {
    name: 'Date of Full Registration*',
    key: 'dateOfFullRegistration',
  },
];

const QUALIFICATION = [
  {
    name: 'Academic Title*',
    key: 'name',
  },
  {
    name: 'Year',
    key: 'year',
  },
  {
    name: 'Institution Name*',
    key: 'institution',
  },
];

let PRACTICING_CERTIFICATE = [
  {
    name: 'Start Date',
    key: 'startDate',
  },
  {
    name: 'End Date',
    key: 'endDate',
  },
  {
    name: 'Currrently  Practicing as a',
    key: 'position',
  },
];

const OTHERS = [
  {
    name: 'Are you on the family physician register?',
    key: 'isFamilyPhysicianRegister',
  },
  {
    name: 'Entry Date',
    key: 'familyPhysicianRegisterEntryDate',
  },
  {
    name: 'Are you a designated workplace doctor?',
    key: 'isDesignatedWorkplaceDoctor',
  },
];

export default function ProfessionalInformationTable({ doctorData }) {
  const [doctorSpecialityList, setDoctorSpecialityList] = useState([]);
  const [aoiData, setAoiData] = useState({})
  const [finalPracticingData, setFinalPracticingData] = useState([])
  const [finalPracticingPattern, setFinalPracticingPattern] = useState([])
  useEffect(() => {
    MetaService.getMeta('specialities')
      .then((res) => {
        if (res?.code && res?.data) {
          setDoctorSpecialityList(res.data);
        }
      })
      .catch((err) => console.log(err));
      DoctorService.getDoctorWorkBackground()
      .then((res) => {
        if (res?.data) {
          const { areasOfInterest } = res.data.content;
          MetaService.getMeta('area-of-interests')
            .then((aoiRes) => {
              if (aoiRes?.code && aoiRes?.data) {
                let aoiName = {}
                areasOfInterest?.map((a, idx) => {
                  const selectAoi = aoiRes.data.filter(aoiItem => aoiItem.id === a)
                  aoiName = {...aoiName, [`aoi_${idx}`]: selectAoi[0].name}
                })
                setAoiData(aoiName);
              }
            })
            .catch((err) => console.log(err));
        }
      })
      .catch((err) => console.log(err));
  }, []);
  useEffect(() => {
    const finalData = Object.assign(practicingCertificateData[0], aoiData);
    practicingCertificateData = [finalData]
    let pPattern = [];
    Object.keys(aoiData).forEach((key, idx) => {
      if (idx === 0) {
        pPattern.push({
          name: "Area of Interest",
          key: key
        })
      } else {
        pPattern.push({
          name: "",
          key: key
        })
      }
    })
    const finalPattern = FINAL_PRACTICING_CERTIFICATE.concat(pPattern)
    setFinalPracticingPattern(finalPattern);
    setFinalPracticingData(practicingCertificateData);
  }, [aoiData]);

  const professionalInfoData = [
    Utils.getPartObject(doctorData, 'mcrNumber', 'dateOfFullRegistration'),
  ];

  const qualificationData = Utils.getPartObject(doctorData, 'qualifications')?.qualifications || [];
  let practicingCertificateData = Utils.getPartObject(
    doctorData.practicingCertificate,
    'startDate',
    'endDate',
    'isSpecialist',
    'specialities',
  );
  const specialityArray = practicingCertificateData.specialities;
  const SPEC_PATTERN = [];
  const SPEC_DATA = {};
  specialityArray.map((s, idx) => {
    SPEC_PATTERN.push({
      name: 'Specialty/ Sub-Specialty',
      key: `spec_${idx}`,
    });
    SPEC_PATTERN.push({
      name: 'Start Date',
      key: `spec_year_${idx}`,
    });
    const name = (doctorSpecialityList || []).filter((spec) => spec.id === s.speciality)[0]?.name;
    SPEC_DATA[`spec_${idx}`] = name;
    SPEC_DATA[`spec_year_${idx}`] = s.entryDate;
  });
  const FINAL_PRACTICING_CERTIFICATE = PRACTICING_CERTIFICATE.concat(SPEC_PATTERN);
  practicingCertificateData = { ...practicingCertificateData, ...SPEC_DATA };
  practicingCertificateData.position = practicingCertificateData.isSpecialist
    ? SPECIFIC_FIELD.SPECIALIST
    : SPECIFIC_FIELD.GENERAL_PRACTITIONER;
  practicingCertificateData = [practicingCertificateData];

  let othersData = Utils.getPartObject(
    doctorData.practicingCertificate,
    'isFamilyPhysicianRegister',
    'familyPhysicianRegisterEntryDate',
    'isDesignatedWorkplaceDoctor',
  );
  othersData.isFamilyPhysicianRegister = othersData.isFamilyPhysicianRegister
    ? Y_N_ANSWER.YES
    : Y_N_ANSWER.NO;
  othersData.isDesignatedWorkplaceDoctor = othersData.isDesignatedWorkplaceDoctor
    ? Y_N_ANSWER.YES
    : Y_N_ANSWER.NO;
  othersData = [othersData];

  return (
    <TableContainer component={Paper} style={{ boxShadow: 'none' }}>
      <CustomTable
        title={'Professional Information'}
        subtitle={'Professional Information'}
        rowPattern={PROFESSIONAL_INFORMATION}
        data={professionalInfoData}
      />
      <CustomTable
        title={'Qualification(s)'}
        subtitle={'Qualification'}
        isMultiple
        rowPattern={QUALIFICATION}
        data={qualificationData}
      />
      <CustomTable
        title={'Practicing Certificate'}
        subtitle={'Practicing Certificate'}
        rowPattern={finalPracticingPattern}
        data={finalPracticingData}
      />
      <CustomTable rowPattern={OTHERS} data={othersData} />
    </TableContainer>
  );
}
