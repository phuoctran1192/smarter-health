import React, { useEffect, useState } from 'react';
import { Divider, Grid, makeStyles } from '@material-ui/core';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';

import BackgroundFormSection from '../FormSection/BackgroundFormSection';
import WorkExperienceFormSection from '../FormSection/WorkExperienceFormSection';
import AccreditedFacilityFormSection from '../FormSection/AccreditedFacilityFormSection';

import { WORK_EXPERIENCE_FORM_SECTION_SCHEMA } from '../../validation/workExperience';
import MembershipOfProfessionalBodiesFormSection from '../FormSection/MembershipOfProfessionalBodiesFormSection';
import AdditionalInfoFormSection from '../FormSection/AdditionalInfoFormSection';
import { getDoctorWorkBackgroundSectionSchema } from 'features/profile/validation/doctorWorkBackground';
import DoctorSpecialAreasOfInterest from '../FormSection/SpecialAreasOfInterest';
import { useDispatch, useSelector } from 'react-redux';
import APIActions from '../../store/APIActions';
import Actions from '../../store/Actions';
import PlaceOfPracticeFormSection from '../FormSection/PlaceOfPracticeFormSection';
import { PLACE_OF_PRACTICE_FORM_SECTION_SCHEMA } from 'features/profile/validation/placeOfPractice';
import ErrorIcon from '@assets/icons/Error.svg';

const useStyles = makeStyles((theme) => ({
  divider: {
    margin: 0,
    backgroundColor: theme.palette.primary.main,
    height: '2px',
  },
  button: {
    height: '60px',
    width: '100%',
    backgroundColor: theme.palette.primary.black,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
    lineHeight: '24px',
    cursor: 'pointer',
    borderRadius: '5px',
    border: 'none',
  },
  buttonCancel: {
    height: '60px',
    width: '100%',
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'black',
    lineHeight: '24px',
    cursor: 'pointer',
    borderRadius: '5px',
    border: '1px solid black',
  },
  submitErrBox: {
    borderRadius: '12px',
    padding: '20px 20px',
    border: '1px solid red',
    position: 'relative',
    color: 'red',
    textAlign: 'center'
  },
  warning: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    border: '1px solid #FF0000',
    borderRadius: '77px',
    padding: '13px 0',
    width: '85%',
    margin: 'auto',
    marginBottom: '40px',
  },
  wrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  warningText: {
    marginLeft: '10px',
    color: '#FF0000',
  },
}));

const WorkBackgroundForm = ({ doctor, onChange }) => {
  const classes = useStyles();
  const editingWorkBackgroundErrorMessage = useSelector(
    (state) => state.doctorProfile?.errorMessage.workBackgroundSubmitErrorMessage,
  );
  const [submitErrMsg, setSubmitErrMsg] = useState("");

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(APIActions.fetchDoctorWorkBackground());
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
    var msg = "";
    if (editingWorkBackgroundErrorMessage.response && editingWorkBackgroundErrorMessage.response.data) {
      msg += editingWorkBackgroundErrorMessage.response.data.errors[0].message;
    }
    setSubmitErrMsg(msg);
  }, [editingWorkBackgroundErrorMessage]);

  const isSpecialist = doctor?.practicingCertificate?.isSpecialist;

  const validationSchema = Yup.object().shape({
    ...getDoctorWorkBackgroundSectionSchema(isSpecialist),
    workExperiences: WORK_EXPERIENCE_FORM_SECTION_SCHEMA,
    practicingPlaces: PLACE_OF_PRACTICE_FORM_SECTION_SCHEMA
  });

  return (
    <Formik
      enableReinitialize
      initialValues={{ ...doctor }}
      validationSchema={validationSchema}
      onSubmit={(values) => onChange && onChange(values)}
    >
      <Form>
        {
          !doctor.hasFullProfile && (
            <div className={classes.warning}>
              <div className={classes.wrapper}>
                <div className={classes.icon}>
                  <img src={ErrorIcon} />
                </div>
                <div className={classes.warningText}>
                  Please complete this section in order to upgrade your profile status to FULL
                </div>
              </div>
            </div>
          )
        }
        {submitErrMsg && submitErrMsg !== "" && <Grid item xs={12}>
          <div className="container-930">
            <p className={classes.submitErrBox}>{submitErrMsg}</p>
          </div>
        </Grid>}
        {isSpecialist && <DoctorSpecialAreasOfInterest />}
        <BackgroundFormSection />
        <PlaceOfPracticeFormSection />
        <AccreditedFacilityFormSection />
        <WorkExperienceFormSection />
        <MembershipOfProfessionalBodiesFormSection />
        <Divider className={classes.divider} />
        <AdditionalInfoFormSection />
        <Grid container spacing={2}>
          {
            (doctor.hasFullProfile) && (
              <Grid item xs>
                <button
                  className={classes.buttonCancel}
                  onClick={() => dispatch(Actions.editWorkBackground(false))}
                  type="button"
                >
                  Cancel
                </button>
              </Grid>
            )
          }
          <Grid item xs>
            <button className={classes.button} type="submit">
              Save
            </button>
          </Grid>
        </Grid>
      </Form>
    </Formik>
  );
};

export default WorkBackgroundForm;
