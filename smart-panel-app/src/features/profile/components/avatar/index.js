import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import { CardContent, Typography } from '@material-ui/core';
import MetaService from '@services/meta.service';
import { uploadFile } from 'services/upload-file.service';
import DoctorService from 'services/doctor.service';
import { BASE_URL, SMART_PANEL_PREFIX } from 'services/utils/constants';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('992')]: {
      flexDirection: 'column',
    },
    '&> *': {
      padding: 0,
    },
  },
  content: {
    paddingBottom: '0 !important',
  },
  input: {
    display: 'none',
  },
  avatar: {
    width: 170,
    height: 170,
    borderRadius: '50%',
    border: '3px solid #4024CD',
  },
  inputLabel: {
    position: 'relative',
    [theme.breakpoints.up('993')]: {
      marginRight: 60,
    },
  },
  editIcon: {
    width: 34,
    height: 34,
    backgroundImage: `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='34' height='34' viewBox='0 0 34 34' fill='none'%3E%3Cpath d='M28.3917 14.1666L19.9796 5.66683L0.350951 25.5002L0 33.6464L8.76297 34L28.3917 14.1666Z' fill='black'/%3E%3Cpath d='M25.587 0L21.3815 4.25037L29.7935 12.7501L34 8.49975L25.587 0Z' fill='black'/%3E%3C/svg%3E")`,
    position: 'absolute',
    zIndex: 2,
    bottom: '50%',
    right: 0,
  },
  gray: {
    color: '#737373',
  },
  name: {
    [theme.breakpoints.down('992')]: {
      fontSize: 22,
    },
  },
  avatarText: {
    [theme.breakpoints.down('992')]: {
      fontSize: 18,
    },
  },
}));

export default function ProfileAvatar({ doctor, editable = true }) {
  const [country, setCountry] = useState('');
  const [avatarId, setAvatarId] = useState(doctor?.uploadedPhotoId);

  useEffect(() => {
    MetaService.getMeta('countries')
      .then((res) => {
        if (res?.code && res?.data) {
          const country = res.data.filter((c) => c.id === doctor.address.country);
          setCountry(country[0]?.name);
        }
      })
      .catch((err) => console.log(err));
  }, []);

  function handleFile(e) {
    uploadFile(
      e.target.files[0],
      () => { },
      () => { },
    ).then(({ uploadId }) => {
      DoctorService.updateDoctorAvatar(uploadId).then((res) => {
        if (res?.code && res?.data) {
          setAvatarId(uploadId)
        }
      });
    });
  }

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <label htmlFor="icon-button-file" className={classes.inputLabel}>
        <IconButton color="primary" aria-label="upload picture" component="span">
          {editable ? <span className={classes.editIcon}></span> : null}
          <Avatar src={`${BASE_URL}${SMART_PANEL_PREFIX}/v1/document?uploadID=${avatarId}`} className={classes.avatar} />
        </IconButton>
      </label>
      <input
        accept="image/*"
        className={classes.input}
        id="icon-button-file"
        type="file"
        onChange={handleFile}
        disabled={!editable}
      />
      <CardContent className={classes.content}>
        <Typography component="h3" variant="h3" className={classes.name}>
          {doctor.firstName + ' ' + doctor.lastName}
        </Typography>
        <Typography variant="subtitle1" className={`${classes.gray} ${classes.avatarText}`}>
          {country}
        </Typography>
      </CardContent>
    </div>
  );
}
