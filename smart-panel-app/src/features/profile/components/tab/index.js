import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PersonalInformationTab from '@features/profile/sections/personal-information';
import ProfessionalInformationTab from '@features/profile/sections/professional';
import { ApplicantProvider } from '../../context';
import { useSelector } from 'react-redux';

function TabPanel({ children, value, index, ...props }) {
  return <div>{value === index && <>{children}</>}</div>;
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    marginTop: 80,
  },
  tabTitle: {
    [theme.breakpoints.up('993')]: {
      fontSize: 24,
      lineHeight: '36px',
      padding: '27px 56px',
      whiteSpace: 'nowrap',
    },
    fontWeight: 600,
    maxWidth: '50%',
    color: '#C4C4C4',
    borderRadius: '10px 10px 0 0',
    '&.Mui-selected': {
      color: 'black',
      borderBottom: 'none',
      backgroundColor: '#F7F7F7',
    },
  },
}));

export default function ProfileTab({ doctor, specialityList, genderList, editable = true }) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const identityTypes = useSelector((state) => state.doctorProfile?.meta.identityTypes) || [];

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <ApplicantProvider editable={editable}>
      <div className={classes.root}>
        <Tabs value={value} onChange={handleChange} aria-label="profile" centered>
          <Tab label=" Personal Information" className={classes.tabTitle} />
          <Tab label="Professional Information" className={classes.tabTitle} />
        </Tabs>
        <TabPanel value={value} index={0}>
          <PersonalInformationTab
            doctor={doctor}
            genderList={genderList}
            identityList={identityTypes}
          />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <ProfessionalInformationTab doctor={doctor} specialityList={specialityList} />
        </TabPanel>
      </div>
    </ApplicantProvider>
  );
}
