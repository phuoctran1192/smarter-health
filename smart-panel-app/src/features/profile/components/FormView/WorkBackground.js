import React from 'react';
import TableContainer from '@material-ui/core/TableContainer';
import Paper from '@material-ui/core/Paper';
import Utils from 'utils';
import TableCustom from 'features/profile/components/table';
import { get } from 'lodash';
import { Y_N_ANSWER } from 'features/profile/utils/constants';
import { v4 as uuidv4 } from 'uuid';

function getNewKey() {
  return uuidv4();
}

const workBackgroundPattern = [
  {
    name: 'Background',
    key: 'background',
  },
];

const workExperiencePattern = [
  {
    name: 'Organization Name',
    key: 'organization',
  },
  {
    name: 'Department',
    key: 'department',
  },
  {
    name: 'Position',
    key: 'position',
  },
  {
    name: 'Start',
    key: 'range',
  },
];

let membershipPattern = [];
const othersPattern = [
  {
    name: 'I am a panel doctor of',
    key: 'panelDoctor',
  },
  {
    name: 'Which clinic management system are you currently using?',
    key: 'clinicManagementSystem',
  },
];
let placeOfPracticePattern = [
  {
    name: 'Name of Practice / Department',
    key: 'name',
  },
  {
    name: '',
    key: 'licenseNumber',
  },
  {
    name: '',
    key: 'contactNumber',
  },
  {
    name: '',
    key: 'email',
  },
  {
    name: '',
    key: 'website',
  },
  {
    name: '',
    key: 'number',
  },
  {
    name: 'Address 1 (Street Name)',
    key: 'street',
  },
  {
    name: 'Address 2 (Building Name)',
    key: 'building',
  },
  {
    name: '',
    key: 'block',
  },
  {
    name: '',
    key: 'level',
  },
  {
    name: '',
    key: 'postalCode',
  },
  {
    name: 'Consultation Schedule',
    key: 'consultationScheduleDate',
  },
];
export default function WorkBackGroundTable({ doctorData }) {
  const workBackgroundData = [Utils.getPartObject(doctorData, 'background')];
  
  const practicingPlace = get(doctorData, 'practicingPlaces', []);
  let practicingPlaceData = [];
  practicingPlace.map((p) => {
    const { consultationSchedules } = p;
    const {street, postalCode, number, level, building, block} = p.address;
    let cSchedulesDay = [];
    let cSchedulesSession = {};
    let cSchedulesDaySession = [];
    const ukey = getNewKey();
    consultationSchedules.map((c) => {
      cSchedulesDay.push(c.weekday)
      cSchedulesDaySession.push({name: c.weekday, key: ukey})
      let sessionRange = [];
      c.sessions.map((ss) => {
        sessionRange.push(`${ss.startTime} - ${ss.endTime}`)
      })
      cSchedulesSession[ukey] = sessionRange.join(' , ')
    })
    p = {...p, street: street, postalCode: postalCode, number: number,level: level, building: building, block: block, consultationScheduleDate: cSchedulesDay.join(','), ...cSchedulesSession};
    practicingPlaceData = [...practicingPlaceData, p];
    placeOfPracticePattern = [...placeOfPracticePattern, ...cSchedulesDaySession]
  });

  let workExperience = get(doctorData, 'workExperiences', []);
  const workExperienceData = workExperience.map(we => {
    let range = "";
    if (we.isCurrent) {
      range = `${we.start} - Current`
    } else {
      range = `${we.start} - ${we.end}`
    }
    return {...we, range: range}
  })
  let membershipData = Utils.getPartObject(doctorData, 'isSMAMember', 'isCFPSMember', 'isAMSMember');
  membershipData.isSMAMember = membershipData.isSMAMember ? Y_N_ANSWER.YES : Y_N_ANSWER.NO;
  membershipData.isCFPSMember = membershipData.isCFPSMember ? Y_N_ANSWER.YES : Y_N_ANSWER.NO;
  membershipData.isAMSMember = membershipData.isAMSMember ? Y_N_ANSWER.YES : Y_N_ANSWER.NO;
  membershipData = [membershipData];

  const othersData = [Utils.getPartObject(doctorData, 'clinicManagementSystem')];

  const { isSpecialist } = doctorData.practicingCertificate;

  if (isSpecialist) {
    membershipPattern = [
      {
        name: 'Member of Singapore Medical Association (SMA)?',
        key: 'isSMAMember',
      },
      {
        name: 'Member of Academy of Medicine Singapore (AMS)?*',
        key: 'isAMSMember',
      }
    ]
  } else {
    membershipPattern = [
      {
        name: 'Member of Singapore Medical Association (SMA)?',
        key: 'isSMAMember',
      },
      {
        name: 'Member of College of Family Physicians Singapore (CFPS)?*',
        key: 'isCFPSMember',
      }
    ]
  }

  return (
    <TableContainer component={Paper} style={{ boxShadow: 'none' }}>
      <TableCustom
        title="Work Background"
        subtitle="Work Background"
        rowPattern={workBackgroundPattern}
        data={workBackgroundData}
        titleBorderColor={true}
      />
      <TableCustom
        title="Place of Practice"
        subtitle="Place of Practice"
        rowPattern={placeOfPracticePattern}
        data={practicingPlaceData}
        titleBorderColor={true}
      />
      <TableCustom
        title="Work Experience"
        subtitle="Work Experience"
        rowPattern={workExperiencePattern}
        data={workExperienceData}
        titleBorderColor={true}
      />
      <TableCustom
        title="Membership of Professional Bodies"
        subtitle="Membership of Professional Bodies"
        rowPattern={membershipPattern}
        data={membershipData}
        titleBorderColor={true}
      />
      <TableCustom tilte={''} subtitle={''} rowPattern={othersPattern} data={othersData} />
    </TableContainer>
  );
}
