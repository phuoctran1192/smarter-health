import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Divider, Typography } from '@material-ui/core';
import clsx from 'clsx';
import { DocumentLink } from 'features/profile/components/professional/document/link';

const useStyles = makeStyles((theme) => ({
  root: {
    '&:nth-child(1)': {
      marginTop: 0,
    },
    marginTop: 40,
    minWidth: '100%',
  },
  noSpace: {
    marginTop: 0,
  },
  divider: {
    backgroundColor: theme.palette.primary.main,
    height: '1px',
  },
  dividerNo: {
    height: '0',
  },
  rowEnd: {
    '&:last-child .MuiTableCell-root': {
      border: 0,
    },
  },
}));

export default function TableCustom({
  title = '',
  subtitle = '',
  isMultiple = false,
  rowPattern,
  data = [],
  noSpace = false,
}) {
  const classes = useStyles();
  return (
    <div className={clsx(classes.root, { [classes.noSpace]: noSpace })}>
      {title && (
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell style={{ border: 0, paddingBottom: '8px' }}>
                <Typography component="p" variant="subtitle2">
                  {title}
                </Typography>
              </TableCell>
            </TableRow>
          </TableHead>
        </Table>
      )}
      <Divider classes={{ root: title ? classes.divider : classes.dividerNo }} />

      {data.length
        ? data.map((data, index) => (
            <Table aria-label="simple table" key={`${subtitle}-${index}`}>
              {isMultiple && (
                <TableHead>
                  <TableRow>
                    <TableCell>
                      <b>{`${index + 1}.${subtitle}`}</b>
                    </TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                </TableHead>
              )}
              <TableBody>
                {rowPattern.map(
                  (pattern, idx) =>
                    !pattern.hide && data[pattern.key] && (
                      <TableRow key={idx} classes={{ root: classes.rowEnd }}>
                        <TableCell component="th" scope="row">
                          {pattern.name}
                          {pattern.hide}
                        </TableCell>
                        <TableCell align="right">
                          {typeof data[pattern.key] !== 'object' ? (
                            data[pattern.key] || null
                          ) : data[pattern.key] ? (
                            <DocumentLink file={data[pattern.key]} />
                          ) : null}
                        </TableCell>
                      </TableRow>
                    )
                )}
              </TableBody>
            </Table>
          ))
        : null}
    </div>
  );
}
