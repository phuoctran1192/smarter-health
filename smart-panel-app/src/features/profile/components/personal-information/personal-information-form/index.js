import React, { useState } from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { Grid, makeStyles, Typography } from '@material-ui/core';
import Textfield from 'components/FormsUI/Textfield';
import Button from 'components/FormsUI/Button';
import CustomSelect from 'components/FormsUI/Select';
import PhoneInput from 'components/FormsUI/PhoneInput';
import CustomizedRadios from 'components/FormsUI/RadioButton';
import SimpleLabel from 'components/FormsUI/Label';
import { TooltipWrapper } from 'components/Elements/Tooltip';
import { FormItem } from 'components/Forms/FormItem';
import DoctorService from 'services/doctor.service';
import Utils from 'utils';
import { DatePicker } from 'components/Inputs/DatePicker';
import moment from 'moment';
import DateSchemaBuilder from 'utils/formik/DateSchemaBuilder';

const personalInforValidationSchema = Yup.object().shape({
  firstName: Yup.string().required('This field is required'),
  lastName: Yup.string().required('This field is required'),
  gender: Yup.string().required('This field is required'),
  dob: new DateSchemaBuilder()
    .withRequired()
    .withMaxDate(
      moment().subtract(24, 'years').endOf('year'),
      'The minimum age to be a doctor is 24 years old',
    )
    .build(),
  identityType: Yup.string().required('This field is required'),
  otherIdentity: Yup.string()
    .nullable()
    .when('identityType', {
      is: '4',
      then: Yup.string().nullable().required('This field is required'),
    }),
  identityNumber: Yup.string().required('This field is required'),
  phoneNumber: Yup.string().required('This field is required'),
  email: Yup.string().email('Please choose a valid email').required('This field is required'),
  address: Yup.object().shape({
    block: Yup.string().required('This field is required'),
    street: Yup.string().required('This field is required'),
    building: Yup.string(),
    level: Yup.string(),
    postalCode: Yup.string().required('This field is required'),
  }),
});
const personalInforInitialValues = {
  email: '',
  firstName: '',
  lastName: '',
  gender: '',
  dob: '',
  identityType: '',
  otherIdentity: '',
  identityNumber: '',
  phoneNumber: '',
  address: {
    block: '',
    street: '',
    building: '',
    level: '',
    number: '',
    postalCode: '',
    city: '',
    state: '',
    country: '',
  },
};
const useStyles = makeStyles({
  formTitle: {
    lineHeight: '33px',
    borderBottom: '1px solid #4126CF',
    marginBottom: 20,
  },
  topSpace: {
    marginTop: 20,
  },
  botSpace: {
    marginBottom: 18,
  },
  button: {
    height: '60px',
    width: '100%',
    backgroundColor: 'black',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
    lineHeight: '24px',
    cursor: 'pointer',
    borderRadius: '5px',
    border: 'none',
  },
  buttonCancel: {
    height: '60px',
    width: '100%',
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'black',
    lineHeight: '24px',
    cursor: 'pointer',
    borderRadius: '5px',
    border: '1px solid black',
  },
  submitErrBox: {
    borderRadius: '12px',
    padding: '20px 20px',
    border: '1px solid red',
    position: 'relative',
    color: 'red',
  },
});
const PersonalInformationForm = ({
  doctorPersonalData,
  identityList,
  onCancelEditPersonal,
  onChangeSuccess,
}) => {
  const classes = useStyles();
  const identityOptions = identityList.reduce((acc, cur) => ({ ...acc, [cur.id]: cur.name }), {});
  const genderOptions = {
    1: 'Male',
    2: 'Female',
  };
  const partData = Utils.getPartObject(
    doctorPersonalData,
    'id',
    'firstName',
    'lastName',
    'phoneNumber',
    'email',
    'gender',
    'dob',
    'identityType',
    'otherIdentity',
    'identityNumber',
    'address',
  );
  const cancelEditPersonal = () => {
    onCancelEditPersonal();
  };

  const formSubmit = async (values) => {
    DoctorService.updateDoctorProfile(values)
      .then((res) => {
        if (res?.code && res?.data) {
          onChangeSuccess();
        }
      })
      .catch((err) => {
        var msg = 'Failed to submit form. Please check again';
        if (err.response && err.response.data) {
          msg += err.response.data.message;
        }
        console.log(msg);
      });
  };
  return (
    <Formik
      initialValues={partData || personalInforInitialValues}
      validationSchema={personalInforValidationSchema}
      onSubmit={formSubmit}
      validator={() => ({})}
    >
      {(formik) => (
        <Form>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography component="p" variant="subtitle2" className={classes.formTitle}>
                Personal Information
              </Typography>
            </Grid>
            <Grid item xs={12} md={6} className={classes.botSpace}>
              <Textfield name="firstName" label="First Name" />
            </Grid>
            <Grid item xs={12} md={6} className={classes.botSpace}>
              <Textfield name="lastName" label="Last Name" />
            </Grid>
            <Grid item xs={12} className={classes.botSpace}>
              <Grid container justifyContent="space-between">
                <SimpleLabel htmlFor="gender">Gender</SimpleLabel>
                <TooltipWrapper title="Medical conditions are very personal in nature and highly influenced by sociodemographic factors e.g. religion. Some patients may have specific preferences.">
                  Why are we asking?
                </TooltipWrapper>
              </Grid>
              <CustomizedRadios name="gender" options={genderOptions} />
            </Grid>
            <Grid item xs={12} className={classes.botSpace}>
              <DatePicker
                name="dob"
                label="Date of Birth*"
                maxDate={moment().subtract(24, 'years').endOf('year')}
              />
            </Grid>
            <Grid item xs={12} md={6} className={classes.botSpace}>
              <CustomSelect
                name="identityType"
                options={identityOptions}
                label="Identification Document (Type)*"
              />
            </Grid>
            <Grid item xs={12} md={6} className={classes.botSpace}>
              {formik.values.identityType.toString() === '4' && (
                <Textfield name="otherIdentity" label="Please specify" />
              )}
            </Grid>
            <Grid item xs={12} md={6} className={classes.botSpace}>
              <Textfield name="identityNumber" label="Identification Document No.*" />
            </Grid>
            <Grid item xs={12} className={classes.botSpace}>
              <Typography
                component="p"
                variant="subtitle2"
                className={`${classes.formTitle} ${classes.topSpace}`}
              >
                Contact Information
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.botSpace}>
              <FormItem name="phoneNumber" label="Mobile No.*">
                <PhoneInput />
              </FormItem>
            </Grid>
            <Grid item xs={12} className={classes.botSpace}>
              <Textfield name="email" label="Email" />
            </Grid>
            <Grid item xs={6} md={3} className={classes.botSpace}>
              <Textfield name="address.block" label="House  / Block No.*" />
            </Grid>
            <Grid item xs={6} md={3} className={classes.botSpace}>
              <Textfield name="address.level" label="Level" />
            </Grid>
            <Grid item xs={12} md={6} className={classes.botSpace}>
              <Textfield name="address.postalCode" label="Postal Code*" />
            </Grid>
            <Grid item xs={12} className={classes.botSpace}>
              <Textfield name="address.street" label="Address 1 (Street Name)*" />
            </Grid>
            <Grid item xs={12} className={classes.botSpace}>
              <Textfield name="address.building" label="Address 2 (Building Name)" />
            </Grid>
            <Grid item xs={12}>
              <Grid container spacing={2} className={classes.topSpace}>
                <Grid item xs={12} md={6}>
                  <button
                    className={classes.buttonCancel}
                    onClick={cancelEditPersonal}
                    type="button"
                  >
                    Cancel
                  </button>
                </Grid>
                <Grid item xs={12} md={6}>
                  <button className={classes.button} type="submit">
                    Save
                  </button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default PersonalInformationForm;
