import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Utils from 'utils';
import { Typography } from '@material-ui/core';
import MetaService from '@services/meta.service';

const useStyles = makeStyles({
  table: {
    minWidth: '100%',
    tableLayout: 'fixed',
    borderCollapse: 'collapse',
    '&:nth-child(2)': {
      marginTop: 40,
    },
    '& td': {
      overflowWrap: 'break-word',
      wordWrap: 'break-word',
      WebkitHyphens: 'auto',
      hyphens: 'auto',
      verticalAlign: 'top',
    },
  },
});

function createData(name, value) {
  return { name, value };
}

export default function PersonalInformationTable({ doctorData, identityList }) {
  const [idList, setIdList] = useState(identityList);
  useEffect(() => {
    if (!identityList || identityList.length === 0) {
      MetaService.getMeta('identity-types')
        .then((res) => {
          if (res?.code && res?.data) {
            setIdList(res.data);
          }
        })
        .catch((err) => console.log(err));
    }
  }, []);
  const classes = useStyles();
  const idType = idList.reduce((acc, cur) => ({ ...acc, [cur.id]: cur.name }), {});
  const genderType = {
    1: 'Male',
    2: 'Female',
  };
  const personalData = Utils.getPartObject(
    doctorData,
    'firstName',
    'lastName',
    'gender',
    'dob',
    'identityType',
    'otherIdentity',
    'identityNumber',
  );
  const contactData = { ...doctorData.address };
  contactData.phoneNumber = doctorData.phoneNumber;
  contactData.email = doctorData.email;
  const rows = [
    createData('First Name', personalData.firstName),
    createData('Last Name', personalData.lastName),
    createData('Gender', genderType[personalData.gender]),
    createData('Date of Birth', personalData.dob),
    createData('Identification Document (Type)', idType[personalData.identityType]),
    createData('', personalData.otherIdentity ? personalData.otherIdentity : ''),
    createData('Identification Document No.', personalData.identityNumber),
  ];

  const contactRows = [
    createData('Mobile No.', contactData.phoneNumber),
    createData('E-mail', contactData.email),
    createData('House/Block No.', contactData.block),
    createData('Address 1 (Street Name)', contactData.street),
    createData('Address 2 (Building Name)', contactData.building),
    createData('Level', contactData.level),
    createData('Number', contactData.number),
    createData('Postal Code', contactData.postalCode),
  ];

  return (
    <TableContainer component={Paper} style={{ boxShadow: 'none' }}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>
              <Typography component="p" variant="subtitle2">
                Personal Information
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => {
            if (row.name || row.value) {
              return (
                <TableRow key={row.name}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="right">{row.value}</TableCell>
                </TableRow>
              );
            } else {
              return null;
            }
          })}
        </TableBody>
      </Table>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>
              <Typography component="p" variant="subtitle2">
                Contact Information
              </Typography>
            </TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {contactRows.map((row) => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.value}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
