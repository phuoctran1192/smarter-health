import React from 'react';
import * as Yup from 'yup';
import CustomTable from '../../../table';

export const validationSchema = Yup.object().shape({
  addableGroup: Yup.array()
    .of(
      Yup.object().shape({
        language: Yup.string().required('This field is required'),
        fluency: Yup.string().required('This field is required'),
      }),
    )
    .required('This field is required'),
});

const LANGUAGE = [
  {
    name: 'Language',
    key: 'language',
  },
  {
    name: 'Fluency',
    key: 'fluency',
  },
];

export const LanguageTable = ({ doctorData, proficiency }) => {

  return (
    <CustomTable
      title="Language"
      subtitle="Language"
      isMultiple
      rowPattern={LANGUAGE}
      data={proficiency}
    />
  );
};
export default LanguageTable;
