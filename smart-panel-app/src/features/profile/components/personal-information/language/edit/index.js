import React, { useState, useEffect } from 'react';
import { Form, Formik, Field, useFormikContext } from 'formik';
import { makeStyles } from '@material-ui/core';
import * as Yup from 'yup';
import LanguageItem from '../LanguageItem';
import DoctorService from '@services/doctor.service';
import { Grid } from '@material-ui/core';
import { LoadingButton } from '@components/Buttons/LoadingButton';
import { SmartSpinner } from '../../../../../../components/Elements';
import ErrorIcon from '@assets/icons/Error.svg';
export const validationSchema = Yup.object().shape({
  addableGroup: Yup.array()
    .of(
      Yup.object().shape({
        language: Yup.string().required('This field is required'),
        fluency: Yup.string().required('This field is required'),
      }),
    )
    .required('This field is required'),
});
const useStyles = makeStyles((theme) => ({
  cancel: {
    height: '60px',
    width: '100%',
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'black',
    lineHeight: '24px',
    cursor: 'pointer',
    borderRadius: '5px',
    border: '1px solid black',
  },
  warning: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    border: '1px solid #FF0000',
    borderRadius: '77px',
    padding: '13px 0',
    width: '85%',
    margin: 'auto',
    marginBottom: '40px',
  },
  wrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  warningText: {
    marginLeft: '10px',
    color: '#FF0000',
  },
}));

export const LanguageEdit = ({
  proficiency,
  srcLanguage,
  srcFluency,
  onEditLanguage,
  reloadDoctorProficiency,
}) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [value, setValue] = useState(proficiency);
  const formModel = {
    language: '',
    fluency: '',
  };
  const initialValues = {
    addableGroup: proficiency,
  };

  const submit = (values) => {
    setLoading(true);
    const data = {
      data: values.addableGroup
        ? values.addableGroup?.map((item) => ({
            language: item.language,
            proficiency: item.fluency,
          }))
        : [],
    };

    DoctorService.updateDoctorProficiency(data)
      .then((res) => {
        onEditLanguage();
        setLoading(false);
        reloadDoctorProficiency();
      })
      .catch((err) => console.log(err))
      .finally(() => {
        setLoading(false);
      });
  };
  const onCancel = () => {
    onEditLanguage();
  };

  return initialValues ? (
    <>
      <div className={classes.warning}>
        <div className={classes.wrapper}>
          <div className={classes.icon}>
            <img src={ErrorIcon} />
          </div>
          <div className={classes.warningText}>
            Please complete this section in order to upgrade your profile status to FULL
          </div>
        </div>
      </div>
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={submit}>
        <Form>
          <Grid item xs={12}>
            <Field
              name={'addableGroup'}
              component={LanguageItem}
              formModel={formModel}
              srcLanguage={srcLanguage}
              srcFluency={srcFluency}
              title="Language Proficiency"
              defaultValue={proficiency}
              setValue={setValue}
            />
          </Grid>
          {value.length > 1 && (
            <Grid container xs={12} style={{ margin: 0, padding: 0, marginTop: 40 }} spacing={4}>
              <Grid item xs={6} lg={6} style={{ paddingLeft: 0 }}>
                <LoadingButton
                  type="button"
                  text="Cancel"
                  onClick={onCancel}
                  className={classes.cancel}
                />
              </Grid>
              <Grid item xs={6} lg={6} style={{ paddingRight: 0 }}>
                <LoadingButton type="submit" loading={loading} text="Save" />
              </Grid>
            </Grid>
          )}
          {value.length === 1 && (
            <Grid item xs={12} style={{ marginTop: 40 }}>
              <LoadingButton type="submit" loading={loading} text="Save" />
            </Grid>
          )}
        </Form>
      </Formik>
    </>
  ) : (
    <SmartSpinner loading={true} />
  );
};
export default LanguageEdit;
