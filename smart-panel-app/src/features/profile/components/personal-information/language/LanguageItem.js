import { Grid } from '@material-ui/core';
import React from 'react';
import { FormItem } from '@components/Forms';
import { SPSelect, withAddable } from '@components/Inputs';
import { makeStyles } from '@material-ui/core/styles';
import { CollectionsOutlined } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '0',
  },
}));

const LanguageItem = ({ value, index, srcLanguage, srcFluency, ...props }) => {
  const classes = useStyles();
  return (
    <Grid container spacing={4} className={classes.root}>
      <Grid item xs={12} md={6}>
        <FormItem label="Language" required name={`addableGroup[${index}].language`}>
          <SPSelect dataSource={srcLanguage} />
        </FormItem>
      </Grid>
      <Grid item xs={12} md={6}>
        <FormItem label="Fluency" required name={`addableGroup[${index}].fluency`}>
          <SPSelect dataSource={srcFluency} />
        </FormItem>
      </Grid>
    </Grid>
  );
};

export default withAddable(LanguageItem);
