import React from 'react';
import { FieldArray, useField, useFormikContext } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import SPMultiCheckbox from 'components/FormsUI/MultiCheckbox';
import { WEEKDAY } from 'utils/constants';
import { Grid } from '@material-ui/core';
import ConsultationScheduleSession from './ConsultationScheduleSession';

const useStyles = makeStyles({
  topSpace: {
    marginTop: 40,
    display: 'block',
    width: '100%',
  },
});
const ConsultationSchedule = ({ index, name }) => {
  const { setFieldValue } = useFormikContext();
  const [field] = useField(name);
  const selectedDay = field.value.reduce((acc, cur) => [...acc, cur.weekday], []);
  const classes = useStyles();

  const onDateChange = ({ item, change }) => {
    let newValue = field.value;
    if (change === 'ADD') {
      const csItem = { weekday: item, sessions: [] };
      newValue = [...newValue, csItem];
    } else {
      newValue = newValue.filter((d) => d.weekday !== item);
    }
    setFieldValue(name, newValue);
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <SPMultiCheckbox onDateChange={onDateChange} options={WEEKDAY} initValue={selectedDay} />
      </Grid>
      <div className={classes.topSpace}></div>
      <Grid item xs={12}>
        <Grid container spacing={2}>
          <FieldArray
            name={`practicingPlaces[${index}].consultationSchedules`}
            render={({ push, remove, form }) => {
              const items = form?.values?.practicingPlaces[index]?.consultationSchedules || [];
              return (
                <>
                  {items.map((i, idx) => (
                    <Grid item xs={12} md={6} key={idx}>
                      <ConsultationScheduleSession
                        date={i.weekday}
                        pindex={index}
                        cindex={idx}
                        name={`practicingPlaces[${index}].consultationSchedules[${idx}]`}
                      />
                    </Grid>
                  ))}
                </>
              );
            }}
          />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default ConsultationSchedule;
