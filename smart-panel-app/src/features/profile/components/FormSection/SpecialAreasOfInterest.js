import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import MultipleSelect from 'components/Inputs/MultipleSelect';
import { useSelector } from 'react-redux';
import { useFormSectionStyles } from './common';
import ReadOnlyMultipleSelect from 'components/Inputs/ReadOnlyMultipleSelect';

const DoctorSpecialAreasOfInterest = () => {
  const classes = useFormSectionStyles();

  const specialities = useSelector((state) => state.doctorProfile.meta.specialities) || [];
  const areasOfInterest = useSelector((state) => state.doctorProfile.meta.areasOfInterest) || [];

  const doctor = useSelector((state) => state.doctorProfile.doctor);

  const specialityIds = specialities.map((s) => s.id);

  // Remove Speciality options that are disabled (which is not returned when fetch metadata)
  let doctorSpecialities = [];
  if (Array.isArray(doctor?.practicingCertificate?.specialities)) {
    doctorSpecialities = doctor.practicingCertificate.specialities
      .filter((m2m) => m2m.speciality != null && specialityIds.includes(m2m.speciality))
      .map((m2m) => m2m.speciality);
  }

  // Remove Area of Interest options that not related to doctor's specialities
  const availableAreasOfInterest = areasOfInterest.filter(
    (aoi) => aoi.speciality && doctorSpecialities.includes(aoi.speciality),
  );
  let areasOfInterestBySpec = {};
  doctorSpecialities.map((ds) => {
    areasOfInterestBySpec[ds] = availableAreasOfInterest.filter((aaoi) => aaoi.speciality === ds);
  });

  return (
    <>
      <Grid container spacing={2} className={classes.formSection}>
        <Grid item xs={12}>
          <Typography component="p" variant="subtitle1" className={classes.formTitle}>
            Special Area(s) of Interest
          </Typography>
        </Grid>
        {doctorSpecialities.map((selectedSpec, idx) => (
          <React.Fragment key={idx}>
            <Grid item xs={12} md={12}>
              <ReadOnlyMultipleSelect
                id="specialities"
                options={specialities}
                label="Specialty/Sub-Specialty*"
                value={[selectedSpec]}
              />
            </Grid>
            <Grid item xs={12} md={12}>
              {areasOfInterestBySpec[selectedSpec].length > 0 ? (
                <MultipleSelect
                  id="areasOfInterest"
                  name={`areasOfInterest`}
                  options={areasOfInterestBySpec[selectedSpec]}
                  label="Area of Interest*"
                />
              ) : (
                <ReadOnlyMultipleSelect label="Area of Interest" options={[]} />
              )}
            </Grid>
          </React.Fragment>
        ))}
      </Grid>
    </>
  );
};

export default DoctorSpecialAreasOfInterest;
