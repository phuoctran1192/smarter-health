import { Grid, Typography } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useFormSectionStyles } from './common';
import { FieldArray, useFormikContext } from 'formik';
import { FormItem } from 'components/Forms/FormItem';
import { SPTextField } from 'components/Inputs/TextField';
import PhoneInput from 'components/FormsUI/PhoneInput';
import { getEmtpyPlaceOfPractice } from 'features/profile/validation/placeOfPractice';
import ConsultationSchedule from './ConsultationSchedule';
import SimpleLabel from 'components/FormsUI/Label';

const PlaceOfPracticeFormSection = () => {
  const classes = useFormSectionStyles();
  const {values, setFieldValue} = useFormikContext();
  useEffect(() => {
    if(values.practicingPlaces.length === 0) {
      setFieldValue('practicingPlaces', [getEmtpyPlaceOfPractice()])
    }
  }, [values.practicingPlaces]);
  return (
    <Grid container spacing={2} className={classes.formSection}>
      <Grid item xs={12}>
        <FieldArray
          name="practicingPlaces"
          render={({ push, remove, form }) => {
            const items = form?.values?.practicingPlaces || [];
            return (
              <>
                <Grid item xs={12} className={classes.formTitle}>
                  <div className="flex-center-between">
                    <Typography component="p" variant="subtitle1">
                      Place of Practice / Department
                    </Typography>
                    <button
                      type="button"
                      className="add-group"
                      onClick={() => push(getEmtpyPlaceOfPractice())}
                    >
                      <span className="plus-icon">plus</span>
                      Add Place of Practice
                    </button>
                  </div>
                </Grid>
                <div style={{ marginBottom: '50px' }}></div>
                {items.length === 0 && (
                  <Grid item xs={12} lg={12} className={classes.formText}>
                    <Typography className={classes.formText}>
                      You have no Place of Practice
                    </Typography>
                  </Grid>
                )}
                {items.map((_, index) => (
                  <Grid container key={index} spacing={2} className={classes.fieldListItem}>
                    <Grid item xs={6}>
                      <p className="subtitle3">{index + 1}. Place of Practice</p>
                    </Grid>
                    {
                      values.practicingPlaces.length > 1 && (
                      <Grid item xs={6}>
                        <button
                          type="button"
                          className="addable-section-remove"
                          onClick={() => remove(index)}
                        >
                          Delete
                        </button>
                      </Grid>
                      )
                    }
                    <Grid item xs={12}>
                      <FormItem
                        required
                        label="Name of Practice/ Department"
                        name={`practicingPlaces[${index}].name`}
                      >
                        <SPTextField value={values.practicingPlaces[index].name} />
                      </FormItem>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <FormItem
                        required
                        label="Clinic License No"
                        name={`practicingPlaces[${index}].licenseNumber`}
                      >
                        <SPTextField value={values.practicingPlaces[index].licenseNumber} />
                      </FormItem>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <FormItem
                        required
                        label="Contact No."
                        name={`practicingPlaces[${index}].contactNumber`}
                      >
                        <PhoneInput />
                      </FormItem>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <FormItem label="Email" required name={`practicingPlaces[${index}].email`}>
                        <SPTextField value={values.practicingPlaces[index].email}/>
                      </FormItem>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <FormItem label="Website/URL" name={`practicingPlaces[${index}].website`}>
                        <SPTextField value={values.practicingPlaces[index].website}/>
                      </FormItem>
                    </Grid>
                    <Grid item xs={12}>
                      <FormItem
                        label="Address 1 (Street Name)"
                        required
                        name={`practicingPlaces[${index}].address.street`}
                      >
                        <SPTextField value={values.practicingPlaces[index].address.street}/>
                      </FormItem>
                    </Grid>
                    <Grid item xs={12}>
                      <FormItem
                        label="Address 2 (Building Name)"
                        required
                        name={`practicingPlaces[${index}].address.building`}
                      >
                        <SPTextField value={values.practicingPlaces[index].address.building}/>
                      </FormItem>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <FormItem
                        label="House/Block No"
                        required
                        name={`practicingPlaces[${index}].address.block`}
                      >
                        <SPTextField value={values.practicingPlaces[index].address.block}/>
                      </FormItem>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <FormItem
                        label="Postal Code"
                        required
                        name={`practicingPlaces[${index}].address.postalCode`}
                      >
                        <SPTextField value={values.practicingPlaces[index].address.postalCode}/>
                      </FormItem>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <FormItem label="Level" name={`practicingPlaces[${index}].address.level`}>
                        <SPTextField value={values.practicingPlaces[index].address.level}/>
                      </FormItem>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <FormItem label="Number" name={`practicingPlaces[${index}].address.number`}>
                        <SPTextField value={values.practicingPlaces[index].address.number}/>
                      </FormItem>
                    </Grid>
                    <Grid item xs={12}>
                      <SimpleLabel>
                        Consultation Schedule (You can choose more than one)*
                      </SimpleLabel>
                      <ConsultationSchedule index={index} name={`practicingPlaces[${index}].consultationSchedules`} />
                    </Grid>
                  </Grid>
                ))}
              </>
            );
          }}
        />
      </Grid>
    </Grid>
  );
};

export default PlaceOfPracticeFormSection;
