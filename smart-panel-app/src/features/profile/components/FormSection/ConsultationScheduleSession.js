import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import Textfield from 'components/FormsUI/Textfield';
import { FieldArray, useFormikContext } from 'formik';
import { useFormSectionStyles } from './common';
import { WEEKDAY } from 'utils/constants';
import clsx from 'clsx';

const ConsultationScheduleSession = ({ date, pindex, cindex, name }) => {
  const day = WEEKDAY.filter((d) => d.value === date);
  const {values} = useFormikContext();
  const classes = useFormSectionStyles();

  return (
    <FieldArray
      name={`practicingPlaces[${pindex}].consultationSchedules[${cindex}].sessions`}
      render={({ push, remove, form }) => {
        const items =
          form?.values?.practicingPlaces[pindex]?.consultationSchedules[cindex].sessions || [];
        const addSession = () => {
          if (items.length < 3) {
            push({ startTime: '', endTime: '' });
          }
        };
        return (
          <>
            <Grid item xs={12}>
              <div className="flex-center-between">
                <Typography component="p" variant="subtitle1">
                  Every {day[0]?.label}
                </Typography>
                <button
                  type="button"
                  className={clsx('add-group', items.length >= 3 ? 'disable' : '')}
                  onClick={addSession}
                >
                  + Session
                </button>
              </div>
            </Grid>
            {items.map((_, sindex) => (
              <Grid
                container
                key={`session_${sindex}`}
                spacing={2}
                className={classes.fieldListItem}
              >
                <Grid item xs={12} className={classes.noPaddingBottom}>
                  <div className="flex-center-between">
                  <p className={clsx('subtitle2', 'noMargin')}>
                      {
                        {
                          0: 'First ',
                          1: 'Second ',
                          2: 'Third ',
                        }[sindex]
                      }
                      Session
                    </p>
                    <button
                      type="button"
                      className="addable-section-remove"
                      onClick={() => remove(sindex)}
                    >
                      Delete
                    </button>
                  </div>
                </Grid>
                <Grid item xs={12} md={6} className={classes.noPaddingTop}>
                  <Textfield
                    name={`practicingPlaces[${pindex}].consultationSchedules[${cindex}].sessions[${sindex}].startTime`}
                    type="time"
                    InputLabelProps={{
                      shrink: false,
                    }}
                    className="time-picker"
                    value={values.practicingPlaces[pindex].consultationSchedules[cindex].sessions[sindex].startTime}
                    />
                </Grid>
                <Grid item xs={12} md={6} className={classes.noPaddingTop}>
                  <Textfield
                    name={`practicingPlaces[${pindex}].consultationSchedules[${cindex}].sessions[${sindex}].endTime`}
                    type="time"
                    InputLabelProps={{
                      shrink: false,
                    }}
                    className="time-picker"
                    value={values.practicingPlaces[pindex].consultationSchedules[cindex].sessions[sindex].endTime}
                  />
                </Grid>
              </Grid>
            ))}
          </>
        );
      }}
    />
  );
};

export default ConsultationScheduleSession;
