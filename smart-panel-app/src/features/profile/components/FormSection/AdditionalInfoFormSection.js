import React from 'react';
import { Grid } from '@material-ui/core';
import MultipleSelect from 'components/Inputs/MultipleSelect';
import { useFormSectionStyles } from './common';
import Textfield from 'components/FormsUI/Textfield';
import { useSelector } from 'react-redux';

const AdditionalInfoFormSection = () => {
    const classes = useFormSectionStyles();

    const companies = useSelector(state => state.doctorProfile.meta.insuranceCompanies) || [];

    return (
        <Grid container spacing={2} className={classes.formSection}>
            <Grid item xs={12} md={12}>
                <MultipleSelect
                    id="hospitals"
                    name={`panelDoctorFor`}
                    options={companies}
                    label="I'm a panel doctor for"
                />
            </Grid>
            <Grid item xs={12} md={12}>
                <Textfield name={`clinicManagementSystem`} label="Which clinic management system are you currently using?*" />
            </Grid>
        </Grid>
    )
}

export default AdditionalInfoFormSection;
