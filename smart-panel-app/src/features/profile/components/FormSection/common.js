import { makeStyles } from '@material-ui/styles';

export const useFormSectionStyles = makeStyles((theme) => ({
    formSection: {
        marginBottom: 50,
        marginTop: 50,
    },
    formTitle: {
        lineHeight: '33px',
        borderBottom: '1px solid ' + theme.palette.primary.main,
    },
    formText: {
        padding: '8px',
        lineHeight: '33px',
        fontSize: '16px',
        justifyContent: 'center',
    },
    fieldListItem: {
        marginTop: 10,
        marginBottom: 10,
    },
    noPaddingBottom: {
        paddingBottom: '0 !important'
    },
    noPaddingTop: {
        paddingTop: '0 !important'
    }
}))
