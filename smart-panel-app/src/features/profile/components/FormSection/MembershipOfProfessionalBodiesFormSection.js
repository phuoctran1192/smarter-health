import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { useFormSectionStyles } from './common';
import CustomizedRadios from 'components/FormsUI/RadioButton';
import SimpleLabel from 'components/FormsUI/Label';
import { useSelector } from 'react-redux';

const MembershipOfProfessionalBodiesFormSection = () => {
    const classes = useFormSectionStyles();

    const doctor = useSelector(state => state.doctorProfile.doctor);

    const isSpecialist = doctor?.practicingCertificate?.isSpecialist;

    return (
        <Grid container spacing={2} className={classes.formSection}>
            <Grid item xs={12}>
                <Typography component="p" variant="subtitle1" className={classes.formTitle}>
                    Membership of Professional Bodies
                </Typography>
            </Grid>
            <Grid item xs={12} md={12}>
                <SimpleLabel>Are you a member of Singapore Medical Association (SMA)?*</SimpleLabel>
                <CustomizedRadios
                    name="isSMAMember"
                    options={{
                        true: 'Yes',
                        false: 'No',
                    }}
                />
            </Grid>
            {isSpecialist && <Grid item xs={12} md={12}>
                <SimpleLabel>Are you a member of Academy of Medicine, Singapore (AMS)?*</SimpleLabel>
                <CustomizedRadios
                    name="isAMSMember"
                    options={{
                        true: 'Yes',
                        false: 'No',
                    }}
                />
            </Grid>
            }
            {!isSpecialist &&
                <Grid item xs={12} md={12}>
                    <SimpleLabel>Are you a member of College of Family Physicians Singapore (CFPS)?*</SimpleLabel>
                    <CustomizedRadios
                        name="isCFPSMember"
                        options={{
                            true: 'Yes',
                            false: 'No',
                        }}
                    />
                </Grid>
            }
        </Grid>
    )
}

export default MembershipOfProfessionalBodiesFormSection;
