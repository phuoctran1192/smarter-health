import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import Textfield from 'components/FormsUI/Textfield';
import { useFormSectionStyles } from './common';

const DoctorBackgroundFormSection = () => {
    const classes = useFormSectionStyles();

    return (
        <Grid container spacing={2} className={classes.formSection}>
            <Grid item xs={12}>
                <Typography component="p" variant="subtitle1" className={classes.formTitle}>
                    Work Background
                </Typography>
            </Grid>
            <Grid item xs={12} md={12}>
                <Textfield name={`background`} label="Background*" multiline={true} minRows={4} />
            </Grid>
        </Grid>
    )
}

export default DoctorBackgroundFormSection;
