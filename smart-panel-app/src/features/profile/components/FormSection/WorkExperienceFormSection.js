import React, { useEffect } from 'react';
import { Grid, Typography } from '@material-ui/core';
import Textfield from 'components/FormsUI/Textfield';
import { FieldArray, useFormikContext } from 'formik';
import { getEmtpyWorkExperience } from 'features/profile/validation/workExperience';
import { useFormSectionStyles } from './common';
import CheckboxWrapper from 'components/FormsUI/Checkbox';
import { YearPicker } from 'components/Inputs/YearPicker';

const DoctorWorkExperienceFormSection = () => {
    const classes = useFormSectionStyles();

    const { values, setFieldValue } = useFormikContext();
    const isCurrentChange = ({evt, index}) => {
        if(evt.target.checked) {
            setFieldValue(`workExperiences.[${index}].end`, "")
        }
    }
    useEffect(() => {
        if(values.workExperiences.length === 0) {
          setFieldValue('workExperiences', [getEmtpyWorkExperience()])
        }
    }, [values.workExperiences]);
    return (
        <Grid container spacing={2} className={classes.formSection}>
            <Grid item xs={12}>
                <FieldArray
                    name="workExperiences"
                    render={({ push, remove, form }) => {
                        const items = form?.values?.workExperiences || [];
                        return (<>
                            <Grid item xs={12} className={classes.formTitle}>
                                <div className="flex-center-between">
                                    <Typography component="p" variant="subtitle1">
                                        Work Experiences
                                    </Typography>
                                    <button type="button" className="add-group" onClick={() => push(getEmtpyWorkExperience())}>
                                        <span className="plus-icon">plus</span>
                                        Add Work Experience
                                    </button>
                                </div>
                            </Grid>
                            {
                                items.length === 0 &&
                                <Grid item xs={12} lg={12} className={classes.formText} >
                                    <Typography className={classes.formText}>
                                        You have no work experiences
                                    </Typography>
                                </Grid>
                            }
                            {items.map((_, index) =>
                                <Grid container key={index} spacing={2} className={classes.fieldListItem}>
                                    <Grid item xs={6}>
                                        <p className="subtitle3">{index + 1}. Work Experience</p>
                                    </Grid>
                                    {
                                        values.workExperiences.length > 1 && (
                                            <Grid item xs={6}>
                                                <button
                                                    type="button"
                                                    className="addable-section-remove"
                                                    onClick={() => remove(index)}
                                                >
                                                    Delete
                                                </button>
                                            </Grid>
                                        )
                                    }
                                    <Grid item xs={12} md={12}>
                                        <Textfield name={`workExperiences.[${index}].organization`} label="Organization*" />
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Textfield name={`workExperiences.[${index}].department`} label="Department*" />
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Textfield name={`workExperiences.[${index}].position`} label="Position*" />
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <YearPicker name={`workExperiences.[${index}].start`} label="Start*" />
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        {!values.workExperiences[index].isCurrent &&
                                            <YearPicker name={`workExperiences.[${index}].end`} label="End*" />
                                        }
                                        <div className={classes.fieldCheckBox} style={{ marginTop: (values.workExperiences[index].isCurrent) ? 40 : 0 }}>
                                            <CheckboxWrapper index={index} name={`workExperiences.[${index}].isCurrent`} label="Is current?" onCheckboxChange={isCurrentChange}/>
                                        </div>
                                    </Grid>
                                </Grid>
                            )}
                        </>)
                    }}
                />
            </Grid>
        </Grid>
    )
}

export default DoctorWorkExperienceFormSection;