import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import MultipleSelect from 'components/Inputs/MultipleSelect';
import { useSelector } from 'react-redux';
import { useFormSectionStyles } from './common';

const DoctorAccreditedFacilityFormSection = () => {
    const classes = useFormSectionStyles();

    const ambulatorySurgicalCentres = useSelector(state => state.doctorProfile.meta.ambulatorySurgicalCentres) || [];
    const hospitals = useSelector(state => state.doctorProfile.meta.hospitals) || [];

    return (
        <>
            <Grid container spacing={2} className={classes.formSection}>
                <Grid item xs={12}>
                    <Typography component="p" variant="subtitle1" className={classes.formTitle}>
                        Accredited Facilities
                    </Typography>
                </Grid>
                <Grid item xs={12} md={12}>
                    <MultipleSelect
                        id="hospitals"
                        name={`accreditedHospitals`}
                        options={hospitals}
                        label="Hospitals*"
                    />
                </Grid>
                <Grid item xs={12} md={12}>
                    <MultipleSelect
                        id="ambulatorySurgicalCentres"
                        name={`accreditedSurgicalCentres`}
                        options={ambulatorySurgicalCentres}
                        label="Ambulatory Surgical Centres*"
                    />
                </Grid>
            </Grid>
        </>
    )
}

export default DoctorAccreditedFacilityFormSection;