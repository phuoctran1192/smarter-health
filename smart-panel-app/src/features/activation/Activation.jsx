import { Grid, makeStyles } from '@material-ui/core';
import ActivationImage from '@assets/images/activation.svg';
import { ActivationForm } from './components/ActivationForm';
import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { useAuth } from '../../hooks';
const useStyles = makeStyles((theme) => ({
  login: {
    margin: 'auto',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
  },
  content: {
    width: '80%',
    maxWidth: '900px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  image: {
    width: '400px',
    '& img': {
      width: '100%',
    },
  },
  loginForm: {
    width: '400px',
  },
}));

export const Activation = () => {
  const classes = useStyles();
  const { token } = useParams();
  const { verifyToken } = useAuth();
  const [showActivation, setShowActivation] = useState(false);
  useEffect(() => {
    const payload = {
      token,
    };
    verifyToken(payload)
      .then((res) => {
        setShowActivation(true);
        console.log(res);
      })
      .catch();
  }, []);
  return (
    <div className={classes.login}>
      <Grid container className={classes.content} spacing={6}>
        {showActivation ? (
          <>
            <div className={classes.image}>
              <img src={ActivationImage} />
            </div>
            <div className={classes.loginForm}>
              <ActivationForm token={token} />
            </div>
          </>
        ) : (
          <div>Opps, have something wrong! Please try again</div>
        )}
      </Grid>
    </div>
  );
};
