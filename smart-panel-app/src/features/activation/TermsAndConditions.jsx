import { Box, makeStyles, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';
const useStyles = makeStyles((theme) => ({
  page: {
    paddingTop: 80,
    paddingBottom: 80,
    textAlign: 'center',
    backgroundColor: '#E5E5E5',
    height: '100%',
  },
  box: {
    textAlign: 'justify',
    fontSize: 14,
    lineHeight: '149%',
    backgroundColor: 'white',
    padding: '70px',
    width: '100%',
    maxWidth: 930,
    marginTop: 50,
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 10,
    position: 'relative',
    height: 1500,
    overflow: 'auto',
    '@media (max-width: 992px)': {
      padding: '30px 16px',
    },
  },
  content: {},
  backButton: {
    backgroundColor: 'black',
    color: 'white',
    fontWeight: 'normal',
    fontSize: 18,
    lineHeight: '27px',
    textAlign: 'center',
    paddingTop: 17,
    paddingBottom: 17,
    width: '100%',
    maxWidth: 780,
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 80,
    textDecoration: 'none',
    borderRadius: 5,
  },
}));

export const TermsAndConditions = () => {
  const classes = useStyles();
  return (
    <div className={classes.page}>
      <Typography component="h4" variant="h4">
        Platform Terms of Use
      </Typography>
      <div>Please read these terms and conditions carefully.</div>
      <Box className={classes.box}>
        <div className="term-title">Terms & Conditions</div>
        <div>
          Please read these terms and conditions carefully. The Platform (defined below), which is
          owned by Smarter Health Pte. Ltd. (“Smarter Health” or “we”) is provided to you under the
          following terms and conditions (“Terms of Use”). By accessing and/or using the Platform or
          the Service (defined below), you are indicating your agreement to be bound by these Terms
          of Use. If you are not a Client (defined below), Authorised User (defined below) or SP
          (defined below), or if you do not accept any part of these Terms of Use, you must
          immediately cease all access and use of the Platform and the Service. Part A of these
          Terms of Use applies to all Users (as defined below) and Part B of these Terms of Use
          contains additional terms which apply specifically to Clients. The obligations under these
          Terms of Use which are applicable to the Client, shall to the extent applicable be deemed
          to apply to the Client’s Authorised User(s) as though such Authorised User(s) were the
          Client, and each Authorised User hereby agrees to comply with such obligations under these
          Terms of Use.
        </div>

        <div className="term-block">
          <h5>
            <span>1</span>
            <span>DEFINITIONS</span>
          </h5>
          <div className="term-block-sub">
            <span>1.1</span>
            <span>
              Unless the context otherwise requires, the following expressions shall have the
              following meanings:
            </span>
          </div>
          <div className="term-block-sub-2">
            <span>1.1.1</span>
            <i>“Additional Terms”</i>
            is defined in Clause 4.1.
          </div>
          <div className="term-block-sub-2">
            <span>1.1.2</span>
            “Authorised User” means: (i) in respect of Patients, a parent or guardian of a Patient
            who is a minor, an authorised caregiver of a Patient, or any other authorised user of a
            Patient, as the case may be; and (ii) in respect of any other Client, any authorised
            user of such Client
          </div>
          <div className="term-block-sub-2">
            <span>1.1.3</span>
            <i>“Clients”</i>
            means all Users other than SPs. “Clients” may include Users who are using the Platform
            in the capacity of payors (including insurers, employers, third party administrators or
            any other relevant parties) or healthcare service providers.
          </div>
          <div className="term-block-sub-2">
            <span>1.1.4</span>
            <i>“Content”</i>
            means any and all information, content, materials, services and functions including
            software, code, scripts, programs, data, databases, text, diagrams, graphics, graphical
            user interface, photographs, animations, audio, music, video, links or other materials
            contained in or made available through the Platform and/or Service but excludes
            User-Provided Content.
          </div>
          <div className="term-block-sub-2">
            <span>1.1.5</span>
            <i>“GST”</i>
            means any and all information, content, materials, services and functions including
            software, code, scripts, programs, data, databases, text, diagrams, graphics, graphical
            user interface, photographs, animations, audio, music, video, links or other materials
            contained in or made available through the Platform and/or Service but excludes
            User-Provided Content.
          </div>
          <div className="term-block-sub-2">
            <span>1.1.6</span>
            <i>“Healthcare Service Providers”</i>
            means Users who are using the Platform in the capacity of a healthcare service provider
            (including SPs and hospitals).
          </div>
          <div className="term-block-sub-2">
            <span>1.1.7</span>
            <i>“New Services”</i>
            is defined in Clause 4.2
          </div>
          <div className="term-block-sub-2">
            <span>1.1.8</span>
            <i>“Password”</i>
            means the valid password that you use in conjunction with the Username to access and use
            the Platform.
          </div>
          <div className="term-block-sub-2">
            <span>1.1.9</span>
            <i>“Patients”</i>
            means Users who are using the Platform in the capacity of potential or actual patients
            of one or more Healthcare Service Providers.
          </div>
          <div className="term-block-sub-2">
            <span>1.1.10</span>
            <i>“Platform”</i>
            means the online platform, Smarter Health, accessible at such location as may be
            prescribed by Smarter Health from time to time.
          </div>
          <div className="term-block-sub-2">
            <span>1.1.11</span>
            <i>“Security Credentials”</i>
            means the Username, Password, one-time passwords, and other forms of identification or
            information (if any) issued or prescribed by Smarter Health from time to time for use to
            access the Platform.
          </div>
          <div className="term-block-sub-2">
            <span>1.1.12</span>
            <i>“Service”</i>
            means the services, information, functions and features made available to you by Smarter
            Health through the Platform.
          </div>
          <div className="term-block-sub-2">
            <span>1.1.13</span>
            <i>“SPs”</i>
            means users who are using the Platform in the capacity of specialist doctors.
          </div>
          <div className="term-block-sub-2">
            <span>1.1.14</span>
            <i>“Trademarks”</i>
            is defined in Clause 13.2.
          </div>
          <div className="term-block-sub-2">
            <span>1.1.15</span>
            <i>“User-Provided Content”</i>
            means any information, text, and/or other materials submitted by you for inclusion
            and/or posting through the Platform and/or Service.
          </div>
          <div className="term-block-sub-2">
            <span>1.1.16</span>
            <i>“Users”</i>
            means an end user of the Platform.
          </div>
          <div className="term-block-sub-2">
            <span>1.1.17</span>
            <i>“Usernames”</i>
            means the unique login identification name or code which identifies you.
          </div>
          <div className="term-block-sub">
            <span>1.2</span>
            <span>
              Interpretation: Any reference in these Terms of Use to any provision of a statute
              shall be construed as a reference to that provision as amended, re-enacted or extended
              at the relevant time. In these Terms of Use, whenever the words “include”, “includes”
              or “including” are used, they will be deemed to be followed by the words “without
              limitation”. Clause headings are inserted for convenience only and shall not affect
              the interpretation of these Terms of Use. In the event of a conflict or inconsistency
              between any two or more provisions under these Terms of Use, such conflict or
              inconsistency shall be resolved in favour of Smarter Health and the provision which is
              more favourable to Smarter Health shall prevail.
            </span>
          </div>
          <div className="term-block-sub">
            <i>
              A. The following terms and conditions are applicable to all Users. Under this Part A,{' '}
              <br />
              “you” refers to each User.
            </i>
          </div>
        </div>
        <div className="term-block">
          <h5>
            <span>1</span>
            <span>GENERAL USE AND ACCESS OF THE PLATFORM AND/OR SERVICE</span>
          </h5>
          <div className="term-block-sub">
            <span>2.1</span>
            <span>
              Use of the Platform and/or Service by individuals under the age of minority; The
              Platform and Service are not intended to be used by those under 18 years of age.
              However, parents and guardians can use the Platform and Service on behalf of their
              child or ward (as the case may be) who are under 18 years of age, by creating an
              account for their child or ward, submitting information of their child or ward (as the
              case may be) to the Platform and uploading the relevant medical reports.
            </span>
          </div>
          <div className="term-block-sub">
            <span>2.2</span>
            <span>
              Guidelines: You agree to comply with any and all the guidelines, notices, policies and
              instructions pertaining to the use and/or access of the Platform and/or Service, as
              well as any amendments to the aforementioned, issued by us, from time to time. We
              reserve the right to revise these guidelines, notices, operating rules and policies
              and instructions at any time and you are deemed to be aware of and bound by any
              changes to the foregoing upon their publication on the Platform.
            </span>
          </div>
          <div className="term-block-sub">
            <span>2.3</span>
            <span>You represent, warrant and covenant that:</span>
          </div>
          <div className="term-block-sub-2">
            <span>2.3.1</span>
            you shall abide by all applicable laws and regulations in your access and/or use of the
            Platform and the Service;
          </div>
          <div className="term-block-sub-2">
            <span>2.3.2</span>
            you shall access and/or use the Platform in a manner that does not infringe and shall
            not cause us to infringe any patent, copyright, trademark, trade secret or other
            proprietary rights of any third party;
          </div>
          <div className="term-block-sub-2">
            <span>2.3.3</span>
            you shall not impersonate any person or entity or to falsely state or otherwise
            misrepresent your affiliation with any person or entity;
          </div>
          <div className="term-block-sub-2">
            <span>2.3.4</span>
            you shall not send, distribute or upload, in any way, any software, data or materials
            that contain viruses, malicious code or harmful components that may impair or corrupt
            the Platform’s data, damage or interfere with the operation of the Platform or damage
            the operation of another’s computer or device; and
          </div>
          <div className="term-block-sub-2">
            <span>2.3.5</span>
            you shall not post, promote or transmit through the Platform and/or the Service any
            unlawful, harassing, libellous, harmful, vulgar, obscene or otherwise objectionable
            material of any kind or nature.
          </div>
          <div className="term-block-sub">
            <span>2.4</span>
            <span>
              Registered users only: Access to and use of certain functions or features on the
              Platform is restricted to Clients and SPs, who have registered and created a user
              account on the Platform, and the Clients’ Authorised Users. To register and create a
              user account, you shall follow the relevant instructions on the Platform. In
              particular, you shall fully, accurately and truthfully complete the registration
              profile and provide such other requested or required information. You shall not obtain
              or attempt to obtain unauthorised access to any part of the Platform, or to any other
              protected information, through any means not intentionally made available by Smarter
              Health for your specific use. A breach of this provision may be an offence under the
              Computer Misuse Act (Chapter 50A) of Singapore.
            </span>
          </div>
          <div className="term-block-sub">
            <span>2.5</span>
            <span>
              Client and Authorised User: Client shall not permit or allow any third party who is
              not an Authorised User to access or use the Platform. Client acknowledges that control
              of the Authorised User(s) to access or use the Platform is entirely the Client’s
              responsibility. Client shall at all times be deemed to have adopted all actions and
              purported actions (including all omissions and purported omissions) of its Authorised
              User (where applicable) as well as all purported actions (including purported
              omissions) of the Authorised User as such Client’s own actions (and omissions).
              Smarter Health may rely on the authority of each Authorised User or any person
              (whether authorised or unauthorised by the Client) using the Client’s Security
              Credentials to do any act or transmit any information, instructions and other
              communications on the Client’s behalf. Client agrees that Smarter Health shall be
              entitled (but not obliged) to act upon, rely on or hold Client solely responsible and
              liable in respect thereof as if the same were carried out or transmitted by Client.
              All use and/or access of the Platform by Authorised User(s) or any persons, and all
              instructions issued through the Platform, that are referable to the Client’s Security
              Credentials shall be deemed the Client’s use and/or access, regardless of whether such
              use and/or access or such instructions are authorised or unauthorised by the Client.
              Where applicable, references to the Client’s use and/or access of the Platform shall
              be deemed to include its Authorised User(s)’ use and/or access.
            </span>
          </div>
          <div className="term-block-sub">
            <span>2.6</span>
            <span>
              Security Credentials: Access to and use of the Platform will only be granted through
              the use of Security Credentials in accordance with the latest instructions, notices,
              policies and directions issued by Smarter Health. Where applicable, you shall change
              the Security Credentials from time to time. You hereby agree to keep the Security
              Credentials confidential and secure and to take all necessary measures to prevent the
              use of the Security Credentials by any other persons. You agree that you are liable
              for all consequences arising from any loss, disclosure, use or misuse (whether such
              use is authorised or not) of the Security Credentials. You are to notify us
              immediately if you have knowledge that or have reason for suspecting that the
              confidentiality of the Security Credentials has been compromised or if there has been
              any unauthorised use of the Security Credentials.
            </span>
          </div>
          <div className="term-block-sub">
            <span>2.7</span>
            <span>
              Purported use/access: You agree and acknowledge that any use or purported use of or
              access to or purported access to the Platform referable to the relevant Security
              Credentials shall be deemed to be access to and/or use of the Platform by you, and you
              agree to be bound by any access and/or use of the Platform (whether or not such access
              or use are authorised by you) and you agree that we shall be entitled (but not
              obliged) to act upon, rely on or hold you solely responsible and liable in respect
              thereof as if the same were carried out or transmitted by you.
            </span>
          </div>
          <div className="term-block-sub">
            <span>2.8</span>
            <span>
              Registered users only: Access to and use of certain functions or features on the
              Platform is restricted to Clients and SPs, who have registered and created a user
              account on the Platform, and the Clients’ Authorised Users. To register and create a
              user account, you shall follow the relevant instructions on the Platform. In
              particular, you shall fully, accurately and truthfully complete the registration
              profile and provide such other requested or required information. You shall not obtain
              or attempt to obtain unauthorised access to any part of the Platform, or to any other
              protected information, through any means not intentionally made available by Smarter
              Health for your specific use. A breach of this provision may be an offence under the
              Computer Misuse Act (Chapter 50A) of Singapore.
            </span>
          </div>
          <div className="term-block-sub">
            <span>2.9</span>
            <span>
              At your own risk: The access to and/or use of the Platform is entirely at your own
              risk and we shall not be liable for any risk of misunderstanding, error, loss, damage
              or expense resulting from such access to and/or use of the Platform.
            </span>
          </div>
          <div className="term-block-sub">
            <span>2.10</span>
            <span>
              Instructions that have been issued are irrevocable: All instructions issued by you
              through the Platform shall be deemed to be irrevocable. In particular, you agree that
              any instructions issued through the Platform that are referable to any of the Security
              Credentials shall be deemed to be issued by you.
            </span>
          </div>
          <div className="term-block-sub">
            <span>2.11</span>
            <span>
              Third party-provided Content and services: You acknowledge that Content and services
              from such third party providers, including websites or webpages owned, operated and
              maintained any third party (“Third Party Content”), may be included or provided
              through the Platform and/or Service and that any access and/or use of such Third Party
              Content may be subject to further terms and conditions as may be prescribed by the
              relevant third party provider. Neither Smarter Health nor such third party provider
              shall be liable for any errors or delays in the Platform and/or Service or any part
              thereof (including the Third Party Content), or for any actions taken in reliance
              thereon. Smarter Health is under no obligation to monitor or review Third Party
              Content through the Platform and/or Service, and assumes no responsibility or
              liability for any liabilities, damages, losses, expenses or costs (whether direct or
              indirect, or whether foreseeable or not) arising from or in connection with any such
              Third Party Content nor for any error, defamation, libel, slander, omission,
              falsehood, obscenity, profanity, inaccuracy or any other objectionable material
              contained in any such content.
            </span>
          </div>
          <div className="term-block-sub">
            <span>2.12</span>
            <span>
              Payment services: You acknowledge that Smarter Health does not itself provide any
              payment services through the Platform and/or Service and any services made available
              through the Platform and/or Service through which payment may be made are provided by
              third parties. Such services shall be deemed to be “Third Party Content” for the
              purposes of these Terms.
            </span>
          </div>
        </div>
        <div className="term-block">
          <h5>
            <span>3</span>
            <span>RESERVATION OF RIGHT</span>
          </h5>
          <div className="term-block-sub">
            <span>3.1</span>
            <span>
              Smarter Health may from time to time without giving any reason or prior notice,
              upgrade, modify, alter, suspend, discontinue the provision of or remove, whether in
              whole or in part, the Platform and/or the Service and shall not be liable if any such
              upgrade, modification, suspension or alteration prevents you from accessing the
              Platform and/or the Service or any part thereof. The Content may be modified, deleted
              or replaced from time to time and at any time in the absolute discretion of Smarter
              Health.
            </span>
          </div>
          <div className="term-block-sub">
            <span>3.2</span>
            <span>
              Smarter Health reserves the right, but shall not be obliged to: (i) monitor or screen
              your user account on the Platform and any and all activities that are referable to
              such user account, and/or control any activity, content or material provided through
              the Platform or Service; (ii) remove, block, reject or relocate any User-Provided
              Content made available through the Platform or Service; (iii) prevent or restrict
              access of any user to the Platform or Service; and/or (iv) report any activity it
              suspects to be in violation of any applicable law, statutes or regulations to the
              appropriate authorities and to co-operate with such authorities. You hereby authorise
              Smarter Health to access your user account on the Platform for the purposes of
              investigating any complaints or any suspected violation of these Terms of Use. Without
              prejudice to the foregoing, Smarter Health shall be entitled to investigate any
              suspected violation of these Terms of Use and may take any action it deems appropriate
              in connection to any violation of these Terms of Use.
            </span>
          </div>
        </div>
        <div className="term-block">
          <h5>
            <span>4</span>
            <span>OTHER APPLICABLE TERMS/NEW SERVICES</span>
          </h5>
          <div className="term-block-sub">
            <span>4.1</span>
            <span>
              In addition to these Terms of Use, the use of specific aspects of the Platform or
              Service and/or more comprehensive or updated versions of the Platform or Service may
              be subject to additional terms and conditions (“Additional Terms”), which will apply
              in full force and effect. By agreeing to these Terms of Use, you also agree to such
              Additional Terms.
            </span>
          </div>
          <div className="term-block-sub">
            <span>4.2</span>
            <span>
              Smarter Health reserves the right (but shall not be obliged) to introduce new
              applications, services, functions and/or features (collectively, “New Services”) to
              the Platform and/or Service. The term “Service” shall include New Services which are
              provided at no charge or fee unless otherwise indicated.
            </span>
          </div>
          <div className="term-block-sub">
            <span>4.3</span>
            <span>
              All New Services shall be governed by these Terms of Use and may be subject to
              Additional Terms which you shall be required to agree to before access to and use of
              such New Services are provided. In the event of any inconsistency between these Terms
              of Use and the Additional Terms, the Additional Terms shall prevail unless otherwise
              provided.
            </span>
          </div>
        </div>
        <div className="term-block">
          <h5>
            <span>5</span>
            <span>ADVERTISING</span>
          </h5>

          <div className="term-block-sub">
            <span>5.1</span>
            <span>
              We may attach banners, java applets, links to other third party websites and/or such
              other materials to the Platform for marketing purposes. For the avoidance of doubt,
              you shall not be entitled to receive any payment, fee and/or commission in respect of
              any such advertising or other promotional materials.
            </span>
          </div>
        </div>

        <div className="term-block">
          <h5>
            <span>6</span>
            <span>HYPERLINKS</span>
          </h5>

          <div className="term-block-sub">
            <span>6.1</span>
            <span>
              There may be hyperlinks to other websites or content on the Internet that are owned or
              operated by third parties. Such linked websites or content are not under Smarter
              Health’s control and Smarter Health shall not be responsible for the contents of, or
              the consequences of accessing, any linked website.
            </span>
          </div>

          <div className="term-block-sub">
            <span>6.2</span>
            <span>
              Any hyperlink to any other website or webpage is not an endorsement or verification of
              such website or webpage and should only be accessed at your own risk.
            </span>
          </div>
        </div>

        <div className="term-block">
          <h5>
            <span>7</span>
            <span>DISCLAIMERS</span>
          </h5>

          <div className="term-block-sub">
            <span>7.1</span>
            <span>
              Public nature of Internet: You acknowledge and agree that the access and/or use of the
              Platform via the Internet may be subject to interruption, transmission blackout,
              delayed transmission due to internet traffic or incorrect data transmission due to the
              public nature of the Internet
            </span>
          </div>

          <div className="term-block-sub">
            <span>7.2</span>
            <span>
              Reliance on Materials: You acknowledge that any reliance on any Content and any
              User-Provided Content shall be at your sole risk.
            </span>
          </div>
        </div>
        <div className="term-block">
          <h5>
            <span>8</span>
            <span>NO WARRANTY</span>
          </h5>

          <div className="term-block-sub">
            <span>8.1</span>
            <span>
              The Platform and Service and all Content, are provided “as is” and “as available”. No
              warranty of any kind, implied, express or statutory, including any warranties of
              title, non-infringement of third party rights, merchantability, satisfactory quality,
              fitness for a particular purpose and freedom from computer virus or other malicious,
              destructive or corrupting code, agent, program or macros, is given in conjunction with
              the Platform or Service, or any information and materials provided through the
              Platform or Service. Without prejudice to the generality of the foregoing, Smarter
              Health does not warrant: (i) the accuracy, timeliness, adequacy or completeness of the
              Content; (ii) that your use of and/or access to the Platform, Service and/or Content
              will be uninterrupted, secure or free from errors or omissions or that any identified
              defect will be corrected; (iii) that the Platform, Service and/or Content will meet
              your requirements or are free from any virus or other malicious, destructive or
              corrupting code, agent, program or macros; (iv) that the Platform, Service and/or
              Content, and your use thereof and/or access thereto, will be safe, secure or immune
              from any security breach including any unauthorised interception, interruption or
              modification or any ransomware; and (v) that use of the Platform, Service and/or
              Content by you will not infringe the rights of third parties.
            </span>
          </div>

          <div className="term-block-sub">
            <span>8.2</span>
            <span>
              You acknowledge and agree that Smarter Health does not warrant the security of any
              information transmitted by or to you using the Platform or Service and you hereby
              accept the risk that any information transmitted or received using the Platform or
              Service may be accessed by unauthorised third parties and/or disclosed by Smarter
              Health and by its officers, employees or agents to third parties purporting to be you
              or purporting to act under your authority. You shall not hold Smarter Health or any of
              its officers, employees or agents responsible or liable, in contract, tort (including
              negligence or breach of statutory duty), equity or otherwise, for any such access or
              disclosure or for any liabilities, damages, losses, expenses or costs (whether direct
              or indirect, or whether foreseeable or not) suffered or incurred by you as a result of
              any such access or disclosure.
            </span>
          </div>
        </div>

        <div className="term-block">
          <h5>
            <span>9</span>
            <span>USER-PROVIDED CONTENT</span>
          </h5>

          <div className="term-block-sub">
            <span>9.1</span>
            <span>
              Smarter Health does not endorse nor assume any responsibility for the contents of your
              transmissions or communications through the Platform and/or Service and you are solely
              responsible therefor.
            </span>
          </div>

          <div className="term-block-sub">
            <span>9.2</span>
            <span>
              You warrant, represent and undertake that you have the right and authority to submit
              your User-Provided Content and that your User-Provided Content do not infringe the
              intellectual property rights or any other rights of any third party.
            </span>
          </div>

          <div className="term-block-sub">
            <span>9.3</span>
            <span>
              You hereby grant to Smarter Health a non-exclusive, world-wide royalty-free,
              irrevocable licence and right to host, transmit, distribute or use (which shall
              include without limitation, the right to copy, reproduce and/or publish) the
              User-Provided Content in connection with the provision of the Platform and/or Service
              to you and for the purposes you have instructed or requested.
            </span>
          </div>
        </div>
        <div className="term-block">
          <h5>
            <span>10</span>
            <span>DATA PRIVACY AND CONFIDENTIALITY</span>
          </h5>

          <div className="term-block-sub">
            <span>10.1</span>
            <span>
              In addition to and without prejudice to any other consent which you provide to Smarter
              Health and/or any other User from time to time, you agree that all information and/or
              particulars sent or submitted by you through the Platform or Service or provided to
              Smarter Health through any other means are non-confidential and non-proprietary unless
              otherwise notified to Smarter Health by you in writing and may be collected, used and
              disclosed by Smarter Health and/or its related corporations in accordance with Smarter
              Health’s Privacy Statement (which may be found at:
              https://www.smarterhealth.sg/privacy-policy), as may be updated and/or amended by
              Smarter Health from time to time, or otherwise for the following purposes:
            </span>
          </div>

          <div className="term-block-sub-2">
            <span>10.1.1</span>
            to provide you with access to and use of the Platform and/or Service, including:
          </div>

          <div className="term-block-sub-3">
            <span>(i)</span>
            disclosing to Smarter Health’s service providers such information of yours necessary for
            the purposes of facilitating the provision of the services requested by you through the
            Platform; and
          </div>

          <div className="term-block-sub-3">
            <span>(ii)</span>
            disclosing to other Users such information of yours necessary for the purposes of
            facilitating claims processing, hospital admissions and other related services in
            connection with the services provided through the Platform;
          </div>
          <div>
            <span>10.1.2</span>
            to process payments and transactions;
          </div>

          <div>
            <span>10.1.3</span>
            to respond to, handling, and processing queries, requests, applications, complaints, and
            feedback from you;
          </div>
          <div>
            <span>10.1.4</span>
            to manage your relationship with Smarter Health;
          </div>
          <div>
            <span>10.1.5</span>
            to conduct research, analysis and development activities (including, but not limited to,
            data analytics, surveys, product and service development and/or profiling), to monitor
            and analyse how you use the Platform and Service, to improve the Platform and Service
            and/or to enhance your user experience;
          </div>

          <div>
            <span>10.1.6</span>
            to update Smarter Health’s records and to maintain your user account on the Platform;
          </div>
          <div>
            <span>10.1.7</span>
            to respond to legal processes or to comply generally with any applicable laws,
            governmental or regulatory requirements of any relevant jurisdiction (and any applicable
            rules, codes of practice or guidelines), including, without limitation, meeting the
            requirements to make disclosure under the requirements of any applicable laws or
            assisting in law enforcement and investigations by relevant authorities;
          </div>
          <div>
            <span>10.1.8</span>
            if you are a SP, to market and promote the Platform, the Service and any other products
            and services of Smarter Health and its related corporations;
          </div>
          <div>
            <span>10.1.9</span>
            for any other purposes for which you have provided the information; and
          </div>
          <div>
            <span>10.1.10</span>
            for any other incidental business purposes related to or in connection with the above.
          </div>

          <div className="term-block-sub">
            <span>10.2</span>
            <span>
              You further agree that Smarter Health may collect, from Users or other participants in
              or partners to the Platform from time to time, additional medical information relating
              to you, for example information on the effects, success or failure of any treatment.
              You consent to such persons disclosing your information to Smarter Health, and to
              Smarter Health collecting, using and disclosing such information, for any or all of
              the purposes described in Clause 10.1.
            </span>
          </div>

          <div className="term-block-sub">
            <span>10.3</span>
            <span>
              In respect of any personal data of others which you disclose to Smarter Health
              (whether or not by submission through the Platform), you hereby warrant that you would
              have (prior to disclosing such personal data to Smarter Health) obtained consent from
              the relevant individual whose personal data is being disclosed, to permit you to
              disclose the individual’s personal data to Smarter Health, and to permit Smarter
              Health to collect, use and disclose the individual’s personal data, for the purposes
              set out above.
            </span>
          </div>

          <div className="term-block-sub">
            <span>10.4</span>
            <span>
              Smarter Health may sell anonymised and/or aggregated data (including anonymised and/or
              aggregated patient data but excluding personal data) to third parties.
            </span>
          </div>
        </div>
        <div className="term-block">
          <h5>
            <span>11</span>
            <span>LIMITATION OF LIABILITY</span>
          </h5>

          <div className="term-block-sub">
            <span>11.1</span>
            <span>
              Smarter Health shall in no event nor for any reason whatsoever be liable for any
              liabilities, damages, losses, fines, penalties or expenses, including indirect,
              special, or consequential damage, or economic loss, arising from or in connection with
              (i) any access, use or the inability to access or use the Platform and/or Service;
              (ii) any system, server or connection failure, error, omission, interruption, delay in
              transmission, computer virus or other malicious, destructive or corrupting code, agent
              program or macros; (iii) any use of or access to any other website or webpage provided
              through the Platform or Service; (iv) any services, products, information, data,
              software or other material obtained or downloaded from the Platform and/or Service or
              from any other website or webpage provided through the Platform or Service or from any
              other party referred by or through the use of the Platform or Service; (v) any use or
              misuse of the Platform or Service; (vi) any disclosure of personal data through the
              Platform or Service; or (vii) any failure to redact any personal data when submitting
              medical reports through the Platform or Service. In no event shall Smarter Health be
              liable to you, or any other party for: (a) amounts due from other users of the
              Platform or Service in connection with the purchase of any products/services; and/or
              (b) sales, customs and/or import or export taxes.
            </span>
          </div>
        </div>

        <div className="term-block">
          <h5>
            <span>12</span>
            <span>INDEMNITY</span>
          </h5>

          <div className="term-block-sub">
            <span>12.1</span>
            <span>
              You shall indemnify and hold harmless Smarter Health from and against any and all
              claims, actions, proceedings, suits, liabilities, losses, damages, settlements,
              penalties, fines, costs or expenses (including solicitor and client costs and expenses
              (legal or otherwise)), which Smarter Health may sustain or incur, directly or
              indirectly, arising out of or in connection with Smarter Health having made available
              the Platform and/or Service to you or having entered into these Terms of Use with you
              or enforcement of Smarter Health’s rights under these Terms of Use or in acting upon
              any instructions which you may give in relation to the Platform or Service or any
              negligence, fraud and/or misconduct on your part or your breach of these Terms of Use.
            </span>
          </div>

          <div className="term-block-sub">
            <span>12.2</span>
            <span>
              You shall cooperate fully in the defence of any allegation or third-party legal
              proceeding. Smarter Health reserves the right to assume the exclusive control and
              defence of any indemnified matter under this Clause 12.
            </span>
          </div>
        </div>

        <div className="term-block">
          <h5>
            <span>13</span>
            <span>INTELLECTUAL PROPERTY</span>
          </h5>

          <div className="term-block-sub">
            <span>13.1</span>
            <span>
              All copyright and other intellectual property and proprietary rights in the Content
              belong to Smarter Health or its licensors unless otherwise indicated. Smarter Health
              grants to you a personal, limited, non-exclusive, non-transferable, non-sublicensable
              right to access and use the Platform (including any Content) for the purpose of
              accessing and using the Service.
            </span>
          </div>

          <div className="term-block-sub">
            <span>13.2</span>
            <span>
              You may access material displayed on the Platform or through the Service for your
              non-commercial use only provided that you also retain all copyright and other
              proprietary notices contained on the materials. You may not, however, copy, reproduce,
              distribute, modify, transmit, reuse, re-post, or use the Content for public or
              commercial purposes without Smarter Health’s prior written permission. The trademarks,
              logos, and service marks (collectively the “Trademarks”) displayed on the Platform are
              registered and unregistered Trademarks of Smarter Health or where applicable, other
              third party proprietors. No right or licence is given to any party accessing the
              Platform or Service to download, reproduce or use any such Trademarks.
            </span>
          </div>

          <div className="term-block-sub">
            <span>13.3</span>
            <span>
              You may not under any circumstances: (i) copy, sell, distribute, transmit, publicly
              display, rent, lease, export, publish or otherwise reproduce the Platform or any part
              thereof in any form by any means; and/or (ii) adapt, modify, decompile, disassemble
              and/or reverse engineer the Platform or any part thereof.
            </span>
          </div>

          <div className="term-block-sub">
            <span>13.4</span>
            <span>
              Smarter Health reserves all rights not granted hereunder. For the avoidance of doubt,
              Smarter Health shall not at any time be obliged to provide any adaptations,
              enhancements and/or modifications to the Platform and/or Service, including without
              limitation any updates, patches, bug-fixes and/or upgrades to the Platform and/or
              Service or any new versions and/or releases of the Platform and/or Service which
              incorporate new features or functions.
            </span>
          </div>
        </div>
        <div className="term-block">
          <h5>
            <span>14</span>
            <span>TERMINATION</span>
          </h5>

          <div className="term-block-sub">
            <span>14.1</span>
            <span>
              Smarter Health, in its sole discretion, may with immediate effect upon giving you
              notice in any of the manners prescribed in Clause 16 below, terminate your right to
              access and use the Platform and/or the Service and/or invalidate your Username and
              Password and may bar access to the Platform (or any part thereof) and/or the Service
              (or any part thereof) for any reason whatsoever, including without limitation, any
              breach of these Terms of Use.
            </span>
          </div>

          <div className="term-block-sub">
            <span>14.2</span>
            <span>
              Upon termination of these Terms of Use for any reason whatsoever, all rights and/or
              licences granted to you under these Terms of Use shall immediately cease and terminate
              and you shall forthwith cease the access to and use of the Platform and the Service in
              any way whatsoever.
            </span>
          </div>

          <div className="term-block-sub">
            <span>14.3</span>
            <span>
              Termination of these Terms of Use for any reason shall not affect your obligation to
              make full payment of any fees payable if such fee has not already been paid.
            </span>
          </div>
        </div>

        <div className="term-block">
          <h5>
            <span>15</span>
            <span>AMENDMENTS TO TERMS OF USE</span>
          </h5>

          <div className="term-block-sub">
            <span>15.1</span>
            <span>
              Smarter Health may impose such further terms and conditions and make such amendments
              to these Terms of Use as Smarter Health may in its discretion deem fit from time to
              time (including terms or amendments allowing Smarter Health to revise fees for the use
              of the Platform and/or Service). Smarter Health will notify you of such amendments by
              posting the amendments on the Platform or such other method of notification as may be
              designated by Smarter Health (such as via email or other forms of electronic
              communications), which you agree shall be sufficient notice for the purpose of this
              clause. If you do not agree to be bound by the amendments, you shall immediately cease
              all access and/or use of the Platform and Service. You further agree that if you
              continue to use and/or access the Platform and/or Service after being notified of such
              amendments to these Terms of Use, such use and/or access shall constitute an
              affirmative: (i) acknowledgement by you of these Terms of Use and its amendments; and
              (ii) agreement by you to abide and be bound by these Terms of Use and its amendments.
            </span>
          </div>
        </div>

        <div className="term-block">
          <h5>
            <span>16</span>
            <span>NOTICES</span>
          </h5>

          <div className="term-block-sub">
            <span>16.1</span>
            <span>
              Any notice or other communication in connection with these Terms of Use may be given
              electronically if sent to the address then most recently notified by the recipient to
              the sender. Where the notice or communication is given by Smarter Health to you
              electronically, it will be deemed to have been received upon delivery (and a delivery
              report received by Smarter Health will be conclusive evidence of delivery even if the
              communication is not opened by you) and where given to Smarter Health electronically,
              it will be deemed to have been received upon being opened by Smarter Health.
            </span>
          </div>
        </div>
        <div className="term-block">
          <h5>
            <span>18</span>
            <span>GENERAL</span>
          </h5>

          <div className="term-block-sub">
            <span>18.1</span>
            <span>
              Costs and Expenses: Smarter Health shall not be liable for any cost and expense
              incurred by any User in connection with these Terms of Use, including any
              travel-related expenses, administration costs or the cost of preparing any report. All
              such costs and expenses incurred shall be borne by the User as the case may be.
            </span>
          </div>

          <div className="term-block-sub">
            <span>18.2</span>
            <span>
              GST: If any payment under these Terms of Use constitutes the consideration for a
              taxable supply for GST purposes, then in addition to that payment the payer shall pay
              any GST due.
            </span>
          </div>

          <div className="term-block-sub">
            <span>18.3</span>
            <span>
              The parties hereto are independent contractors and nothing herein shall constitute or
              be construed as creating any agency, partnership or other form of business association
              between the parties or as creating any fiduciary relationship between any of the
              parties. Neither party shall have any authority to enter into any contracts on behalf
              of the other party.
            </span>
          </div>

          <div className="term-block-sub">
            <span>18.4</span>
            <span>
              You may not assign your rights under these Terms of Use without Smarter Health’s prior
              written consent. Smarter Health may assign its rights under these Terms of Use to any
              third party
            </span>
          </div>

          <div className="term-block-sub">
            <span>18.5</span>
            <span>
              These Terms of Use shall bind you and Smarter Health and Smarter Health’s respective
              successors in title and assigns and shall continue to bind you notwithstanding any
              change in Smarter Health’s name or constitution or Smarter Health’s merger,
              consolidation or amalgamation with or into any other entity (in which case these Terms
              of Use shall bind you to Smarter Health’s successor entity).
            </span>
          </div>

          <div className="term-block-sub">
            <span>18.6</span>
            <span>
              If any provision of these Terms of Use is held to be invalid, illegal or unenforceable
              (whether in whole or in part), such provision shall apply with whatever deletion or
              modification is necessary so that the provision is legal, valid and enforceable and
              gives effect to the commercial intention of the parties hereto. To the extent it is
              not possible to delete or modify the provision, in whole or in part, then such
              provision is or part of it shall, to the extent that it is illegal, invalid or
              unenforceable, be deemed not to form part of these Terms of Use and the legality,
              validity and enforceability of the remainder of these Terms of Use shall, subject to
              any deletion or modification made under this clause, not be affected.
            </span>
          </div>

          <div className="term-block-sub">
            <span>18.7</span>
            <span>
              No failure or delay to exercise Smarter Health’s rights under these Terms of Use shall
              operate as a waiver thereof nor shall such failure or delay affect the right to
              enforce Smarter Health’s rights under these Terms of Use.
            </span>
          </div>

          <div className="term-block-sub">
            <span>18.8</span>
            <span>
              If these Terms of Use are translated into a language other than English, the English
              text shall prevail.
            </span>
          </div>

          <div className="term-block-sub">
            <span>18.9</span>
            <span>
              These Terms of Use shall be governed by and construed in accordance with Singapore
              law. You hereby submit to the non-exclusive jurisdiction of the courts of Singapore.
            </span>
          </div>

          <div className="term-block-sub">
            <span>18.10</span>
            <span>
              You acknowledge and agree that Smarter Health’s records and any records of the
              communications, transactions, instructions or operations made or performed, processed
              or effected through the Platform and/or Service by you or any person purporting to be
              you, acting on your behalf or purportedly acting on your behalf, with or without your
              consent, or any record of communications, transactions, instructions or operations
              relating to the operation of the Platform and/or Service and any record of any
              communications, transactions, instructions or operations maintained by Smarter Health
              or by any relevant person authorised by Smarter Health relating to or connected with
              the Platform and/or Service shall be binding on you for all purposes whatsoever and
              shall be conclusive evidence of such communications, transactions, instructions or
              operations.
            </span>
          </div>

          <div className="term-block-sub">
            <span>18.11</span>
            <span>
              No person or entity who is not a party to these Terms of Use shall have any right
              under the Contracts (Rights of Third Parties) Act, Chapter 53B of Singapore, or other
              similar laws to enforce any term of these Terms of Use, regardless of whether such
              person or entity has been identified by name, as a member of a class or as answering a
              particular description. For the avoidance of doubt, this shall not affect the rights
              of any permitted assignee or transferee of these Terms of Use.
            </span>
          </div>

          <div className="term-block-sub">
            <span>18.12</span>
            <span>
              Without prejudice to the generality of Clause 18.11 above, Smarter Health’s right to
              vary, amend or rescind these Terms of Use in accordance with these Terms of Use may be
              exercised without the consent of any person or entity who is not a party to these
              Terms of Use.
            </span>
          </div>

          <div className="term-block-sub">
            <span>18.13</span>
            <span>
              You agree and acknowledge that these Terms of Use and the Service do not include the
              provision of Internet access or other telecommunication services by Smarter Health.
              Any Internet access or telecommunications services (such as mobile data connectivity)
              required by you to access and use the Service shall be your sole responsibility and
              shall be separately obtained by you, at your own cost, from the appropriate
              telecommunications or internet access service provider.
            </span>
          </div>

          <div className="term-block-sub">
            <span>B. The following additional terms and conditions are specific to Clients.</span>
            <span>Under this Part B, “you” shall refer to Clients.</span>
          </div>
        </div>

        <div className="term-block">
          <h5>
            <span>19</span>
            <span>LIMITATION OF LIABILITY</span>
          </h5>

          <div className="term-block-sub">
            <span>19.1</span>
            <span>
              Client agrees that the aggregate liability of Smarter Health to such Client (including
              its Authorised User(s)) for any direct damages arising from or in connection with
              these Terms of Use in any calendar year, shall not exceed a sum equivalent to the
              total amount paid by such Client and received by Smarter Health in connection with
              these Terms of Use in the previous calendar year.
            </span>
          </div>
        </div>

        <div className="term-block">
          <h5>
            <span>20</span>
            <span>CLIENT RESPONSIBILITIES</span>
          </h5>

          <div className="term-block-sub">
            <span>20.1</span>
            <span>
              Use of Platform: The Platform and Service are provided by Smarter Health to you to
              facilitate access to medical care, advice and/or related services. The Content is not,
              and shall not be considered, medical advice or a substitute for individual medical
              advice, diagnosis, or treatment. Where you choose to obtain services or products from
              third parties through the Platform, you agree that Smarter Health will only be
              facilitating your access to such services or products and your communication with such
              third-party service provider or product seller. In the event that you have a dispute
              with any such third-party service provider or product seller, you agree that Smarter
              Health shall have no liability in respect of such dispute and you agree to not
              institute or commence any legal proceedings against Smarter Health (and/or its
              officers, directors, agents, and employees) in relation to such dispute and you
              irrevocably and unconditionally release and discharge Smarter Health from any and all
              liability, claims, demands, or damages, including direct, indirect, special, or
              consequential damage, or economic loss, arising out of or in connection with such
              disputes. You hereby agree to indemnify and hold Smarter Health harmless from any
              liabilities, damages. losses, fines, penalties or expenses, including direct,
              indirect, special, or consequential damage, or economic loss, arising from or in
              connection with such disputes.
            </span>
          </div>

          <div className="term-block-sub">
            <span>20.2</span>
            <span>
              Provision of complete and accurate information: You represent, warrant and undertake
              that all information you or your Authorised Users provide or upload to the Platform,
              including any medical information and records, are complete, accurate and true. You
              understand that the Healthcare Service Providers will be relying upon the truth,
              accuracy and completeness of such information in order to provide services to you,
              including, where you are a Patient, evaluate your health condition(s) and to diagnose
              and/or treat you. You shall indemnify and hold Smarter Health harmless from any
              liabilities, damages, losses, fines, penalties or expenses, including direct,
              indirect, special, or consequential damage, or economic loss, arising from or in
              connection with any false, inaccurate or incomplete information provided or uploaded
              by you. You acknowledge that the Healthcare Service Providers will not be able to
              provide services to you, including, where you are a Patient, suggest suitable medical
              treatments or procedures, without true, accurate and complete information of your
              health condition(s).
            </span>
          </div>

          <div className="term-block-sub">
            <span>20.3</span>
            <span>
              Your responsibility to redact your personal data: You may be required to provide or
              upload medical information and records to the Platform in order to effectively use the
              Platform and/or Service. When you provide or upload medical information and records
              and such other information to the Platform, you consent to the disclosure of the
              information including any personal data in the medical reports to other Clients,
              including the Healthcare Service Providers, and to the use of such disclosed
              information by such Clients for the purposes of providing services to you, including,
              where you are a Patient, evaluating your health condition and providing their expert
              medical opinion to you. In certain circumstances, you may have to refrain from
              disclosing personal data in the medical reports and if so, you must redact, and you
              shall be responsible for redacting, such personal data (e.g. name, ID/passport number,
              address, phone numbers etc.) and such other information which might allow the
              recipient of such information to identify who you are. Smarter Health shall not be
              responsible for your failure to redact such information.
            </span>
          </div>
        </div>

        <div className="term-block">
          <h5>
            <span>21</span>
            <span>DISCLAIMERS</span>
          </h5>

          <div className="term-block-sub">
            <span>21.2</span>
            <span>
              No endorsement: Smarter Health makes no representation or warranty whatsoever in
              respect of any of the Healthcare Service Providers and shall not be responsible for
              the quality or suitability of any of the Healthcare Service Providers for any purpose
              whatsoever. Smarter Health shall not be construed as endorsing any of the Healthcare
              Service Providers. While Smarter Health has put in place processes to check and ensure
              that the SPs have valid certifications and licenses to practise medicine in their
              respective countries when such SPs first register onto the Platform, Smarter Health
              does not warrant that such information of the SPs will be accurate.
            </span>
          </div>

          <div className="term-block-sub">
            <span>21.2</span>
            <span>
              Engagement by you: The engagement of any of the Healthcare Service Providers by each
              Patient, including the medical procedures and the prescription of drugs which any of
              the Healthcare Service Providers perform on or provide to such Patient, and the
              interactions and/or communications between any of the Healthcare Service Providers and
              Patients (whether via the Platform or not), shall purely be a matter between such
              Patient and the relevant Healthcare Service Provider and shall be at such Patient’s
              own risk. Smarter Health shall not be responsible for any such engagement by Patients
              or any interactions and/or communications between Patients and Healthcare Service
              Providers and expressly disclaims any liability or claims arising out of or in
              connection with such engagement, interactions and/or communications. Each Patient
              further agrees that Smarter Health shall not be liable for any liabilities, damages,
              losses, fines, penalties or expenses, including direct, indirect, special, or
              consequential damage, or economic loss, arising out of or in connection with the
              conduct or misconduct of, or malpractice or substandard treatment by, the Healthcare
              Service Providers, including reliance by Patients upon any information provided by the
              Healthcare Service Providers.
            </span>
          </div>

          <div className="term-block-sub">
            <span>21.3</span>
            <span>
              Not a medical adviser: Smarter Health is not a medical adviser and shall in no event
              be construed to be providing Patients with any medical care or advice or a substitute
              for medical advice. The provision of the Platform and Service is merely to facilitate
              the provision of, and access to, medical care, advice and related services. The
              Healthcare Service Providers are not employees or agents of Smarter Health and any
              medical advice provided by a Healthcare Service Provider is not under the control of
              Smarter Health. Each Patient acknowledges that such Patient’s relationship for health
              care or medical services shall be with the respective Healthcare Service Provider, and
              each Patient’s obtaining of services from the Healthcare Service Provider shall solely
              be at such Patient’s own risk and such Patient assumes full responsibility for all
              risks associated therewith to the extent permitted by law.
            </span>
          </div>

          <div className="term-block-sub">
            <span>21.4</span>
            <span>
              Disputes with any other User: In the event that you have a dispute with any other
              User, you agree that Smarter Health shall have no liability in respect of such dispute
              and you agree to not institute or commence any legal proceedings against Smarter
              Health (and/or its officers, directors, agents, and employees) in relation to such
              dispute and you irrevocably and unconditionally release and discharge Smarter Health
              from any and all liability, claims, demands, or damages, including direct, indirect,
              special, or consequential damage, or economic loss, arising out of or in connection
              with such disputes.
            </span>
          </div>
        </div>
        <div className="term-block">
          <h5>
            <span>17</span>
            <span>FORCE MAJEURE</span>
          </h5>

          <div className="term-block-sub">
            <span>17.1</span>
            <span>
              Smarter Health shall not be liable for any non-performance, error, interruption or
              delay in the performance of its obligations or in the Platform’s or the Service’s
              operation, or for any inaccuracy, unreliability or unsuitability of the contents made
              available through the Platform or Service if due, in whole or in part, directly or
              indirectly to an event or failure which is beyond the reasonable control of Smarter
              Health (including acts of God, natural disasters, epidemics, acts of war or terrorism,
              acts of any government or authority, power failures, acts or defaults of any
              telecommunications network operator or carriers or the acts of a party for whom
              Smarter Health is not responsible for).
            </span>
          </div>
        </div>
      </Box>
    </div>
  );
};
