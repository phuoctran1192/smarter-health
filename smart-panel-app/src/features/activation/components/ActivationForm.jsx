import { IconButton, Grid, makeStyles, Button } from '@material-ui/core';
import RoleService from '@services/role.service';
import { ROLE } from '@utils/constants';
import Visibility from '@material-ui/icons/Visibility';
import LockIcon from '@material-ui/icons/Lock';
import React, { useEffect, useRef, useState } from 'react';
import { Form, Formik } from 'formik';
import { FormItem } from '@components/Forms';
import { SPTextField } from '@components/Inputs';
import * as Yup from 'yup';
import { useAuth } from '@hooks';
import { useRouter } from '@hooks/useRouter';
import { LoadingButton } from '../../../components/Buttons/LoadingButton';
import { VisibilityOff } from '@material-ui/icons';
import { Link, useNavigate } from 'react-router-dom';
import Checkbox from '@material-ui/core/Checkbox';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: `${theme.spacing(1)}px 0`,
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: '100%',
  },
  btn: {
    minWidth: '150px',
  },
  title: {
    textAlign: 'center',
  },
  showPassword: {
    padding: '5px',
    marginRight: '-5px',
  },
  extension: {
    display: 'flex',
    fontSize: '12px',
    justifyContent: 'end',
    '& .forgot-pwd': {
      color: theme.palette.primary.main,
    },
  },
  signup: {
    marginTop: '20px',
    cursor: 'pointer',
    textAlign: 'center',
    '& .highlight': {
      color: theme.palette.primary.main,
      textDecoration: 'underline',
      fontWeight: 600,
    },
  },
}));

export const ActivationForm = ({ token }) => {
  const classes = useStyles();
  const router = useRouter();
  const { activate, loading, errors } = useAuth();
  const formikRef = useRef();
  const navigate = useNavigate();
  const [checked, setChecked] = useState(true);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const validationSchema = Yup.object().shape({
    confirmPassword: Yup.string()
      .required('This field is required')
      .min(8, 'Password is too short - should be 8 chars minimum.')
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
        'Password must contains Uppercase, Lowercase, Number and Special Case Character',
      )
      .oneOf([Yup.ref('password'), null], 'Passwords must match'),
    password: Yup.string()
      .required('This field is required')
      .min(8, 'Password is too short - should be 8 chars minimum.')
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
        'Password must contains Uppercase, Lowercase, Number and Special Case Character',
      ),
  });

  const onActivate = (account) => {
    const payload = {
      password: account.password,
      token,
    };
    activate(payload)
      .then((res) => {
        console.log(res);
        navigate('/smart-panel/login');
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {});
  };

  const handleToggleShowPass = () => {
    setShowPassword(!showPassword);
  };

  const handleToggleShowConfirmPass = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  const handleChange = (event) => {
    setChecked(event.target.checked);
  };

  return (
    <div className="login">
      <div className={classes.title}>
        <h1>Activate Your Account!</h1>
        <p>Let’s get you all set up by completing your profile</p>
      </div>
      <Formik
        innerRef={formikRef}
        initialValues={{
          confirmPassword: '',
          password: '',
        }}
        validationSchema={validationSchema}
        onSubmit={onActivate}
      >
        <Form>
          <Grid container spacing={4} style={{ paddingBottom: 0, paddingTop: 0, marginBottom: 0 }}>
            <Grid item xs={12} md={12} style={{ paddingBottom: 0 }}>
              <FormItem label="Password" name="password">
                <SPTextField
                  type={showPassword ? 'text' : 'password'}
                  placeholder="Enter Password"
                  InputProps={{
                    endAdornment: (
                      <IconButton
                        aria-label="toggle password visibility"
                        edge="end"
                        className={classes.showPassword}
                      >
                        <IconButton
                          aria-label="toggle password visibility"
                          edge="end"
                          onClick={handleToggleShowPass}
                        >
                          {showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </IconButton>
                    ),
                    startAdornment: <LockIcon style={{ marginRight: '5px', opacity: '0.7' }} />,
                  }}
                />
              </FormItem>
            </Grid>
            <Grid item xs={12} md={12} style={{ paddingBottom: 0, paddingTop: 0 }}>
              <FormItem label="Confirm Password" name="confirmPassword">
                <SPTextField
                  type={showConfirmPassword ? 'text' : 'password'}
                  placeholder="Enter Confirm Password"
                  InputProps={{
                    endAdornment: (
                      <IconButton
                        aria-label="toggle password visibility"
                        edge="end"
                        className={classes.showPassword}
                      >
                        <IconButton
                          aria-label="toggle password visibility"
                          edge="end"
                          onClick={handleToggleShowConfirmPass}
                        >
                          {showConfirmPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </IconButton>
                    ),
                    startAdornment: <LockIcon style={{ marginRight: '5px', opacity: '0.7' }} />,
                  }}
                />
              </FormItem>
            </Grid>
          </Grid>
          <div style={{ marginTop: '10px' }} className="flex-center">
            <Checkbox
              checked={checked}
              defaultChecked
              color="primary"
              onChange={handleChange}
              inputProps={{ 'aria-label': 'controlled' }}
            />
            <span>I agree to the</span>
            <span style={{ marginLeft: '6px' }}>
              <a href="/smart-panel/terms-and-conditions" target="_blank">
                {' '}
                Terms and Conditions
              </a>
            </span>
          </div>
          <Grid item xs={12} md={12} style={{ marginTop: '20px', width: '100%' }}>
            <LoadingButton type="submit" loading={loading} text="Activate" disabled={!checked} />
          </Grid>
        </Form>
      </Formik>
    </div>
  );
};
