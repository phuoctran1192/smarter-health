import { makeStyles } from '@material-ui/core';
export const MainLayout = ({ ...props }) => {
  return <div>{props.children}</div>;
};
