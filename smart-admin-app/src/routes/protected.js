import { Suspense } from 'react';
import { Outlet } from 'react-router-dom';

import { SmartSpinner } from '@/components/Elements';
import { Dashboard } from '@features/dashboard';
import { Applicant } from '@features/applicant';
import { BasicLayout } from '../layouts';
const App = () => {
  return (
    <Suspense
      fallback={
        <div>
          <SmartSpinner />
        </div>
      }
    >
      <BasicLayout>
        <Outlet />
      </BasicLayout>
    </Suspense>
  );
};

export const protectedRoutes = [
  {
    path: '/smart-panel',
    element: <App />,
    children: [
      { path: 'dashboard/*', element: <Dashboard /> },
      { path: 'applicant/*', element: <Applicant /> },
    ],
  },
];
