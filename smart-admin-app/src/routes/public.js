import { AuthRoutes } from '@/features/auth';
import { Navigate } from 'react-router-dom';
const auth = false; // temporary
export const publicRoutes = [
  {
    path: '/smart-panel',
    element: (
      <>
        <Navigate to={'/smart-panel/auth/login'} />
      </>
    ),
  },
  {
    path: '/smart-panel/auth/*',
    element: <AuthRoutes />,
  },
];
