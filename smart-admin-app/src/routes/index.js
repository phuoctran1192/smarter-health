import { useRoutes } from 'react-router-dom';
import { protectedRoutes } from './protected';
import { publicRoutes } from './public';
import { MainLayout } from '../layouts';
export const AppRoutes = () => {
  const element = useRoutes([...publicRoutes, ...protectedRoutes]);
  return <MainLayout>{element}</MainLayout>;
};
