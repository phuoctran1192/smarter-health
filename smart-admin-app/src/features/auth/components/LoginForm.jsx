import {
  FormControl,
  OutlinedInput,
  InputAdornment,
  IconButton,
  Grid,
  makeStyles,
  Button,
} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import ClearIcon from '@material-ui/icons/Clear';
import clsx from 'clsx';
import { useState } from 'react';
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: `${theme.spacing(1)}px 0`,
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: '100%',
  },
  btn: {
    minWidth: '150px',
  },
}));

export const LoginForm = ({ onSuccess }) => {
  const [account, setAccount] = useState({
    password: '',
    userName: '',
  });
  const classes = useStyles();
  const [showPassword, setShowPassword] = useState(false);

  const handleChange = (type, value) => {
    const temp = { ...account, [type]: value };
    setAccount(temp);
  };
  return (
    <Grid container>
      <Grid item xs={12}>
        <FormControl className={clsx(classes.margin, classes.withoutLabel, classes.textField)}>
          <OutlinedInput
            id="outlined-username"
            type="text"
            value={account.userName}
            placeholder="User Name"
            onChange={(e) => handleChange('userName', e.target.value)}
            endAdornment={
              <InputAdornment position="end">
                <IconButton aria-label="toggle password visibility" edge="end">
                  {account.userName && <ClearIcon />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControl className={clsx(classes.margin, classes.withoutLabel, classes.textField)}>
          <OutlinedInput
            id="outlined-adornment-password"
            type="password"
            placeholder="Password"
            value={account.password}
            onChange={(e) => handleChange('password', e.target.value)}
            endAdornment={
              <InputAdornment position="end">
                <IconButton aria-label="toggle password visibility" edge="end">
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
      </Grid>
      <Grid
        container
        item
        xs={12}
        spacing={2}
        direction="row"
        justifyContent="flex-end"
        alignItems="center"
        style={{ margin: '12px 0 0 0' }}
      >
        <Grid item xs={4}>
          <Button variant="contained" color="primary" classes={{ root: classes.btn }}>
            Register
          </Button>
        </Grid>

        <Grid item xs={4}>
          <Button
            variant="contained"
            color="primary"
            classes={{ root: classes.btn }}
            onClick={onSuccess}
          >
            Login
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};
