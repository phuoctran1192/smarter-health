import { useNavigate } from 'react-router-dom';

import { LoginForm } from './components/LoginForm';
import { makeStyles, Typography } from '@material-ui/core';
import { useEffect } from 'react';
const useStyles = makeStyles((theme) => ({
  login: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '100vh',
  },
  content: {
    width: '500px',
  },
}));

export const Login = () => {
  const navigate = useNavigate();
  const classes = useStyles();
  const onSuccess = () => {
    localStorage.setItem('auth', JSON.stringify({ user: {} }));
    navigate('/smart-panel/dashboard');
  };

  return (
    <div className={classes.login}>
      <div className={classes.content}>
        <Typography align="center" variant="h4" color="primary">
          LOGIN
        </Typography>
        <LoginForm onSuccess={onSuccess} />
      </div>
    </div>
  );
};
