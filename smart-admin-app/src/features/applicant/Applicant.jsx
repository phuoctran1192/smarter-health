import { SPTable } from '@components/Elements';

export const Applicant = () => {
  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
  ];
  return <SPTable columns={columns} />;
};
