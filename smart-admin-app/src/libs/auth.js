import { initReactQueryAuth } from 'react-query-auth';

import { SmartSpinner } from '@/components/Elements';
import { getUser } from '@/features/auth';
import storage from '@/utils/storage';

async function handleUserResponse() {
  const { jwt, user } = data;
  storage.setToken(jwt);
  return user;
}

async function loadUser() {
  if (storage.getToken()) {
    const data = await getUser();
    return data;
  }
  return null;
}

const authConfig = {
  LoaderComponent() {
    return (
      <div className="w-screen h-screen flex justify-center items-center">
        <SmartSpinner />
      </div>
    );
  },
};

export const { AuthProvider, useAuth } = authConfig;
