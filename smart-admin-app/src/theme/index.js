import { createTheme } from '@material-ui/core';
export const SPTheme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1140,
      xl: 1536,
    },
  },
  palette: {
    primary: {
      main: '#296FDB',
      lightGrey: '#C4C4C4',
      black: '#010101',
    },
    error: {
      main: '#ED5E58', // change the error color
    },
  },
  typography: {
    fontFamily: ['Poppins', 'sans-serif'].join(','),
    fontSize: 16,
    h3: {
      fontFamily: ['Playfair Display', 'serif'].join(','),
      fontSize: 42,
      lineHeight: 'initial',
    },
    subtitle1: {
      fontFamily: ['Poppins', 'serif'].join(','),
      fontSize: 22,
      lineHeight: '125%',
    },
  },
  overrides: {
    MuiInputLabel: {
      root: {
        fontSize: '16px',
      },
    },
    MuiMenuItem: {
      root: {
        fontSize: '16px',
      },
    },
  },
});
