import React, { useEffect, useState } from 'react';
import {
  Paper,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Table,
  Link,
  TablePagination,
  Grid,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 700,
  },
  tableHead: {
    backgroundColor: theme.palette.primary.main,
  },
}));

export const SPTable = ({ isReloadTable, setIsReloadTable, params, columns, apiGetList }) => {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageIndex, setPageIndex] = useState(0);
  const [pageSize, setPageSize] = useState(10);

  const fetchList = () => {
    if (isReloadTable) {
      const payload = {
        limit: pageSize,
        pageNo: pageIndex,
        ...params,
      };
      apiGetList(payload)
        .then((res) => {
          if (res.data) {
            setData(res.data.data.pageData);
            setTotal(res.data.data.totalItems);
          }
        })
        .catch((err) => console.log(err))
        .finally(() => setIsReloadTable(false));
    }
  };

  useEffect(() => {
    fetchList();
  }, [isReloadTable]);

  const handleChangePage = (event, newPage) => {
    setPageIndex(newPage);
    setIsReloadTable(true);
  };
  const handleChangePageSize = (event) => {
    setPageSize(event.target.value);
    setPageIndex(0);
    setIsReloadTable(true);
  };

  return (
    <div className={classes.table}>
      <TableContainer
        component={Paper}
        elevation={0}
        id="tableScroll"
        style={{ marginTop: 30, borderRadius: '0.3cm' }}
      >
        <Table>
          <TableHead classes={{ root: classes.tableHead }}>
            <TableRow>
              {columns.map((column, id) => (
                <TableCell style={{ color: 'white', fontSize: '15px' }} key={id}>
                  {column.title}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {data.length === 0 ? (
              <TableRow>
                <TableCell style={{ textAlign: 'center' }} colSpan={columns.length}>
                  No results found
                </TableCell>
              </TableRow>
            ) : (
              data.map((item) => (
                <TableRow key={item.name}>
                  {columns.map((cell) => (
                    <TableCell component="th" scope="row">
                      {item[cell.dataIndex]}
                    </TableCell>
                  ))}
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <Grid item xs={12} style={{ display: 'grid', justifyContent: 'right' }}>
        <TablePagination
          component="div"
          count={total}
          page={pageIndex}
          onChangePage={handleChangePage}
          rowsPerPage={pageSize}
          onChangeRowsPerPage={handleChangePageSize}
          labelRowsPerPage="Rows:"
        />
      </Grid>
    </div>
  );
};
