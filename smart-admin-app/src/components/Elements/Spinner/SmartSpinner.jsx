import CircularProgress from '@material-ui/core/CircularProgress';
export const SmartSpinner = () => <CircularProgress variant="contained" color="primary" />;
