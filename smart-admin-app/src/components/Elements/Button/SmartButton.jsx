import Button from '@material-ui/core/Button';

export const SmartButton = () => (
  <Button variant="contained" color="primary">
    Default
  </Button>
);
